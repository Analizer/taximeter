package com.taximeter.fragments.company.details.mvp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.taximeter.fragments.company.details.CompanyDetailsContract
import com.taximeter.fragments.company.details.CompanyDetailsEventsModule
import com.taximeter.fragments.company.list.mvp.Company
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.CompanyDetailsPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class CompanyDetailsPresenterTest {

    private val analyticsLabel = AnalyticsLabel.Budapest

    private lateinit var mUnderTest: CompanyDetailsPresenter

    private val mReporter: Reporter = mock()
    private val mModel: CompanyDetailsContract.Model = mock()
    private val mEventsHandler: CompanyDetailsEventsModule = mock()
    private val mCompany: Company = mock()

    @Before
    fun setUp() {
        mUnderTest = CompanyDetailsPresenter(mReporter, mModel, mEventsHandler)
        whenever(mModel.company).thenReturn(mCompany)
        whenever(mCompany.analyticsLabel).thenReturn(analyticsLabel)
    }

    @Test
    fun onViewAvailable() {
        mUnderTest.onViewAvailable()
        verify(mEventsHandler).setupTitle()
    }

    @Test
    fun phoneNumberClicked() {
        mUnderTest.phoneNumberClicked("phone_number")
        verify(mReporter).reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.Call, analyticsLabel)
        verify(mEventsHandler).call("phone_number")
    }

    @Test
    fun call() {
        mUnderTest.mNumberToCall = "phone_number"
        mUnderTest.call()
        verify(mEventsHandler).call("phone_number")
    }

    @Test
    fun emailClicked() {
        mUnderTest.emailClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.Mail, analyticsLabel)
        verify(mEventsHandler).writeEmail()
    }

    @Test
    fun webPageClicked() {
        mUnderTest.webPageClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.WebPage, analyticsLabel)
        verify(mEventsHandler).openWebPage()
    }

    @Test
    fun addressClicked() {
        mUnderTest.addressClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.CompanyAddress, analyticsLabel)
        verify(mEventsHandler).navigateToAddress()
    }
}