package com.taximeter.fragments.company.list.mvp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.taximeter.fragments.company.list.CompanyListAdapter
import com.taximeter.fragments.company.list.Caller
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.utils.navigetion.FragmentNavigator
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.CompanyListPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class CompanyListPresenterTest {

    private lateinit var mUnderTest: CompanyListPresenter

    private val mAdapter: CompanyListAdapter = mock()
    private val mEventsHandler: Caller = mock()
    private val mFragmentNavigator: FragmentNavigator = mock()
    private val mView: CompanyListContract.View = mock()

    private val mNumberToCall = "number"

    @Before
    fun setUp() {
        mUnderTest = CompanyListPresenter(mAdapter, mEventsHandler, mFragmentNavigator, mView)
        mUnderTest.mNumberToCall = mNumberToCall
    }

    @Test
    fun onResume() {
        mUnderTest.onResume()
        verify(mAdapter).init(mUnderTest)
        verify(mView).setup(mAdapter)
    }

    @Test
    fun onStop() {
        mUnderTest.onStop()
        verify(mAdapter).tearDown()
        verify(mView).tearDown()
    }

    @Test
    fun onCallButtonClickedWithOneResult() {
        val arrayWithOneNumber: Array<String> = arrayOf("number")
        mUnderTest.onCallButtonClicked(arrayWithOneNumber)
        verify(mEventsHandler).call("number")
    }

    @Test
    fun onCallButtonClickedWithMoreResults() {
        val arrayWithOneNumber: Array<String> = arrayOf("number1", "number2")
        mUnderTest.onCallButtonClicked(arrayWithOneNumber)
        verify(mEventsHandler).showPhoneNumbersDialog(arrayWithOneNumber, mUnderTest)
    }

    @Test
    fun onPhoneNumberSelected() {
        mUnderTest.onPhoneNumberSelected("number")
        verify(mEventsHandler).call("number")
    }

    @Test
    fun onCompanyClicked() {
        val clickedCompany = Company(0, 0, 0, 0, 0, 0, AnalyticsLabel.Budapest)
        mUnderTest.onCompanyClicked(clickedCompany)
        verify(mFragmentNavigator).goToCompanyDetails(clickedCompany)
    }

    @Test
    fun makeCall() {
        mUnderTest.makeCall()
        verify(mEventsHandler).call(mNumberToCall)
    }
}