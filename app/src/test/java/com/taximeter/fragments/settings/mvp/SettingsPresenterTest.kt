package com.taximeter.fragments.settings.mvp

import com.nhaarman.mockitokotlin2.*
import com.taximeter.activity.main.MainActivityFragmentHandler
import com.taximeter.fragments.FragmentType
import com.taximeter.utils.LocaleManager
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.extras.Language
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.SettingsPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class SettingsPresenterTest {

    private lateinit var mUnderTest: SettingsPresenter

    private val mView: SettingsContract.View = mock()
    private val mSharedPref: SharedPref = mock()
    private val mLocaleManager: LocaleManager = mock()
    private val mReporter: Reporter = mock()
    private val mMeasureServiceStateChecker: MeasureServiceStateChecker = mock()
    private val mDialogFactory: DialogFactory = mock()
    private val mFragmentHandler: MainActivityFragmentHandler = mock()

    @Before
    fun setUp() {
        mUnderTest = SettingsPresenter(
                mSharedPref,
                mLocaleManager,
                mReporter,
                mMeasureServiceStateChecker,
                mDialogFactory,
                mFragmentHandler)
        mUnderTest.mView = mView
    }

    @Test
    fun onViewAvailable() {
        mUnderTest.onViewAvailable(mView)
        verify(mView).initListeners()
    }

    @Test
    fun setupLanguageSection() {
        whenever(mLocaleManager.currentLanguage).thenReturn(Language.HUNGARIAN)
        mUnderTest.setupLanguageSection()
        verify(mView).selectHungarian()

        whenever(mLocaleManager.currentLanguage).thenReturn(Language.ENGLISH)
        mUnderTest.setupLanguageSection()
        verify(mView).selectEnglish()
    }

    @Test
    fun setupShowTraffic() {
        whenever(mSharedPref.getBoolean(any(), any())).thenReturn(true)
        mUnderTest.setupShowTraffic()
        verify(mView).setTrafficEnabled(true)

        whenever(mSharedPref.getBoolean(any(), any())).thenReturn(false)
        mUnderTest.setupShowTraffic()
        verify(mView).setTrafficEnabled(false)
    }

    @Test
    fun setupScreenAwake() {
        whenever(mSharedPref.getBoolean(any(), any())).thenReturn(true)
        mUnderTest.setupScreenAwake()
        verify(mView).setScreenAwakeEnabled(true)

        whenever(mSharedPref.getBoolean(any(), any())).thenReturn(false)
        mUnderTest.setupScreenAwake()
        verify(mView).setScreenAwakeEnabled(false)
    }

    @Test
    fun handleEnglishCheckChecked() {
        mUnderTest.handleEnglishCheckChanged(true)
        verify(mView).selectEnglish()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToEnglish)
        verify(mLocaleManager).setLocale(Language.ENGLISH)
    }

    @Test
    fun handleEnglishCheckUnChecked() {
        mUnderTest.handleEnglishCheckChanged(false)
        verify(mView).selectEnglish()
        verifyZeroInteractions(mReporter)
        verifyZeroInteractions(mLocaleManager)
    }

    @Test
    fun handleHungarianCheckChecked() {
        mUnderTest.handleHungarianCheckChanged(true)
        verify(mView).selectHungarian()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToHungarian)
        verify(mLocaleManager).setLocale(Language.HUNGARIAN)
    }

    @Test
    fun handleHungarianCheckUnChecked() {
        mUnderTest.handleHungarianCheckChanged(false)
        verify(mView).selectHungarian()
        verifyZeroInteractions(mReporter)
        verifyZeroInteractions(mLocaleManager)
    }

    @Test
    fun hungarianCheckClickedWhileNotMeasuring() {
        whenever(mMeasureServiceStateChecker.isMeasuring()).thenReturn(false)

        mUnderTest.hungarianCheckClicked(true)
        verify(mView).selectHungarian()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToHungarian)
        verify(mLocaleManager).setLocale(Language.HUNGARIAN)
    }

    @Test
    fun hungarianCheckClickedWhileMeasuring() {
        whenever(mLocaleManager.currentLanguage).thenReturn(Language.HUNGARIAN)

        whenever(mMeasureServiceStateChecker.isMeasuring()).thenReturn(true)
        mUnderTest.hungarianCheckClicked(true)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.DialogSettingsLanguage)
//        verify(mDialogFactory).showAlertDialog(R.string.education_switch_language_while_measuring)
        verify(mView).selectHungarian()
    }

    @Test
    fun englishCheckClickedWhileNotMeasuring() {
        whenever(mMeasureServiceStateChecker.isMeasuring()).thenReturn(false)

        mUnderTest.englishCheckClicked(true)
        verify(mView).selectEnglish()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToEnglish)
        verify(mLocaleManager).setLocale(Language.ENGLISH)
    }

    @Test
    fun englishCheckClickedWhileMeasuring() {
        whenever(mLocaleManager.currentLanguage).thenReturn(Language.ENGLISH)

        whenever(mMeasureServiceStateChecker.isMeasuring()).thenReturn(true)
        mUnderTest.englishCheckClicked(true)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.DialogSettingsLanguage)
//        verify(mDialogFactory).showAlertDialog(R.string.education_switch_language_while_measuring)
        verify(mView).selectEnglish()
    }

    @Test
    fun trafficCheckClicked() {
        mUnderTest.trafficCheckClicked(true)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.UseTrafficOnMap)
        verify(mSharedPref).saveBoolean(Preference.TRAFFIC, true)

        mUnderTest.trafficCheckClicked(false)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.DontUseTrafficOnMap)
        verify(mSharedPref).saveBoolean(Preference.TRAFFIC, false)
    }

    @Test
    fun screeAwakeCheckClicked() {
        mUnderTest.screeAwakeCheckClicked(true)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.KeepScreenAwake)
        verify(mSharedPref).saveBoolean(Preference.SCREEN_AWAKE, true)

        mUnderTest.screeAwakeCheckClicked(false)
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.DontKeepScreenAwake)
        verify(mSharedPref).saveBoolean(Preference.SCREEN_AWAKE, false)
    }

    @Test
    fun privacyPolicyClicked() {
        mUnderTest.privacyPolicyClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.PrivacyPolicyClicked)
        verify(mFragmentHandler).switchFragment(FragmentType.PRIVACY_POLICY)
    }

    @Test
    fun termsAndConditionsClicked() {
        mUnderTest.termsAndConditionsClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.Settings, AnalyticsAction.TermsAndConditionsClicked)
        verify(mFragmentHandler).switchFragment(FragmentType.TERMS_AND_CONDIOTIONS)
    }
}