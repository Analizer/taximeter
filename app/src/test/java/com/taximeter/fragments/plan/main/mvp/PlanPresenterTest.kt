package com.taximeter.fragments.plan.main.mvp

import com.nhaarman.mockitokotlin2.mock
import com.taximeter.fragments.plan.main.list.PlanListAdapter
import com.taximeter.fragments.plan.main.map.PlanMapHeaderBinder
import com.taximeter.fragments.plan.main.map.PlanMapMapModule
import com.taximeter.fragments.plan.main.modules.PlanAppBarHandler
import com.taximeter.fragments.plan.main.modules.PlanDirectionsRetreiver
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.reporting.Reporter
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.SnackBarHandler
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.location.CurrentLocationRetriever
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.PlanPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class PlanPresenterTest {

    private val mModel: PlanContract.Model = mock()
    private val mSnackbarHandler: SnackBarHandler = mock()
    private val mMap: PlanMapMapModule = mock()
    private val mListAdapter: PlanListAdapter = mock()
    private val mDialogFactory: DialogFactory = mock()
    private val mReporter: Reporter = mock()
    private val mNavigator: PlanNavigator = mock()
    private val mHeaderBinder: PlanMapHeaderBinder = mock()
    private val mDirectionsHandler: PlanDirectionsRetreiver = mock()
    private val mCurrentLocationRetriever: CurrentLocationRetriever = mock()
    private val mAppBarHandler: PlanAppBarHandler = mock()
    private val mErrorHandler: ErrorHandler = mock()
    private val mView: PlanContract.View = mock()

    private lateinit var mUnderTest: PlanPresenter

//    @Before
//    fun setUp() {
//        mUnderTest = PlanPresenter(
//                mModel,
//                mSnackbarHandler,
//                mMap,
//                mListAdapter,
//                mDialogFactory,
//                mReporter,
//                mNavigator,
//                mHeaderBinder,
//                mDirectionsHandler,
//                mCurrentLocationRetriever,
//                mAppBarHandler,
//                mErrorHandler)
//        mUnderTest.mView = mView
//    }
//
//    @Test
//    fun onViewAvailable() {
//        mUnderTest.onViewAvailable(mView)
//
//        verify(mMap).init(mUnderTest)
//        verify(mHeaderBinder).init(mView)
//        verify(mAppBarHandler).init(mView)
//        verify(mView).initAppBarLayout()
//        verify(mDialogFactory).showEducationDialogIfNeeded(DialogType.PLAN)
//    }
//
//    @Test
//    fun onResumeWithNoModelToRestore() {
//        mUnderTest.onResume(null)
//
//        verify(mView).setupRecyclerView()
//        verify(mListAdapter).init(mUnderTest)
//        verifyNoMoreInteractions(mReporter)
//        verifyNoMoreInteractions(mListAdapter)
//        verifyNoMoreInteractions(mMap)
//        verifyNoMoreInteractions(mDirectionsHandler)
//    }
//
//    @Test
//    fun onResumeWithModelToRestoreWithNoRealStops() {
//        val saveDate = 123L
//        val sourceAddress: Stop = mock()
//        val destinationAddress: Stop = mock()
//        val planModelToLoad = PlanModel(saveDate, sourceAddress, destinationAddress)
//        whenever(mModel.hasRealStops()).thenReturn(false)
//        mUnderTest.onResume(planModelToLoad)
//
//        verify(mView).setupRecyclerView()
//        verify(mListAdapter).init(mUnderTest)
//        verify(mModel).load(planModelToLoad)
//        verify(mReporter).reportEvent(AnalyticsCategory.Plan, AnalyticsAction.PlanItemsAdded)
//        verify(mListAdapter).update(mModel)
//        verify(mMap).update(mModel)
//        verify(mModel).hasRealStops()
//
//        verifyNoMoreInteractions(mDirectionsHandler)
//
//        verify(mHeaderBinder).hideHeader()
//        verify(mView).hideCarFab()
//        verify(mView).hideCurrentLocationButton()
//    }
//
//    @Test
//    fun onResumeWithModelToRestoreWithRealStops() {
//        val saveDate = 123L
//        val sourceAddress: Stop = mock()
//        val destinationAddress: Stop = mock()
//        val planModelToLoad = PlanModel(saveDate, sourceAddress, destinationAddress)
//        whenever(mModel.hasRealStops()).thenReturn(true)
//        mUnderTest.onResume(planModelToLoad)
//
//        verify(mView).setupRecyclerView()
//        verify(mListAdapter).init(mUnderTest)
//        verify(mModel).load(planModelToLoad)
//        verify(mReporter).reportEvent(AnalyticsCategory.Plan, AnalyticsAction.PlanItemsAdded)
//        verify(mListAdapter).update(mModel)
//        verify(mMap).update(mModel)
//        verify(mModel).hasRealStops()
//        verify(mView).showProgress()
//        verify(mDirectionsHandler).requestDirections(any(), any(), any())
//        verify(mHeaderBinder).hideHeader()
//        verify(mView).hideCarFab()
//        verify(mView).hideCurrentLocationButton()
//    }
//
//    @Test
//    fun onPause() {
//        mUnderTest.onPause()
//        verify(mModel).saveIfNeeded(any())
//        verify(mListAdapter).tearDown()
//        verify(mDirectionsHandler).tearDown()
//        verify(mView).hideCarFab()
//    }
//
//    @Test
//    fun onDestroyView() {
//        mUnderTest.onDestroyView()
//        verify(mCurrentLocationRetriever).tearDown()
//        verify(mMap).tearDown()
//    }
//
//    @Test
//    fun onDataSetChangedWithRealStops() {
//        whenever(mModel.hasRealStops()).thenReturn(true)
//        mUnderTest.onDataSetChanged()
//
//        verify(mListAdapter).notifyDataSetChanged()
//        verify(mMap).update(mModel)
//        verify(mHeaderBinder).update(mModel)
//
//        verify(mModel).hasRealStops()
//        verify(mView).showProgress()
//        verify(mDirectionsHandler).requestDirections(any(), any(), any())
//        verify(mHeaderBinder).hideHeader()
//        verify(mView).hideCarFab()
//        verify(mView).hideCurrentLocationButton()
//    }
//
//    @Test
//    fun onDataSetChangedWithNoRealStops() {
//        whenever(mModel.hasRealStops()).thenReturn(false)
//        mUnderTest.onDataSetChanged()
//
//        verify(mListAdapter).notifyDataSetChanged()
//        verify(mMap).update(mModel)
//        verify(mHeaderBinder).update(mModel)
//
//        verify(mModel).hasRealStops()
//        verifyNoMoreInteractions(mReporter)
//        verifyNoMoreInteractions(mListAdapter)
//        verifyNoMoreInteractions(mMap)
//        verifyNoMoreInteractions(mDirectionsHandler)
//    }
//
//    @Test
//    fun onItemChanged() {
//        mUnderTest.onItemChanged(1)
//        verify(mListAdapter).notifyItemChanged(1)
//    }
//
////    @Test
////    fun onItemClicked() {
////        val stop: Stop = mock()
////        val onSuccessAction = any<(location: Location) -> Unit>()
////        mUnderTest.onItemClicked(stop)
////        verify(mCurrentLocationRetriever).requestLocation(onSuccess = onSuccessAction)//TODO
////    }
//
//    @Test
//    fun deleteItem() {
//        val itemPosition = 1
//        mUnderTest.deleteItem(itemPosition)
//        verify(mReporter).reportEvent(AnalyticsCategory.Plan, AnalyticsAction.DeletePlanItem, AnalyticsLabel.DeleteIcon)
//        verify(mListAdapter).deleteItem(itemPosition)
//    }
//
//    @Test
//    fun showSnackBar() {
//        val onUndoClick = {}
//        mUnderTest.showSnackBar(0, onUndoClick)
//        verify(mSnackbarHandler).showSnackBar(0, onUndoClick)
//    }
//
//    @Test
//    fun showProgress() {
//        mUnderTest.showProgress()
//        verify(mView).showProgress()
//    }
//
//    @Test
//    fun hideProgress() {
//        mUnderTest.hideProgress()
//        verify(mView).hideProgress()
//    }
//
//    @Test
//    fun handleCarFabClickedWhenNoDirections() {
//        whenever(mModel.hasDirections()).thenReturn(false)
//        assertFailsWith<IllegalArgumentException> {
//            mUnderTest.handleCarFabClicked()
//        }
//        verifyNoMoreInteractions(mDialogFactory)
//        verifyNoMoreInteractions(mNavigator)
//        verifyNoMoreInteractions(mReporter)
//    }
//
//    @Test
//    fun handleCarFabClickedWhenDirectionsAndNeedsEducationDialog() {
//        whenever(mModel.hasDirections()).thenReturn(true)
//        mUnderTest.handleCarFabClicked()
//        verify(mDialogFactory).showDialogIfNeeded(any(), any(), any())//TODO
//    }
//
//    @Test
//    fun handleMapClicked() {
//        mUnderTest.handleMapClicked()
//        verify(mAppBarHandler).switch()
//    }
//
//    @Test
//    fun onRequestPermissionsResult() {
//        mUnderTest.onRequestPermissionsResult()
//        verify(mCurrentLocationRetriever).onRequestPermissionsResult()
//    }

//    @Test
//    fun onCurrentLocationClickedOnMap() {
//        mUnderTest.onCurrentLocationClickedOnMap()
//        verify(mCurrentLocationRetriever).requestLocation(onSuccess = any())
//    }
}