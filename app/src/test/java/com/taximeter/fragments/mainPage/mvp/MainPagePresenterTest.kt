package com.taximeter.fragments.mainPage.mvp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.taximeter.activity.main.DrawerModule
import com.taximeter.fragments.mainPage.MainPageTerminateReceiver
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.navigetion.FragmentNavigator
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.MainPagePresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class MainPagePresenterTest {

    private val mView: MainPageContract.View = mock()
    private val mSharedPref: SharedPref = mock()
    private val mTerminateReceiver: MainPageTerminateReceiver = mock()
    private val mServiceChecker: MeasureServiceStateChecker = mock()
    private val mDrawerModule: DrawerModule = mock()
    private val mFragmentNavigator: FragmentNavigator = mock()
    private val mDialogFactory: DialogFactory = mock()
    private val mReporter: Reporter = mock()

    private lateinit var mUnderTest: MainPagePresenter

    @Before
    fun setUp() {
        mUnderTest = MainPagePresenter(mSharedPref, mTerminateReceiver, mServiceChecker, mDrawerModule, mFragmentNavigator, mDialogFactory, mReporter)
        mUnderTest.mView = mView
    }

    @Test
    fun onViewAvailableIfDrawerShouldOpen() {
        whenever(mSharedPref.getBoolean(Preference.INITIAL_DRAWER, false)).thenReturn(false)
        mUnderTest.onViewAvailable(mView)
        verify(mDrawerModule).openDrawer()
        verify(mSharedPref).saveBoolean(Preference.INITIAL_DRAWER, true)
    }

    @Test
    fun onViewAvailableIfDrawerShouldNotOpen() {
        whenever(mSharedPref.getBoolean(Preference.INITIAL_DRAWER, false)).thenReturn(true)
        mUnderTest.onViewAvailable(mView)
        verifyNoMoreInteractions(mDrawerModule)
    }

    @Test
    fun onResumeWhileMeasuring() {
        whenever(mServiceChecker.isMeasuring()).thenReturn(true)
        mUnderTest.onResume()
        verify(mTerminateReceiver).init()
        verify(mView).updateMeasureButtonText(true)
    }

    @Test
    fun onResumeWhileNotMeasuring() {
        whenever(mServiceChecker.isMeasuring()).thenReturn(false)
        mUnderTest.onResume()
        verify(mTerminateReceiver).init()
        verify(mView).updateMeasureButtonText(false)
    }

    @Test
    fun onCallClicked() {
        mUnderTest.onCallClicked()
        verify(mFragmentNavigator).goToCompanyList()
    }

//    @Test
//    fun onMeasureClickedWhenEducationDialogIsNeeded() {
//        val educationDialogListener: EducationDialogListener = mock()
//        mUnderTest.educationDialogListener = educationDialogListener
//        whenever(mDialogFactory.showDialogIfNeeded(DialogType.START_MEASUREMENT, educationDialogListener)).thenReturn(true)
//        mUnderTest.onMeasureClicked()
//        verifyNoMoreInteractions(mFragmentNavigator)
//    }

//    @Test
//    fun onMeasureClickedWhenEducationDialogIsNotNeeded() {
//        val educationDialogListener: EducationDialogListener = mock()
//        mUnderTest.educationDialogListener = educationDialogListener
//        whenever(mDialogFactory.showDialogIfNeeded(DialogType.START_MEASUREMENT, educationDialogListener)).thenReturn(false)
//        mUnderTest.onMeasureClicked()
//        verify(mFragmentNavigator).goToMeasure()
//    }

    @Test
    fun onPlanClicked() {
        mUnderTest.onPlanClicked()
        verify(mFragmentNavigator).goToPlan()
    }

    @Test
    fun setStartMeasurementText() {
        mUnderTest.setStartMeasurementText()
        verify(mView).updateMeasureButtonText(false)
    }

    @Test
    fun tearDown() {
        mUnderTest.tearDown()
        verify(mTerminateReceiver).tearDown()
    }

    @Test
    fun updateMeasureButtonTextWhileMeasuring() {
        whenever(mServiceChecker.isMeasuring()).thenReturn(true)
        mUnderTest.updateMeasureButtonText()
        verify(mView).updateMeasureButtonText(true)
    }

    @Test
    fun updateMeasureButtonTextWhileNotMeasuring() {
        whenever(mServiceChecker.isMeasuring()).thenReturn(false)
        mUnderTest.updateMeasureButtonText()
        verify(mView).updateMeasureButtonText(false)
    }
}