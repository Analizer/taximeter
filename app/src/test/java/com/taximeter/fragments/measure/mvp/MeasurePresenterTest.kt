package com.taximeter.fragments.measure.mvp

import com.google.android.gms.maps.model.LatLng
import com.nhaarman.mockitokotlin2.*
import com.patloew.rxlocation.RxLocation
import com.taximeter.fragments.measure.modules.*
import com.taximeter.fragments.trip.Trip
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.location.LocationEnabler
import com.taximeter.utils.navigetion.FragmentNavigator
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import io.reactivex.disposables.Disposable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.MeasurePresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class MeasurePresenterTest {

    private val mCancelComponent: MeasureCancelModule = mock()
    private val mMap: MeasureMap = mock()
    private val mModel: MeasureContract.Model = mock()
    private val mFragmentNavigator: FragmentNavigator = mock()
    private val mWindowHelper: MeasureWindowHelperModule = mock()
    private val mPageShouldCloseReceiver: MeasurePageShouldCloseReceiver = mock()
    private val mLocationEnabler: LocationEnabler = mock()
    private val mPlanDataHelper: MeasurePlanDataModule = mock()
    private val mServiceConnection: MeasureServiceConnection = mock()
    private val mReporter: Reporter = mock()
    private val mDialogFactory: DialogFactory = mock()
    private val mRxLocation: RxLocation = mock()
    private val mHeaderHelper: MeasureHeaderModule = mock()
    private val mSharedPref: SharedPref = mock()
    private val mView: MeasureContract.View = mock()
    private val mMeasureServiceStateChecker: MeasureServiceStateChecker = mock()

    private var mFinalizerDisposable: Disposable? = mock()
    private var mTrip: Trip = mock()
    private var mTripPoints: List<LatLng> = mock()

    private val mImageKey = "IMAGE_KEY"
    private lateinit var mUnderTest: MeasurePresenter

    @Before
    fun setUp() {
        mUnderTest = MeasurePresenter(mCancelComponent,
                mMap,
                mModel,
                mFragmentNavigator,
                mWindowHelper,
                mPageShouldCloseReceiver,
                mLocationEnabler,
                mPlanDataHelper,
                mServiceConnection,
                mReporter,
                mDialogFactory,
                mRxLocation,
                mHeaderHelper,
                mSharedPref,
                mMeasureServiceStateChecker)
        mUnderTest.mFinalizerDisposable = mFinalizerDisposable
        mUnderTest.mView = mView
    }

    @Test
    fun onViewAvailable() {
        mUnderTest.onViewAvailable(mView)
        verify(mCancelComponent).onViewAvailable()
    }

    @Test
    fun onResumeShouldGoBackIfPageShouldClose() {
        `when`(mPageShouldCloseReceiver.shouldMeasurePageClose()).thenReturn(true)
        mUnderTest.onResume()
        verify(mFragmentNavigator).goBack()
    }

    @Test
    fun onResumeShouldInitIfPageShouldNotClose() {
        `when`(mPageShouldCloseReceiver.shouldMeasurePageClose()).thenReturn(false)

        mUnderTest.onResume()
        verifyZeroInteractions(mFragmentNavigator)
        verify(mMap).init(mUnderTest)
        verify(mPageShouldCloseReceiver).setup()
        verify(mHeaderHelper).reset()
        verify(mView).showProgress()
        verify(mServiceConnection).register(mUnderTest)
    }

    @Test
    fun onResumeShouldSetScreenAwakeIfParamSet() {
        `when`(mPageShouldCloseReceiver.shouldMeasurePageClose()).thenReturn(false)
        `when`(mSharedPref.getBoolean(Preference.SCREEN_AWAKE, true)).thenReturn(true)

        mUnderTest.onResume()
        verify(mWindowHelper).setScreenAwake()
    }

    @Test
    fun onResumeShouldClearScreenAwakeIfParamNotSet() {
        `when`(mPageShouldCloseReceiver.shouldMeasurePageClose()).thenReturn(false)
        `when`(mSharedPref.getBoolean(Preference.SCREEN_AWAKE, true)).thenReturn(false)

        mUnderTest.onResume()
        verify(mWindowHelper).clearScreenAwake()
    }

    @Test
    fun onStop() {
        mUnderTest.onStop()
        verify(mServiceConnection).unSubscribeFromMeasureService()
        verify(mWindowHelper).clearScreenAwake()
        verify(mMap).reset()
        verify(mCancelComponent).onViewUnavailable()
        verify(mFinalizerDisposable)?.dispose()
    }

    @Test
    fun onDestroyView() {
        mUnderTest.onDestroyView()
        verify(mMap).tearDown()
        verify(mPageShouldCloseReceiver).tearDown()
    }

    @Test
    fun onMapReady() {
        mUnderTest.onMapReady()
        verify(mMap).zoomToBudapest()
        verify(mLocationEnabler).check(any(), any())//TODO
    }

    @Test
    fun onTime() {
        mUnderTest.onTime(1)
        verify(mHeaderHelper).updateTime(1)
    }

//    @Test
//    fun onTripUpdatedForEmptyTrip() {
//        `when`(mTrip.pointsString).thenReturn(mTripPoints)
//        `when`(mTripPoints.isEmpty()).thenReturn(true)
//        mUnderTest.onTripUpdated(mTrip)
//        verify(mModel).trip = mTrip
//        verifyZeroInteractions(mMap)
//        verify(mHeaderHelper).update(mTrip)
//    }
//
//    @Test
//    fun onTripUpdatedForNotEmptyTrip() {
//        `when`(mTrip.pointsString).thenReturn(mTripPoints)
//        `when`(mTripPoints.isEmpty()).thenReturn(false)
//        mUnderTest.onTripUpdated(mTrip)
//        verify(mModel).trip = mTrip
//        verify(mMap).update(mTrip, false)
//        verify(mHeaderHelper).update(mTrip)
//    }

    @Test
    fun onTerminateProgress() {
        mUnderTest.onTerminateProgress()
        verify(mFragmentNavigator).goBack()
    }

    @Test
    fun handleCurrentLocationClicked() {
        mUnderTest.handleCurrentLocationClicked()
        verify(mReporter).reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.CurrentLocationOnMap)
        verify(mView).hideCurrentLocationButton()
        verify(mMap).handleCurrentLocationClicked()
    }

    @Test
    fun onMapMovedByUserShouldShowCurrentLocationButton() {
        mUnderTest.onMapMovedByUser()
        verify(mView).showCurrentLocationButton()
    }

    @Test
    fun onMapImageCaptured() {
        `when`(mModel.trip).thenReturn(mTrip)
        mUnderTest.onMapImageCaptured(mImageKey)
        verify(mModel.trip!!).mapThumbnailString = mImageKey
        verify(mModel).save()
        verify(mFragmentNavigator).goToTripDetailsFromMeasure(mModel.trip!!)
    }

    @Test
    fun onRequestPermissionsResult() {
        mUnderTest.onRequestPermissionsResult()
        verify(mLocationEnabler).onRequestPermissionsResult()
    }

    @Test
    fun testMainButtonClickedWhenNotMeasuring() {
        `when`(mModel.isTripStarted()).thenReturn(false)

        mUnderTest.handleStopButtonClicked()
        verify(mModel).isTripStarted()
        verifyZeroInteractions(mMeasureServiceStateChecker)
        verifyZeroInteractions(mReporter)
        verifyZeroInteractions(mPlanDataHelper)
        verifyZeroInteractions(mServiceConnection)
        verifyZeroInteractions(mModel)
        verifyZeroInteractions(mDialogFactory)
    }

//    @Test
//    fun testMainButtonClickedWhenMeasuringAndDialogNeeded() {
//        `when`(mModel.isTripStarted()).thenReturn(true)
//        `when`(mMeasureServiceStateChecker.isMeasuring()).thenReturn(true)
//        `when`(mDialogFactory.isEducationDialogNeeded(DialogType.STOP_MEASUREMENT)).thenReturn(true)
//        mUnderTest.handleStopButtonClicked()
//        verify(mModel).isTripStarted()
//        verify(mMeasureServiceStateChecker).isMeasuring()
//        verify(mDialogFactory).showDialogIfNeeded(eq(DialogType.STOP_MEASUREMENT), any())
//        verifyZeroInteractions(mReporter)
//        verifyZeroInteractions(mPlanDataHelper)
//        verifyZeroInteractions(mServiceConnection)
//        verifyZeroInteractions(mModel)
//    }

//    @Test
//    fun testMainButtonClickedWhenMeasuringAndDialogNotNeeded() {
//        `when`(mModel.isTripStarted()).thenReturn(true)
//        `when`(mModel.trip).thenReturn(trip)
//        `when`(mMeasureServiceStateChecker.isMeasuring()).thenReturn(true)
//        `when`(mDialogFactory.isEducationDialogNeeded(DialogType.STOP_MEASUREMENT)).thenReturn(false)
//        mUnderTest.handleStopButtonClicked()
//        verify(mModel).isTripStarted()
//        verify(mMeasureServiceStateChecker).isMeasuring()
//        verify(mDialogFactory).showEducationDialogWithOptionsIfNeeded(eq(DialogType.STOP_MEASUREMENT), any())
//        verify(mReporter).reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.StopMeasurement)
//        verify(mServiceConnection).stopService()
//        verify(mPlanDataHelper).tearDown()
//        //TODO finalizeTrip
//    }
}