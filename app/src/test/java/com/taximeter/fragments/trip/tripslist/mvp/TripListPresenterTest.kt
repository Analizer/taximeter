package com.taximeter.fragments.trip.tripslist.mvp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripslist.adapter.TripsListAdapter
import com.taximeter.utils.SnackBarHandler
import com.taximeter.utils.navigetion.FragmentNavigator
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.TripListPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class TripListPresenterTest {

    private lateinit var mUnderTest: TripListPresenter

    private val mView: TripListContract.View = mock()
    private val mAdapter: TripsListAdapter = mock()
    private val mSnackbarHandler: SnackBarHandler = mock()
    private val mFragmentNavigator: FragmentNavigator = mock()

    @Before
    fun setUp() {
        mUnderTest = TripListPresenter(mAdapter, mFragmentNavigator, mSnackbarHandler)
        mUnderTest.mView = mView
    }

    @Test
    fun initForEmptyList() {
        whenever(mAdapter.itemCount).thenReturn(0)
        mUnderTest.init()
        verify(mAdapter).init(mUnderTest)
        verify(mView).setup()
        verify(mView).showEmptyMessage()
    }

    @Test
    fun initForNotEmptyList() {
        whenever(mAdapter.itemCount).thenReturn(1)
        mUnderTest.init()
        verify(mAdapter).init(mUnderTest)
        verify(mView).setup()
        verify(mView).hideEmptyMessage()
    }

    @Test
    fun tearDown() {
        mUnderTest.tearDown()
        verify(mAdapter).tearDown()
    }

    @Test
    fun onDataSetChangedForNotEmptyList() {
        whenever(mAdapter.itemCount).thenReturn(1)
        mUnderTest.onDataSetChanged()
        verify(mView).hideEmptyMessage()
    }

    @Test
    fun onItemClicked() {
        val trip: Trip = mock()
        mUnderTest.onItemClicked(trip)
        verify(mFragmentNavigator).goToTripDetailsFromTripList(trip)
    }

    @Test
    fun showSnackBar() {
        val undoFunction = {}
        mUnderTest.showSnackBar(0, undoFunction)
        verify(mSnackbarHandler).showSnackBar(0, undoFunction)
    }

    @Test
    fun scrollToPosition() {
        mUnderTest.scrollToPosition(0)
        verify(mView).scrollToPosition(0)
    }
}