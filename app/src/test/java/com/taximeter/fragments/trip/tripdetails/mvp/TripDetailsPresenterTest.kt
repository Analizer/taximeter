package com.taximeter.fragments.trip.tripdetails.mvp

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.taximeter.fragments.trip.tripdetails.map.TripDetailsMap
import com.taximeter.reporting.Reporter
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.location.CurrentLocationRetriever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/*
* To run this test only:
*  ./gradlew testDebug --tests="*.TripDetailsPresenterTest"
*/
@RunWith(MockitoJUnitRunner::class)
class TripDetailsPresenterTest {

    private val mView: TripDetailsContract.View = mock()
    private val mHeaderCalculator: TripDetailsHeaderCalculator = mock()
    private val mDialogFactory: DialogFactory = mock()
    private val mMap: TripDetailsMap = mock()
    private val mReporter: Reporter = mock()
    private val mCurrentLocationRetriever: CurrentLocationRetriever = mock()

    private lateinit var mUnderTest: TripDetailsPresenter

    @Before
    fun setUp() {
        mUnderTest = TripDetailsPresenter(mHeaderCalculator,
                true,
                mDialogFactory,
                mMap,
                mReporter,
                mCurrentLocationRetriever)
        mUnderTest.mView = mView
    }

    @Test
    fun onViewAvailableFromList() {
        whenever(mHeaderCalculator.getDistance()).thenReturn("distance")
        whenever(mHeaderCalculator.getTime()).thenReturn("duration")
        whenever(mHeaderCalculator.getPrice()).thenReturn("price")
        whenever(mHeaderCalculator.getSpeed()).thenReturn("speed")

        mUnderTest.onViewAvailable(mView)
        verify(mMap).init(mUnderTest)
        verify(mView).setDistance("distance")
        verify(mView).setTime("duration")
        verify(mView).setPrice("price")
        verify(mView).setSpeed("speed")

        verifyNoMoreInteractions(mDialogFactory)
    }

    @Test
    fun onViewAvailableNotFromList() {
        mUnderTest = TripDetailsPresenter(mHeaderCalculator,
                false,
                mDialogFactory,
                mMap,
                mReporter,
                mCurrentLocationRetriever)

        whenever(mHeaderCalculator.getDistance()).thenReturn("distance")
        whenever(mHeaderCalculator.getTime()).thenReturn("duration")
        whenever(mHeaderCalculator.getPrice()).thenReturn("price")
        whenever(mHeaderCalculator.getSpeed()).thenReturn("speed")

        mUnderTest.onViewAvailable(mView)
        verify(mMap).init(mUnderTest)
        verify(mView).setDistance("distance")
        verify(mView).setTime("duration")
        verify(mView).setPrice("price")
        verify(mView).setSpeed("speed")

        verify(mDialogFactory).showDialogIfNeeded(DialogType.TRIP_DETAILS)
    }

//    @Test
//    fun onCurrentLocationClicked() {
//        mUnderTest.onCurrentLocationClicked()
//        verify(mReporter).reportEvent(AnalyticsCategory.TripDetails, AnalyticsAction.CurrentLocationOnMap)
//        val onSuccessAction:(location:Location)->Unit = { location ->
//            mView.hideProgress()
//            mMap.zoomToLocation(GeoUtils.toLatLng(location))
//        }
//        verify(mCurrentLocationRetriever).requestLocation(onSuccess = onSuccessAction)//TODO
//    }

    @Test
    fun tearDown() {
        mUnderTest.tearDown()
        verify(mMap).tearDown()
        verify(mCurrentLocationRetriever).tearDown()
    }

    @Test
    fun onRequestPermissionsResult() {
        mUnderTest.onRequestPermissionsResult()
        verify(mCurrentLocationRetriever).onRequestPermissionsResult()
    }

    @Test
    fun onMapReady() {
        mUnderTest.onMapReady()
        verify(mView).showCurrentLocationButton()
    }
}