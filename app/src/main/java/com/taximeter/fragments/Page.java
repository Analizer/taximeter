package com.taximeter.fragments;

public interface Page {

    FragmentType getFragmentType();
}
