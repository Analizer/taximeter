package com.taximeter.fragments.measure.modules

import android.content.res.Resources
import android.view.View
import android.widget.TextView
import com.taximeter.R
import com.taximeter.fragments.trip.Trip
import com.taximeter.utils.CalculationUtils.getFormattedTimeBySeconds
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.measure_map_header.view.*
import javax.inject.Inject

@FragmentScoped
class MeasureHeaderModule @Inject constructor(private val mResources: Resources) {

    private lateinit var mSpeedTv: TextView
    private lateinit var mTimeTv: TextView
    private lateinit var mDistanceTv: TextView
    private lateinit var mPriceTv: TextView

    fun bind(pageView: View) {
        mSpeedTv = pageView.speed_tv
        mTimeTv = pageView.time_tv
        mDistanceTv = pageView.distance_tv
        mPriceTv = pageView.price_tv
    }

    fun update(trip: Trip) {
        mDistanceTv.text = trip.getFormattedDistance(mResources)
        mSpeedTv.text = trip.getFormattedSpeed(mResources)
        mPriceTv.text = trip.getFormattedPrice(mResources)
    }

    fun updateTime(seconds: Long) {
        mTimeTv.text = getFormattedTimeBySeconds(seconds, mResources, false)
    }

    fun reset() {
        mDistanceTv.text = mResources.getString(R.string.measure_default_distance)
        mPriceTv.text = mResources.getString(R.string.measure_default_price)
        mSpeedTv.text = mResources.getString(R.string.measure_default_speed)
        mTimeTv.text = mResources.getString(R.string.measure_default_time)
    }
}