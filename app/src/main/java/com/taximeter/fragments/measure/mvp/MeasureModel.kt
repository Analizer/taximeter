package com.taximeter.fragments.measure.mvp

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.taximeter.fragments.trip.Trip
import com.taximeter.utils.TripPolyLineOptimizer
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.calculator.PriceCalculator
import javax.inject.Inject

/**
 * Helper class used by the measure function. It handles starting, updating and finishing the trip and wraps the @Trip object while measuring
 */
data class MeasureModel @Inject constructor(override val trip: Trip,
                                            private val mPriceCalculator: PriceCalculator,
                                            private val mPolyLineOptimizer: TripPolyLineOptimizer,
                                            private val mGeoCalculator: GeoCalculator,
                                            private val mGson: Gson)
    : MeasureContract.Model {

    private var mNumberOfSpeeds: Int = 0

    override val points: List<LatLng>
        get() = trip.getPoints(mGson)

    override fun start(location: Location) {
        trip.departureTime = System.currentTimeMillis()
        trip.updatePoints(mutableListOf(mGeoCalculator.toLatLng(location)))
        updateSpeed(location.speed)
        trip.price = PriceCalculator.BASE_PRICE
    }

    override fun update(lastLocation: Location, newLocation: Location) {
        trip.price = mPriceCalculator.update(lastLocation, newLocation)
        updateSpeed(newLocation.speed)
        trip.distance += lastLocation.distanceTo(newLocation).toInt()
        trip.updatePoints(mPolyLineOptimizer.update(trip, newLocation))
    }

    override fun finish(sourceAddress: String, destinationAddress: String) {
        trip.arrivalTime = System.currentTimeMillis()
        trip.sourceAddress = sourceAddress
        trip.destinationAddress = destinationAddress
        trip.pointsString = mGson.toJson(trip.getPoints(mGson))
    }

    override fun isTripStarted(): Boolean {
        return trip.departureTime != 0L
    }

    private fun updateSpeed(speed: Float) {
        val speedSum = trip.averageSpeed * mNumberOfSpeeds
        trip.averageSpeed = (speedSum + speed) / ++mNumberOfSpeeds
    }
}