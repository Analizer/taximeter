package com.taximeter.fragments.measure.mvp

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.trip.Trip
import com.taximeter.service.measure.MeasureFunctions

interface MeasureContract {

    interface Presenter : MeasureFunctions {
        fun onViewAvailable(view: View)

        fun onResume()

        fun onStop()

        fun onDestroyView()

        fun onMapReady()

        fun handleCurrentLocationClicked()

        fun handleStopButtonClicked()

        fun onMapMovedByUser()

        fun onMapImageCaptured(imageKey: String?)

        fun onRequestPermissionsResult()
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun showProgress()

        fun showCurrentLocationButton()

        fun hideCurrentLocationButton()
    }

    interface Model {
        fun isTripStarted(): Boolean

        fun start(location: Location)

        fun update(lastLocation: Location, newLocation: Location)

        fun finish(sourceAddress: String, destinationAddress: String)

        val points: List<LatLng>

        val trip: Trip
    }
}
