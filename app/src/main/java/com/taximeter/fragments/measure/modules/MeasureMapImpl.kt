package com.taximeter.fragments.measure.modules

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import com.taximeter.R
import com.taximeter.fragments.di.ChildFragmentManager
import com.taximeter.fragments.measure.mvp.MeasureContract
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.utils.CalculationUtils.getFullDateString
import com.taximeter.utils.ImageUtils
import com.taximeter.utils.MapHelper
import javax.inject.Inject

class MeasureMapImpl @Inject constructor(
        private val mPlanDataModule: MeasurePlanDataModule,
        private val mSharedPref: SharedPref,
        private val mContext: Context,
        private val mMapHelper: MapHelper,
        @ChildFragmentManager private val mFragmentManager: FragmentManager)
    : MeasureMap {

    private lateinit var mPresenter: MeasureContract.Presenter

    private var mMapImageName: String? = null
    private var mStartMarker: Marker? = null
    private var mPolyline: Polyline? = null
    private var mMap: GoogleMap? = null
    private var mLastPosition: LatLng? = null
    private var mShouldFollowUserPosition = true

    override fun init(presenter: MeasureContract.Presenter) {
        mPresenter = presenter
        (mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)
    }

    override fun reset() {
        mLastPosition = null
        mStartMarker = null
        mPolyline = null
        if (mMap != null) {
            mMap!!.clear()
            mMap = null
        }
    }

    override fun tearDown() {
        val fragment = mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        if (fragment != null) {
            mFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        }
    }

    override fun zoomToBudapest() {
        mMapHelper.zoomToBudapest(mMap!!)
    }

    override fun onSnapshotReady(snapShot: Bitmap?) {
        ImageUtils.saveImage(mContext, snapShot, mMapImageName)
        mPresenter.onMapImageCaptured(mMapImageName)
    }

    override fun handleCurrentLocationClicked() {
        if (mLastPosition != null) {
            mMapHelper.zoomToLocation(mLastPosition!!, mMap!!)
        }
        mShouldFollowUserPosition = true
    }

    override fun update(tripPoints: List<LatLng>, departureTime: Long, isLastUpdate: Boolean) {
        addMarkerIfNeeded(tripPoints, isLastUpdate)
        updatePolyLine(tripPoints)
        mLastPosition = tripPoints[tripPoints.size - 1]
        if (mShouldFollowUserPosition && !isLastUpdate) {
            mMapHelper.zoomToLocation(mLastPosition!!, mMap!!)
        }
        if (mMapImageName == null) {
            mMapImageName = getFullDateString(departureTime)
        }
    }


    @SuppressLint("MissingPermission")
    override fun setMyLocationEnabled() {
        mMap!!.isMyLocationEnabled = true
    }

    override fun onCameraMoveStarted(reason: Int) {
        mShouldFollowUserPosition = reason != GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
        if (!mShouldFollowUserPosition) {
            mPresenter.onMapMovedByUser()
        }
    }

    override fun onMapReady(map: GoogleMap) {
        mMap = map
        setupMap()
        mPlanDataModule.onMapReady(map)
        mPresenter.onMapReady()
    }

    private fun setupMap() {
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.setAllGesturesEnabled(true)
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.isTrafficEnabled = mSharedPref.getBoolean(Preference.TRAFFIC, false)
        mMap!!.setOnCameraMoveStartedListener(this)
        zoomToBudapest()
    }

    private fun addMarkerIfNeeded(tripPoints: List<LatLng>, lastUpdate: Boolean) {
        if (mStartMarker == null) {
            mStartMarker = mMapHelper.addMarkerToMap(mMap!!, tripPoints[0])
            if (tripPoints.size == 1) {
                mMapHelper.animateToLocation(tripPoints[0], mMap!!)
            }
        }
        if (lastUpdate) {
            mMapHelper.addMarkerToMap(mMap!!, tripPoints[tripPoints.size - 1])
            zoomToBudapest()
            mMap!!.setOnMapLoadedCallback { mMap!!.snapshot(this) }
        }
    }

    private fun updatePolyLine(tripPoints: List<LatLng>) {
        if (mPolyline == null) {
            val pathOptions = PolylineOptions().addAll(tripPoints)
            mPolyline = mMap!!.addPolyline(pathOptions)
        } else {
            mPolyline!!.points = tripPoints
        }
    }
}