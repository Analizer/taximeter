package com.taximeter.fragments.measure.modules

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.View
import android.widget.Button
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.service.measure.TerminateCaller
import com.taximeter.utils.UiUtils.animateInView
import com.taximeter.utils.UiUtils.animateOutView
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_measure_map.view.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@FragmentScoped
class MeasureCancelModule @Inject constructor(
        private val mContext: Context,
        private val mReporter: Reporter,
        private val mMeasureServiceStateChecker: MeasureServiceStateChecker
) {

    private val hideButtonDelay: Long = 5
    private lateinit var mCancelButton: Button

    private var mTimerDisposable: Disposable? = null

    fun bind(pageView: View) {
        mCancelButton = pageView.measure_cancel_btn
        mCancelButton.setOnClickListener {
            mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.MeasurementCancelled)
            val terminateIntent = Intent()
            terminateIntent.action = Extra.ACTION_TERMINATE.tag
            terminateIntent.putExtra(Extra.TAG_TERMINATE_CALLER.tag, TerminateCaller.MEASURE_CANCEL_BUTTON)
            mContext.sendBroadcast(terminateIntent)
        }
    }

    fun onViewAvailable() {
        if (!mMeasureServiceStateChecker.isMeasuring()) {
            Handler().postDelayed({ animateInView(mCancelButton) }, 1000)
            startTimer()
        }
    }

    fun onViewUnavailable() {
        mTimerDisposable?.dispose()
        animateOutView(mCancelButton)
    }

    private fun startTimer() {
        mTimerDisposable = Observable.interval(hideButtonDelay, 0, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    animateOutView(mCancelButton)
                    mTimerDisposable?.dispose()
                }
    }

}