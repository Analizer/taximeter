package com.taximeter.fragments.measure.modules

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.measure.mvp.MeasureContract

interface MeasureMap : OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.SnapshotReadyCallback {

    fun init(presenter: MeasureContract.Presenter)

    fun reset()

    fun tearDown()

    fun handleCurrentLocationClicked()

    fun update(tripPoints: List<LatLng>, departureTime: Long, isLastUpdate: Boolean)

    fun setMyLocationEnabled()

    fun zoomToBudapest()
}
