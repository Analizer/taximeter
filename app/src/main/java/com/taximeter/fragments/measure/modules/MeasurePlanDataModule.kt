package com.taximeter.fragments.measure.modules

import android.content.res.Resources
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Polyline
import com.taximeter.R
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.utils.CalculationUtils.*
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.measure_map_footer.view.*
import javax.inject.Inject

@FragmentScoped
class MeasurePlanDataModule @Inject
constructor(private val mResources: Resources) {

    private lateinit var mFooter: View
    private lateinit var mFooterExpenseTv: TextView
    private lateinit var mFooterTimeTv: TextView
    private lateinit var mFooterDistanceTv: TextView

    private var mMap: GoogleMap? = null
    private var mDirections: DirectionModel? = null
    private var mPolyline: Polyline? = null

    fun bind(pageView: View) {
        mFooter = pageView.measure_footer_holder
        mFooterExpenseTv = pageView.plan_expense_tv
        mFooterTimeTv = pageView.plan_time_tv
        mFooterDistanceTv = pageView.plan_distance_tv
    }

    fun onMapReady(map: GoogleMap?) {
        mMap = map
        addPlanIfReady()
    }

    fun addDirections(directionModel: DirectionModel?) {
        mDirections = directionModel
        addPlanIfReady()
    }

    fun tearDown() {
        mPolyline?.remove()
        mDirections = null
    }

    private fun addPlanIfReady() {
        if (shouldAddPlan()) {
            fillFooterData()
            val pathOptions = mDirections!!.polylineOptions
            pathOptions!!.color(ResourcesCompat.getColor(mResources, R.color.green, null))
            mPolyline = mMap!!.addPolyline(pathOptions)
        }
    }

    private fun fillFooterData() {
        mFooterDistanceTv.text = getKilometersTextByMeters(mDirections!!.distance, mResources)
        mFooterTimeTv.text = getFormattedTimeBySeconds(mDirections!!.duration.toLong(), mResources, true)
        mFooterExpenseTv.text = getFormattedPrice(getRoundedPrice(mDirections!!.estimatedPrice), mResources)
        mFooter.visibility = View.VISIBLE
    }

    private fun shouldAddPlan(): Boolean {
        return mMap != null && mDirections != null && mDirections!!.isValid
    }
}
