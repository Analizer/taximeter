package com.taximeter.fragments.measure.modules

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.service.measure.MeasureFunctions
import com.taximeter.service.measure.MeasureService
import com.taximeter.service.measure.MeasureServiceBinder
import com.taximeter.utils.extras.Extra

import javax.inject.Inject

class MeasureServiceConnection @Inject
constructor(private val mPlanDataHelper: MeasurePlanDataModule,
            private val mDirectionModel: DirectionModel?,
            private val mContext: Context) : ServiceConnection {

    private var mMeasureFunctions: MeasureFunctions? = null
    private var mMeasureServiceBinder: MeasureServiceBinder? = null

    override fun onServiceConnected(componentName: ComponentName, binder: IBinder) {
        mMeasureServiceBinder = binder as MeasureServiceBinder
        mMeasureServiceBinder!!.subscribe(mMeasureFunctions)
        mPlanDataHelper.addDirections(mDirectionModel)
    }

    override fun onServiceDisconnected(componentName: ComponentName) {
        mMeasureServiceBinder = null
    }

    fun register(functions: MeasureFunctions) {
        mMeasureFunctions = functions
    }

    fun connect() {
        val serviceIntent = Intent(mContext, MeasureService::class.java)
        mContext.startService(serviceIntent) //if not, this gets destroyed after unbind
        mContext.bindService(serviceIntent, this, Context.BIND_AUTO_CREATE)
    }

    fun unSubscribeFromMeasureService() {
        mMeasureServiceBinder?.unSubscribe()
        mMeasureServiceBinder = null
        mContext.unbindService(this)
    }

    fun stopService() {
        val serviceIntent = Intent(Extra.ACTION_STOP_FOREGROUND.tag)
        serviceIntent.setClass(mContext, MeasureService::class.java)
        mContext.startService(serviceIntent)
    }
}
