package com.taximeter.fragments.measure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.measure.modules.MeasureCancelModule
import com.taximeter.fragments.measure.modules.MeasureHeaderModule
import com.taximeter.fragments.measure.modules.MeasurePlanDataModule
import com.taximeter.fragments.measure.mvp.MeasureContract
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MeasureFragment : Fragment(), Page {

    @Inject
    lateinit var mView: MeasureContract.View

    @Inject
    lateinit var mPresenter: MeasureContract.Presenter

    @Inject
    lateinit var mPlanDataModule: MeasurePlanDataModule

    @Inject
    lateinit var mCancelModule: MeasureCancelModule

    @Inject
    lateinit var mHeaderModule: MeasureHeaderModule

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_measure_map, container, false)
        mView.bind(pageLayout)
        mCancelModule.bind(pageLayout)
        mHeaderModule.bind(pageLayout)
        mPlanDataModule.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.MEASURE
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        mPresenter.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDestroyView()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPresenter.onRequestPermissionsResult()
    }
}