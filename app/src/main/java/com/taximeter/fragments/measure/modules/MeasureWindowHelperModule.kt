package com.taximeter.fragments.measure.modules

import android.view.Window
import android.view.WindowManager

import javax.inject.Inject

class MeasureWindowHelperModule @Inject constructor(private val mWindow: Window) {

    fun setScreenAwake() {
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun clearScreenAwake() {
        mWindow.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }
}
