package com.taximeter.fragments.measure.modules

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.fragment.app.Fragment
import com.taximeter.utils.extras.Extra
import javax.inject.Inject

/**
 * Receiver class "remembering" that the measurement got terminated, so in case it happened while the app was in the background and the @[MeasureFragment] was visible,
 * returning the app will not trigger a new measurement process. Two known cases are when the user terminates the process from the notification or when the "Enable location"
 * dialog is shown, the app is backgrounded and foregrounded again and the Cancel button is pressed. The point is that however the fragment is unBound from the service
 * in onStop, this receiver is alive until onDestroyView.
 */
class MeasurePageShouldCloseReceiver @Inject constructor(private val mFragment: Fragment) : BroadcastReceiver() {

    private var mMeasurePageShouldClose: Boolean

    init {
        mMeasurePageShouldClose = false
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (Extra.ACTION_TERMINATE.tag == intent.action) {
            mMeasurePageShouldClose = true
        }
    }

    fun setup() {
        val terminateAndFinishNeededFilter = IntentFilter(Extra.ACTION_TERMINATE.tag)
        mFragment.activity?.registerReceiver(this, terminateAndFinishNeededFilter)
    }

    fun tearDown() {
        mFragment.activity?.unregisterReceiver(this)
    }

    fun shouldMeasurePageClose(): Boolean {
        return mMeasurePageShouldClose
    }
}