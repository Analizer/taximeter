package com.taximeter.fragments.measure.di

import android.view.Window
import androidx.fragment.app.Fragment
import com.taximeter.fragments.measure.modules.MeasureMap
import com.taximeter.fragments.measure.modules.MeasureMapImpl
import com.taximeter.fragments.measure.modules.MeasurePlanDataModule
import com.taximeter.fragments.measure.modules.MeasureServiceConnection
import com.taximeter.fragments.measure.mvp.MeasureContract
import com.taximeter.fragments.measure.mvp.MeasureModel
import com.taximeter.fragments.measure.mvp.MeasurePresenter
import com.taximeter.fragments.measure.mvp.MeasureView
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.service.measure.MeasureFunctions
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class MeasureFragmentModule {

    @Binds
    abstract fun presenter(measurePresenter: MeasurePresenter): MeasureContract.Presenter

    @Binds
    abstract fun model(model: MeasureModel): MeasureContract.Model

    @Binds
    abstract fun measureFunctions(presenter: MeasureContract.Presenter): MeasureFunctions

    @Binds
    abstract fun map(map: MeasureMapImpl): MeasureMap

    @Binds
    abstract fun view(view: MeasureView): MeasureContract.View

    companion object {

        @Provides
        fun measureServiceConnection(fragment: Fragment, planDataHelper: MeasurePlanDataModule): MeasureServiceConnection {
            val directionModel = if (fragment.arguments != null) fragment.requireArguments().getParcelable<DirectionModel>(Extra.TAG_DIRECTIONS.tag) else null
            return MeasureServiceConnection(planDataHelper, directionModel, fragment.requireContext())
        }

        @Provides
        fun window(fragment: Fragment): Window {
            return fragment.requireActivity().window
        }
    }
}