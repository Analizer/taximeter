package com.taximeter.fragments.measure.mvp

import android.view.View
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.taximeter.utils.UiUtils.animateInView
import com.taximeter.utils.UiUtils.animateOutView
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_measure_map.view.*
import javax.inject.Inject

@FragmentScoped
class MeasureView @Inject constructor(private val mPresenter: MeasureContract.Presenter,
                                      private val mProgressBarModule: ProgressBarModule)
    : MeasureContract.View {

    private lateinit var mCurrentLocationButton: FloatingActionButton

    override fun bind(pageView: View) {
        mProgressBarModule.bind(pageView)
        mCurrentLocationButton = pageView.current_location_button
        mCurrentLocationButton.setOnClickListener { mPresenter.handleCurrentLocationClicked() }
        val startBtn = pageView.start_btn
        startBtn.setOnClickListener { mPresenter.handleStopButtonClicked() }
    }

    override fun showProgress() {
        mProgressBarModule.showProgress()
    }

    override fun showCurrentLocationButton() {
        animateInView(mCurrentLocationButton)
    }

    override fun hideCurrentLocationButton() {
        animateOutView(mCurrentLocationButton)
    }
}