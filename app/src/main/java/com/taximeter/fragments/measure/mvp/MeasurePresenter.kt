package com.taximeter.fragments.measure.mvp

import android.location.Address
import com.patloew.rxlocation.RxLocation
import com.taximeter.fragments.measure.modules.*
import com.taximeter.persistence.database.DatabaseModule
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.AddressHandler
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.extras.Language
import com.taximeter.utils.location.LocationEnabler
import com.taximeter.utils.navigetion.FragmentNavigator
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

@FragmentScoped
class MeasurePresenter @Inject constructor(private val mCancelComponent: MeasureCancelModule,
                                           private val mMap: MeasureMap,
                                           private var mModel: MeasureContract.Model,
                                           private val mFragmentNavigator: FragmentNavigator,
                                           private val mWindowHelper: MeasureWindowHelperModule,
                                           private val mPageShouldCloseReceiver: MeasurePageShouldCloseReceiver,
                                           private val mLocationEnabler: LocationEnabler,
                                           private val mPlanDataHelper: MeasurePlanDataModule,
                                           private val mMeasureServiceConnection: MeasureServiceConnection,
                                           private val mReporter: Reporter,
                                           private val mDialogFactory: DialogFactory,
                                           private val mRxLocation: RxLocation,
                                           private val mHeaderHelper: MeasureHeaderModule,
                                           private val mSharedPref: SharedPref,
                                           private val mMeasureServiceStateChecker: MeasureServiceStateChecker,
                                           private val mAddressHandler: AddressHandler,
                                           private val mDatabase: DatabaseModule)
    : MeasureContract.Presenter {

    lateinit var mView: MeasureContract.View
    var mFinalizerDisposable: Disposable? = null

    override fun onViewAvailable(view: MeasureContract.View) {
        mView = view
        mCancelComponent.onViewAvailable()
    }

    override fun onResume() {
        if (mPageShouldCloseReceiver.shouldMeasurePageClose()) {
            mFragmentNavigator.goBack()
        } else {
            init()
        }
    }

    override fun onStop() {
        mMeasureServiceConnection.unSubscribeFromMeasureService()
        mWindowHelper.clearScreenAwake()
        mMap.reset()
        mCancelComponent.onViewUnavailable()
        mFinalizerDisposable?.dispose()
    }

    override fun onDestroyView() {
        mMap.tearDown()
        mPageShouldCloseReceiver.tearDown()
        mDatabase.tearDown()
    }

    override fun onMapReady() {
        mMap.zoomToBudapest()
        mLocationEnabler.check(
                onGranted = {
                    mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.LocationPermissionGranted)
                    mMap.setMyLocationEnabled()
                    mMeasureServiceConnection.connect()
                },
                onDenied = {
                    mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.LocationPermissionDenied)
                    mFragmentNavigator.goBack()
                })
    }

    override fun onTime(seconds: Long) {
        mHeaderHelper.updateTime(seconds)
    }

    override fun onTripUpdated(model: MeasureContract.Model) {
        mModel = model
        if (!model.points.isEmpty()) {
            mMap.update(model.points, model.trip.departureTime, false)
        }
        mHeaderHelper.update(model.trip)
    }

    override fun onTerminateProgress() {
        mFragmentNavigator.goBack()
    }

    override fun handleCurrentLocationClicked() {
        mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.CurrentLocationOnMap)
        mView.hideCurrentLocationButton()
        mMap.handleCurrentLocationClicked()
    }

    override fun onMapMovedByUser() {
        mView.showCurrentLocationButton()
    }

    override fun onMapImageCaptured(imageKey: String?) {
        mModel.trip.mapThumbnailString = imageKey
        mDatabase.saveTrip(mModel.trip) { mFragmentNavigator.goToTripDetailsFromMeasure(mModel.trip) }
    }

    override fun onRequestPermissionsResult() {
        mLocationEnabler.onRequestPermissionsResult()
    }

    override fun handleStopButtonClicked() {
        if (mModel.isTripStarted() && mMeasureServiceStateChecker.isMeasuring()) {
            mDialogFactory.showDialogIfNeeded(DialogType.STOP_MEASUREMENT,
                    { stopProgress() },
                    { mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.StopMeasurementCancelled) })
        } else {
            throw IllegalArgumentException("Stop button should not be shown on the Measure page when trip started: ${mModel.isTripStarted()} and is measuring: ${mMeasureServiceStateChecker.isMeasuring()}")
        }
    }

    private fun init() {
        mMap.init(this)
        mPageShouldCloseReceiver.setup()
        mHeaderHelper.reset()
        setScreenStayAwakeState()
        mView.showProgress()
        mMeasureServiceConnection.register(this)
    }

    private fun stopProgress() {
        mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.StopMeasurement)
        mMeasureServiceConnection.stopService()
        mPlanDataHelper.tearDown()
        finalizeTrip()
    }

    private fun finalizeTrip() {
        val locale = Locale(Language.currentAppLanguage.languageCode)
        val sourceAddressObservable = mRxLocation.geocoding().fromLocation(locale, mModel.points[0].latitude, mModel.points[0].longitude, 1).toObservable()
        val destinationAddressObservable = mRxLocation.geocoding().fromLocation(locale, mModel.points[mModel.points.size - 1].latitude, mModel.points[mModel.points.size - 1].longitude, 1).toObservable()
        mFinalizerDisposable = Observable.combineLatest<List<Address>, List<Address>, Unit>(
                sourceAddressObservable,
                destinationAddressObservable,
                BiFunction { sourceAddress, destinationAddress ->
                    mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.FinalizeTrip)
                    val source = if (sourceAddress.isEmpty()) "-" else mAddressHandler.getStopByAddress(sourceAddress[0]).getText()
                    val destination = if (destinationAddress.isEmpty()) "-" else mAddressHandler.getStopByAddress(destinationAddress[0]).getText()
                    mModel.finish(source, destination)
                }
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mMap.update(mModel.points, mModel.trip.departureTime, true) }
    }

    private fun setScreenStayAwakeState() {
        if (mSharedPref.getBoolean(Preference.SCREEN_AWAKE, true)) {
            mWindowHelper.setScreenAwake()
        } else {
            mWindowHelper.clearScreenAwake()
        }
    }
}
