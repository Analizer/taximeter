package com.taximeter.fragments;

import androidx.fragment.app.Fragment;

import com.taximeter.R;
import com.taximeter.fragments.company.details.CompanyDetailsFragment;
import com.taximeter.fragments.company.list.CompanyListFragment;
import com.taximeter.fragments.mainPage.MainPageFragment;
import com.taximeter.fragments.measure.MeasureFragment;
import com.taximeter.fragments.plan.addresspickermap.AddressPickerMapFragment;
import com.taximeter.fragments.plan.autosuggest.AutoSuggestFragment;
import com.taximeter.fragments.plan.favorites.FavoritesFragment;
import com.taximeter.fragments.plan.main.PlanFragment;
import com.taximeter.fragments.plan.recents.RecentsFragment;
import com.taximeter.fragments.settings.SettingsFragment;
import com.taximeter.fragments.staticpage.PrivacyPolicyFragment;
import com.taximeter.fragments.staticpage.RulesFragment;
import com.taximeter.fragments.staticpage.TermsAndConditionsFragment;
import com.taximeter.fragments.staticpage.WhatToDoFragment;
import com.taximeter.fragments.trip.tripdetails.TripDetailsFragment;
import com.taximeter.fragments.trip.tripslist.TripListFragment;


public enum FragmentType {

    MAIN_PAGE(1, R.string.action_bar_main_page, R.string.screen_main_page, R.drawable.home, R.drawable.home_black, false),
    MEASURE(2, R.string.drawer_name_measure, R.string.screen_measure_route_map, R.drawable.taxi, R.drawable.taxi_black, true),
    CALL(3, R.string.drawer_name_order, R.string.screen_taxi_company_list, R.drawable.call, R.drawable.call_black, false),
    PLAN(4, R.string.drawer_name_plan, R.string.screen_plan_route, R.drawable.directions, R.drawable.directions_black, true),
    TRIPS_LIST(5, R.string.drawer_name_saved_trips, R.string.screen_saved_trip_list, R.drawable.folder, R.drawable.folder_black, false),
    WHAT_TO_DO(6, R.string.drawer_name_what_to_do, R.string.screen_what_to_do, R.drawable.help, R.drawable.help_black, false),
    RULES(7, R.string.drawer_name_rules_of_calculation, R.string.screen_rules_of_calculation, R.drawable.info, R.drawable.info_black, false),
    SETTINGS(8, R.string.drawer_name_settings, R.string.screen_settings, R.drawable.settings, R.drawable.settings_black, false),
    AUTO_SUGGEST(-1, -1, R.string.screen_plan_route_autosuggest, true),
    ADDRESS_PICKER_MAP(-1, R.string.action_bar_title_plan_map, R.string.screen_plan_route_map, true),
    FAVORITES(-1, R.string.plan_favorites, R.string.plan_favorites, false),
    RECENT_PLANS(-1, R.string.plan_use_recents, R.string.plan_use_recents, false),
    TRIP_DETAILS(-1, R.string.action_bar_title_trip_details, R.string.screen_trip_details, false),
    PRIVACY_POLICY(-1, R.string.privacy_policy, R.string.privacy_policy, false),
    TERMS_AND_CONDIOTIONS(-1, R.string.terms_and_conditions, R.string.terms_and_conditions, false),
    COMPANY_DETAIL(-1, -1, R.string.screen_taxi_company_details, false);

    private int mId;
    private int mTitleId;
    private int mAnalyticsId;
    private int mIconId;
    private int mIconIdActive;
    private boolean mNetworkNeeded;

    FragmentType(int id, int titleId, int analyticsId, int iconId, int iconIdActive, boolean networkNeeded) {
        mTitleId = titleId;
        mId = id;
        mAnalyticsId = analyticsId;
        mIconId = iconId;
        mIconIdActive = iconIdActive;
        mNetworkNeeded = networkNeeded;
    }

    FragmentType(int id, int titleId, int analyticsId, boolean networkNeeded) {
        mId = id;
        mTitleId = titleId;
        mAnalyticsId = analyticsId;
        mNetworkNeeded = networkNeeded;
    }

    public int getId() {
        return mId;
    }

    public int getTitleId() {
        return mTitleId;
    }

    public int getAnalyticsId() {
        return mAnalyticsId;
    }

    public int getIconId() {
        return mIconId;
    }

    public int getmIconIdActive() {
        return mIconIdActive;
    }

    public boolean isNetworkNeeded() {
        return mNetworkNeeded;
    }

    public boolean hasTitle() {
        return mTitleId != -1;
    }

    public boolean hasDrawer() {
        switch (this) {
            case MAIN_PAGE:
            case MEASURE:
            case CALL:
            case PLAN:
            case RULES:
            case WHAT_TO_DO:
            case SETTINGS:
            case TRIPS_LIST:
                return true;
            default:
                return false;
        }
    }

    public static FragmentType getTypeFromIdentifier(long identifier) {
        for (FragmentType type : values()) {
            if (type.getId() == identifier) {
                return type;
            }
        }
        return MAIN_PAGE;
    }

    public Fragment getFragment() {
        switch (this) {
            case MAIN_PAGE:
                return new MainPageFragment();
            case MEASURE:
                return new MeasureFragment();
            case CALL:
                return new CompanyListFragment();
            case PLAN:
                return new PlanFragment();
            case COMPANY_DETAIL:
                return new CompanyDetailsFragment();
            case TRIP_DETAILS:
                return new TripDetailsFragment();
            case TRIPS_LIST:
                return new TripListFragment();
            case SETTINGS:
                return new SettingsFragment();
            case WHAT_TO_DO:
                return new WhatToDoFragment();
            case PRIVACY_POLICY:
                return new PrivacyPolicyFragment();
            case TERMS_AND_CONDIOTIONS:
                return new TermsAndConditionsFragment();
            case RULES:
                return new RulesFragment();
            case AUTO_SUGGEST:
                return new AutoSuggestFragment();
            case ADDRESS_PICKER_MAP:
                return new AddressPickerMapFragment();
            case FAVORITES:
                return new FavoritesFragment();
            case RECENT_PLANS:
                return new RecentsFragment();
            default:
                return null;
        }
    }
}
