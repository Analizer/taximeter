package com.taximeter.fragments.mainPage.mvp

interface MainPageContract {

    interface Presenter {

         fun onViewAvailable(view : View)

        fun onResume()

        fun onCallClicked()

        fun onMeasureClicked()

        fun onPlanClicked()

        fun setStartMeasurementText()

        fun tearDown()
    }

    interface View {

        fun bind(pageView: android.view.View)

        fun updateMeasureButtonText(isMeasuring: Boolean)
    }
}