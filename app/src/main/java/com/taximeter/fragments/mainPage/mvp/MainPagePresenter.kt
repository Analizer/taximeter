package com.taximeter.fragments.mainPage.mvp

import com.taximeter.activity.main.DrawerModule
import com.taximeter.fragments.mainPage.MainPageTerminateReceiver
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.navigetion.FragmentNavigator
import javax.inject.Inject

class MainPagePresenter @Inject constructor(private val mSharedPref: SharedPref,
                                            private val mCloseReceiver: MainPageTerminateReceiver,
                                            private val mMeasureServiceStateChecker: MeasureServiceStateChecker,
                                            private val mDrawerModule: DrawerModule,
                                            private val mFragmentNavigator: FragmentNavigator,
                                            private val mDialogFactory: DialogFactory,
                                            private val mReporter: Reporter) : MainPageContract.Presenter {

    internal lateinit var mView: MainPageContract.View

    override fun onViewAvailable(view: MainPageContract.View) {
        mView = view
        openDrawerIfNeeded()
    }

    override fun onResume() {
        mCloseReceiver.init(this)
        updateMeasureButtonText()
    }

    override fun onCallClicked() {
        mFragmentNavigator.goToCompanyList()
    }

    override fun onMeasureClicked() {
        mFragmentNavigator.goToMeasure()
    }

    override fun onPlanClicked() {
        mFragmentNavigator.goToPlan()
    }

    override fun setStartMeasurementText() {
        mView.updateMeasureButtonText(false)
    }

    override fun tearDown() {
        mCloseReceiver.tearDown()
    }

    internal fun updateMeasureButtonText() {
        mView.updateMeasureButtonText(mMeasureServiceStateChecker.isMeasuring())
    }

    private fun openDrawerIfNeeded() {
        if (!mSharedPref.getBoolean(Preference.INITIAL_DRAWER, false)) {
            mDrawerModule.openDrawer()
            mSharedPref.saveBoolean(Preference.INITIAL_DRAWER, true)
        }
    }
}