package com.taximeter.fragments.mainPage

import androidx.fragment.app.Fragment
import com.taximeter.activity.main.DrawerModule
import com.taximeter.activity.main.MainActivity
import com.taximeter.fragments.mainPage.mvp.MainPageContract
import com.taximeter.fragments.mainPage.mvp.MainPagePresenter
import com.taximeter.fragments.mainPage.mvp.MainPageView
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class MainPageModule {

    @Binds
    internal abstract fun presenter(presenter: MainPagePresenter): MainPageContract.Presenter

    @Binds
    internal abstract fun view(view: MainPageView): MainPageContract.View

    companion object {
        @Provides
        fun drawerModule(fragment: Fragment): DrawerModule {
            return (fragment.activity as MainActivity).mDrawerController
        }
    }
}