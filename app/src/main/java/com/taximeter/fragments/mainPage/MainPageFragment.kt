package com.taximeter.fragments.mainPage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.mainPage.mvp.MainPageContract
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainPageFragment : Fragment(), Page {

    @Inject
    lateinit var mView: MainPageContract.View
    @Inject
    lateinit var mPresenter: MainPageContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_page, container, false)
        mView.bind(view)
        mPresenter.onViewAvailable(mView)
        return view
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.tearDown()
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.MAIN_PAGE
    }
}