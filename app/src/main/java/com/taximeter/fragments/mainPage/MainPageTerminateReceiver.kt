package com.taximeter.fragments.mainPage

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.fragment.app.Fragment
import com.taximeter.fragments.mainPage.mvp.MainPageContract
import com.taximeter.utils.extras.Extra
import javax.inject.Inject


class MainPageTerminateReceiver @Inject constructor(private val mFragment: Fragment)
    : BroadcastReceiver() {

    private var mPresenter: MainPageContract.Presenter? = null

    fun init(presenter: MainPageContract.Presenter) {
        mPresenter = presenter
        val terminateAndFinishNeededFilter = IntentFilter(Extra.ACTION_TERMINATE.tag)
        mFragment.requireActivity().registerReceiver(this, terminateAndFinishNeededFilter)
    }

    fun tearDown() {
        mPresenter = null
        try {
            mFragment.requireActivity().unregisterReceiver(this)
        } catch (e: IllegalArgumentException) {
            //TODO ugly hack :( -> happens when we go to the Autosuggest page, the @MainActivity is destroyed and we hit back
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (Extra.ACTION_TERMINATE.tag == intent?.action) {
            mPresenter?.setStartMeasurementText()
        }
    }
}
