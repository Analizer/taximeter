package com.taximeter.fragments.mainPage.mvp

import android.view.View
import android.widget.Button
import com.taximeter.R
import kotlinx.android.synthetic.main.fragment_main_page.view.*
import javax.inject.Inject

class MainPageView @Inject constructor(private val mPresenter: MainPageContract.Presenter) : MainPageContract.View {

    private lateinit var callBtn: Button
    private lateinit var startBtn: Button
    private lateinit var planBtn: Button

    override fun bind(pageView: View) {
        callBtn = pageView.callBtn
        startBtn = pageView.startBtn
        planBtn = pageView.planBtn

        initListeners()
    }

    override fun updateMeasureButtonText(isMeasuring: Boolean) {
        if (isMeasuring) {
            startBtn.setText(R.string.main_page_continue)
        } else {
            startBtn.setText(R.string.main_page_start)
        }
    }

    private fun initListeners() {
        planBtn.setOnClickListener { mPresenter.onPlanClicked() }
        callBtn.setOnClickListener { mPresenter.onCallClicked() }
        startBtn.setOnClickListener { mPresenter.onMeasureClicked() }
    }
}