package com.taximeter.fragments.company.list

import android.Manifest
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.DeviceUtils.callNumber
import com.taximeter.utils.DeviceUtils.checkPermission
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class Caller @Inject constructor(private val mFragment: Fragment,
                                 private val mReporter: Reporter) {

    private lateinit var mNumberToCall: String

    fun onPermissionGranted() {
        makeCall()
    }

    fun call(phoneNumber: String, toReport: AnalyticsCategory) {
        mNumberToCall = phoneNumber
        mReporter.reportEvent(toReport, AnalyticsAction.Call)
        if (checkPermission(mFragment , Manifest.permission.CALL_PHONE, Extra.PERMISSION_REQUEST_CALL_PHONE.tag.toInt())) {
            makeCall()
        }
    }

    private fun makeCall() {
        callNumber(mFragment.context, mNumberToCall)
        val intent = Intent()
        intent.action = Intent.ACTION_CALL
        intent.data = Uri.parse("tel:$mNumberToCall")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        mFragment.context!!.startActivity(intent)
    }
}