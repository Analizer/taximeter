package com.taximeter.fragments.company.details

import com.taximeter.fragments.company.list.mvp.Company

interface CompanyDetailsContract {

    interface Presenter {
        fun onViewAvailable()

        fun phoneNumberClicked(phoneNumber: String)

        fun call()

        fun emailClicked()

        fun webPageClicked()

        fun addressClicked()
    }

    interface Model {
        val company: Company
    }

    interface View {
        fun bind(pageView: android.view.View)
    }
}