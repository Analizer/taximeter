package com.taximeter.fragments.company.details.mvp

import com.taximeter.fragments.company.details.CompanyDetailsContract
import com.taximeter.fragments.company.details.CompanyDetailsEventsModule
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CompanyDetailsPresenter @Inject constructor(
        private val mReporter: Reporter,
        private val mModel: CompanyDetailsContract.Model,
        private val mEventsHandler: CompanyDetailsEventsModule)
    : CompanyDetailsContract.Presenter {

    lateinit var mNumberToCall: String

    override fun onViewAvailable() {
        mEventsHandler.setupTitle()
    }

    override fun phoneNumberClicked(phoneNumber: String) {
        mReporter.reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.Call, mModel.company.analyticsLabel)
        mNumberToCall = phoneNumber
        call()
    }

    override fun call() {
        mEventsHandler.call(mNumberToCall)
    }

    override fun emailClicked() {
        mReporter.reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.Mail, mModel.company.analyticsLabel)
        mEventsHandler.writeEmail()
    }

    override fun webPageClicked() {
        mReporter.reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.WebPage, mModel.company.analyticsLabel)
        mEventsHandler.openWebPage()
    }

    override fun addressClicked() {
        mReporter.reportEvent(AnalyticsCategory.CompanyDetails, AnalyticsAction.CompanyAddress, mModel.company.analyticsLabel)
        mEventsHandler.navigateToAddress()
    }
}