package com.taximeter.fragments.company.list.di

import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.company.list.CompanyListAdapter
import com.taximeter.fragments.company.list.CompanyListViewHolder
import com.taximeter.fragments.company.list.mvp.CompanyListContract
import com.taximeter.fragments.company.list.mvp.CompanyListModel
import com.taximeter.fragments.company.list.mvp.CompanyListPresenter
import com.taximeter.fragments.company.list.mvp.CompanyListView
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class CompanyListModule {

    @Binds
    internal abstract fun presenter(presenter: CompanyListPresenter): CompanyListContract.Presenter

    @Binds
    internal abstract fun view(view: CompanyListView): CompanyListContract.View

    @Binds
    internal abstract fun model(model: CompanyListModel): CompanyListContract.Model

    @Binds
    internal abstract fun adapter(adapter: CompanyListAdapter): RecyclerView.Adapter<CompanyListViewHolder>
}
