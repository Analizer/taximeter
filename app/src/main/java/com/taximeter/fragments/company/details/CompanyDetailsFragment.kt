package com.taximeter.fragments.company.details

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CompanyDetailsFragment : Fragment(), Page {

    @Inject
    lateinit var mPresenter: CompanyDetailsContract.Presenter

    @Inject
    lateinit var mView: CompanyDetailsContract.View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_company_details, container, false)
        mView.bind(pageLayout)
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.COMPANY_DETAIL
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onViewAvailable()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Extra.PERMISSION_REQUEST_CALL_PHONE.tag.toInt() && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPresenter.call()
        }
    }
}
