package com.taximeter.fragments.company.details.di

import androidx.fragment.app.Fragment
import com.taximeter.fragments.company.details.CompanyDetailsContract
import com.taximeter.fragments.company.details.mvp.CompanyDetailsModel
import com.taximeter.fragments.company.details.mvp.CompanyDetailsPresenter
import com.taximeter.fragments.company.details.mvp.CompanyDetailsView
import com.taximeter.fragments.company.list.mvp.Company
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@InstallIn(FragmentComponent::class)
@Module
abstract class CompanyDetailsModule {

    @Binds
    internal abstract fun presenter(presenter: CompanyDetailsPresenter): CompanyDetailsContract.Presenter

    @Binds
    internal abstract fun model(model: CompanyDetailsModel): CompanyDetailsContract.Model

    @Binds
    internal abstract fun view(view: CompanyDetailsView): CompanyDetailsContract.View

    companion object {

        @Provides
        @FragmentScoped
        fun company(fragment: Fragment): Company {
            return fragment.requireArguments().getSerializable(Extra.TAG_COMPANY.tag) as Company
        }
    }
}