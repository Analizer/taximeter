package com.taximeter.fragments.company.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_taxi_company_list.view.*

class CompanyListViewHolder(var root: View) : RecyclerView.ViewHolder(root) {
    var callButton: ImageView = root.taxi_company_call_btn
    var nameTextView: TextView = root.taxi_company_name
    var taxiCompanyIcn: ImageView = root.taxi_company_icn
}
