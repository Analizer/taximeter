package com.taximeter.fragments.company.list.mvp

import com.taximeter.R
import com.taximeter.reporting.AnalyticsLabel
import javax.inject.Inject

class CompanyListModel @Inject constructor() : CompanyListContract.Model {

    private val companies: MutableList<Company> = mutableListOf()

    init {
        companies += Company(R.string.name_city, R.string.mail_city, R.string.address_city, R.string.web_city, R.drawable.logo_city, R.array.phone_city, AnalyticsLabel.City)
        companies += Company(R.string.name_fo, R.string.mail_fo, R.string.address_fo, R.string.web_fo, R.drawable.logo_fotaxi, R.array.phone_fo, AnalyticsLabel.Fo)
        companies += Company(R.string.name_taxiplus, R.string.mail_taxiplus, R.string.address_taxiplus, R.string.web_taxiplus, R.drawable.logo_taxiplus, R.array.phone_taxiplus, AnalyticsLabel.Plus)
        companies += Company(R.string.name_hatszorhat, R.string.mail_hatszorhat, R.string.address_hatszorhat, R.string.web_hatszorhat, R.drawable.logo_6x6, R.array.phone_6x6, AnalyticsLabel.Hatszorhat)
        companies += Company(R.string.name_budapest, R.string.mail_budapest, R.string.address_budapest, R.string.web_budapest, R.drawable.logo_budapest, R.array.phone_budapest, AnalyticsLabel.Budapest)
        companies += Company(R.string.name_taxi4, R.string.mail_taxi4, R.string.address_taxi4, R.string.web_taxi4, R.drawable.logo_taxi4, R.array.phone_taxi4, AnalyticsLabel.Taxi4)
        companies += Company(R.string.name_tele5, R.string.mail_tele5, R.string.address_tele5, R.string.web_tele5, R.drawable.logo_tele5, R.array.phone_tele5, AnalyticsLabel.Tele5)
        companies += Company(R.string.name_elit, R.string.mail_elit, R.string.address_elit, R.string.web_elit, R.drawable.logo_elit, R.array.phone_elit, AnalyticsLabel.Elit)
    }

    override fun size(): Int {
        return companies.size
    }

    override fun get(index: Int): Company {
        return companies[index]
    }
}