package com.taximeter.fragments.company.list

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.R
import com.taximeter.fragments.company.list.mvp.CompanyListContract
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CompanyListAdapter @Inject constructor(private val mResources: Resources,
                                             private val mModel: CompanyListContract.Model)
    : RecyclerView.Adapter<CompanyListViewHolder>() {

    private lateinit var mPresenter: CompanyListContract.Presenter

    fun init(presenter: CompanyListContract.Presenter) {
        mPresenter = presenter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyListViewHolder {
        val row = LayoutInflater.from(parent.context).inflate(R.layout.row_taxi_company_list, parent, false)
        return CompanyListViewHolder(row)
    }

    override fun onBindViewHolder(holder: CompanyListViewHolder, position: Int) {
        val company = mModel.get(position)
        holder.nameTextView.text = mResources.getString(company.nameId)
        holder.taxiCompanyIcn.setImageResource(company.logoId)
        holder.callButton.setOnClickListener { mPresenter.onCallButtonClicked(mResources.getStringArray(company.phoneNumbersId), company.analyticsLabel) }
        holder.root.setOnClickListener { mPresenter.onCompanyClicked(company) }
    }

    override fun getItemCount(): Int {
        return mModel.size()
    }
}
