package com.taximeter.fragments.company.details.mvp

import com.taximeter.fragments.company.details.CompanyDetailsContract
import com.taximeter.fragments.company.list.mvp.Company
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
data class CompanyDetailsModel @Inject constructor(override val company: Company) : CompanyDetailsContract.Model