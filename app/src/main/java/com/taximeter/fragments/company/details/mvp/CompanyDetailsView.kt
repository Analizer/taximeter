package com.taximeter.fragments.company.details.mvp

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.taximeter.R
import com.taximeter.fragments.company.details.CompanyDetailsContract
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_company_details.view.*
import javax.inject.Inject

@FragmentScoped
class CompanyDetailsView @Inject constructor(
        private val mPresenter: CompanyDetailsContract.Presenter,
        private val mModel: CompanyDetailsContract.Model,
        private val mInflater: LayoutInflater,
        private val mResources: Resources)
    : CompanyDetailsContract.View {

    private lateinit var mEmailTv: TextView
    private lateinit var mWebPageTv: TextView
    private lateinit var mAddressTv: TextView

    override fun bind(pageView: View) {
        mEmailTv = pageView.email
        mWebPageTv = pageView.webpage
        mAddressTv = pageView.address

        val nameTv = pageView.name
        val companyIcnView = pageView.logo
        val phoneNumbersHolder = pageView.phone_holder

        nameTv.setText(mModel.company.nameId)
        mEmailTv.setText(mModel.company.emailId)
        mWebPageTv.setText(mModel.company.webId)
        mAddressTv.setText(mModel.company.addressId)

        companyIcnView.setImageResource(mModel.company.logoId)

        addPhoneNumberRows(phoneNumbersHolder)

        setupListeners()
    }

    private fun addPhoneNumberRows(phoneNumbersHolder: LinearLayout) {
        val separatorParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1)
        for (phoneNumber in mResources.getStringArray(mModel.company.phoneNumbersId)) {
            val phoneTv = mInflater.inflate(R.layout.row_taxi_company_details_phone_row, null, false) as TextView
            val separator = mInflater.inflate(R.layout.separator, null, false)
            separator.layoutParams = separatorParams
            phoneTv.text = phoneNumber
            phoneTv.setOnClickListener { mPresenter.phoneNumberClicked(phoneTv.text.toString()) }
            phoneNumbersHolder.addView(phoneTv)
            phoneNumbersHolder.addView(separator)
        }
    }

    private fun setupListeners() {
        mEmailTv.setOnClickListener { mPresenter.emailClicked() }
        mWebPageTv.setOnClickListener { mPresenter.webPageClicked() }
        mAddressTv.setOnClickListener { mPresenter.addressClicked() }
    }
}