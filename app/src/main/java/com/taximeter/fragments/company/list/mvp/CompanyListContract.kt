package com.taximeter.fragments.company.list.mvp

import com.taximeter.reporting.AnalyticsLabel

interface CompanyListContract {

    interface Presenter {
        fun onViewAvailable(view: View)

        fun onResume()

        fun onCallButtonClicked(phoneNumbers: Array<String>, toReport: AnalyticsLabel)

        fun onCompanyClicked(company: Company)

        fun onCallPermissionGranted()
    }

    interface Model {
        fun size(): Int

        fun get(index: Int): Company
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun setup()
    }
}