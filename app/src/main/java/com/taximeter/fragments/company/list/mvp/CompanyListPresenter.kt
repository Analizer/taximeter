package com.taximeter.fragments.company.list.mvp

import com.taximeter.fragments.company.list.Caller
import com.taximeter.fragments.company.list.CompanyListAdapter
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.navigetion.FragmentNavigator
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CompanyListPresenter @Inject constructor(
        private val mAdapter: CompanyListAdapter,
        private val mCaller: Caller,
        private val mFragmentNavigator: FragmentNavigator,
        private val mDialogFactory: DialogFactory,
        private val mReporter: Reporter)
    : CompanyListContract.Presenter {

    private lateinit var mView: CompanyListContract.View

    override fun onViewAvailable(view: CompanyListContract.View) {
        mView = view
    }

    override fun onResume() {
        mAdapter.init(this)
        mView.setup()
    }

    override fun onCallButtonClicked(phoneNumbers: Array<String>, toReport: AnalyticsLabel) {
        mReporter.reportEvent(AnalyticsCategory.CompanyList, AnalyticsAction.Call, toReport)
        if (phoneNumbers.size > 1) {
            mDialogFactory.showPhoneNumbersDialog(phoneNumbers) { number -> mCaller.call(number, AnalyticsCategory.CompanyList) }
        } else {
            mCaller.call(phoneNumbers[0], AnalyticsCategory.CompanyList)
        }
    }

    override fun onCompanyClicked(company: Company) {
        mReporter.reportEvent(AnalyticsCategory.CompanyList, AnalyticsAction.CompanyDetailsOpen, company.analyticsLabel)
        mFragmentNavigator.goToCompanyDetails(company)
    }

    override fun onCallPermissionGranted() {
        mCaller.onPermissionGranted()
    }
}