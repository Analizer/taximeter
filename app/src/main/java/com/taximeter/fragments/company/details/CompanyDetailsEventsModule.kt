package com.taximeter.fragments.company.details

import android.Manifest
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import com.taximeter.activity.main.MainActivity
import com.taximeter.utils.DeviceUtils
import com.taximeter.utils.DeviceUtils.checkPermission
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CompanyDetailsEventsModule @Inject constructor(
        private val mFragment: Fragment,
        private val mModel: CompanyDetailsContract.Model) {

    fun call(phoneNumber: String) {
        if (checkPermission(mFragment as Fragment, Manifest.permission.CALL_PHONE, Extra.PERMISSION_REQUEST_CALL_PHONE.tag.toInt())) {
            DeviceUtils.callNumber(mFragment.context, phoneNumber)
        }
    }

    fun openWebPage() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(mFragment.getString(mModel.company.webId))
        mFragment.startActivity(i)
    }

    fun writeEmail() {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf(mFragment.getString(mModel.company.emailId)))
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, "")
        mFragment.startActivity(Intent.createChooser(i, "Send mail..."))
    }

    fun navigateToAddress() {
        val uri = "geo:0,0?q=" + mFragment.getString(mModel.company.addressId)
        mFragment.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
    }

    fun setupTitle() {
        ((mFragment.activity) as MainActivity).supportActionBar!!.setTitle(mModel.company.nameId)
    }
}