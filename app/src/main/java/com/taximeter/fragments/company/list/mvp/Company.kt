package com.taximeter.fragments.company.list.mvp

import com.taximeter.reporting.AnalyticsLabel
import java.io.Serializable

data class Company constructor(internal val nameId: Int,
                               internal val emailId: Int,
                               internal val addressId: Int,
                               internal val webId: Int,
                               internal val logoId: Int,
                               internal val phoneNumbersId: Int,
                               internal val analyticsLabel: AnalyticsLabel)
    : Serializable