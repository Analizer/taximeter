package com.taximeter.fragments.company.list.mvp

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.company.list.CompanyListViewHolder
import com.taximeter.utils.touchhelper.NoMovementTouchHelperCallback
import com.taximeter.utils.ui.RecyclerViewSetup
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_company_list.view.*
import javax.inject.Inject

@FragmentScoped
class CompanyListView @Inject constructor(private val mRecyclerViewSetup: RecyclerViewSetup<CompanyListViewHolder>)
    : CompanyListContract.View {

    private lateinit var recyclerView: RecyclerView

    override fun bind(pageView: View) {
        recyclerView = pageView.recycler_view
    }

    override fun setup() {
        mRecyclerViewSetup.setup(recyclerView, NoMovementTouchHelperCallback())
    }
}