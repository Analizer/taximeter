package com.taximeter.fragments.settings

import com.taximeter.fragments.settings.mvp.SettingsContract
import com.taximeter.fragments.settings.mvp.SettingsPresenter
import com.taximeter.fragments.settings.mvp.SettingsView
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class SettingsModule {

    @Binds
    internal abstract fun presenter(presenter: SettingsPresenter): SettingsContract.Presenter

    @Binds
    internal abstract fun view(presenter: SettingsView): SettingsContract.View
}