package com.taximeter.fragments.settings.mvp

import android.view.View
import android.widget.CheckBox
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_settings.view.*
import javax.inject.Inject

@FragmentScoped
class SettingsView @Inject constructor(private val mPresenter: SettingsContract.Presenter) : SettingsContract.View {

    private lateinit var mHungarianCheck: CheckBox
    private lateinit var mEnglishCheck: CheckBox
    private lateinit var mTrafficCheck: CheckBox
    private lateinit var mScreenAwakeCheck: CheckBox
    private lateinit var mHungarianRow: View
    private lateinit var mEnglishRow: View
    private lateinit var mTrafficRow: View
    private lateinit var mScreenAwakeRow: View
    private lateinit var mPrivacyPolicyRow: View
    private lateinit var mTermsRow: View

    override fun bind(pageView: View) {
        mHungarianCheck = pageView.hungarian_cb
        mEnglishCheck = pageView.english_cb
        mTrafficCheck = pageView.traffic_cb
        mScreenAwakeCheck = pageView.screen_awake_cb

        mHungarianRow = pageView.hungarian_row
        mEnglishRow = pageView.english_row
        mTrafficRow = pageView.traffic_row
        mScreenAwakeRow = pageView.screen_awake_row
        mPrivacyPolicyRow = pageView.privacy_policy_row
        mTermsRow = pageView.terms_and_conditions_row
    }

    override fun initListeners() {
        mHungarianCheck.setOnCheckedChangeListener { _, isChecked -> mPresenter.hungarianCheckClicked(isChecked) }
        mEnglishCheck.setOnCheckedChangeListener { _, isChecked -> mPresenter.englishCheckClicked(isChecked) }
        mTrafficCheck.setOnCheckedChangeListener { _, isChecked -> mPresenter.trafficCheckClicked(isChecked) }
        mScreenAwakeCheck.setOnCheckedChangeListener { _, isChecked -> mPresenter.screeAwakeCheckClicked(isChecked) }

        mHungarianRow.setOnClickListener { mHungarianCheck.performClick() }
        mEnglishRow.setOnClickListener { mEnglishCheck.performClick() }
        mTrafficRow.setOnClickListener { mTrafficCheck.performClick() }
        mScreenAwakeRow.setOnClickListener { mScreenAwakeCheck.performClick() }

        mPrivacyPolicyRow.setOnClickListener { mPresenter.privacyPolicyClicked() }
        mTermsRow.setOnClickListener { mPresenter.termsAndConditionsClicked() }
    }

    override fun selectHungarian() {
        mHungarianCheck.isChecked = true
        mEnglishCheck.isChecked = false
    }

    override fun selectEnglish() {
        mHungarianCheck.isChecked = false
        mEnglishCheck.isChecked = true
    }

    override fun setTrafficEnabled(enabled: Boolean) {
        mTrafficCheck.isChecked = enabled
    }

    override fun setScreenAwakeEnabled(enabled: Boolean) {
        mScreenAwakeCheck.isChecked = enabled
    }
}