package com.taximeter.fragments.settings.mvp

interface SettingsContract {

    interface Presenter {
        fun onViewAvailable(view: View)

        fun hungarianCheckClicked(isChecked: Boolean)

        fun englishCheckClicked(isChecked: Boolean)

        fun trafficCheckClicked(isChecked: Boolean)

        fun screeAwakeCheckClicked(isChecked: Boolean)

        fun privacyPolicyClicked()

        fun termsAndConditionsClicked()

        fun setupLanguageSection()

        fun setupShowTraffic()

        fun setupScreenAwake()

        fun handleEnglishCheckChanged(isChecked: Boolean)

        fun handleHungarianCheckChanged(isChecked: Boolean)
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun initListeners()

        fun selectHungarian()

        fun selectEnglish()

        fun setTrafficEnabled(enabled: Boolean)

        fun setScreenAwakeEnabled(enabled: Boolean)
    }
}