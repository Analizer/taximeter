package com.taximeter.fragments.settings.mvp

import com.taximeter.activity.main.MainActivityFragmentHandler
import com.taximeter.fragments.FragmentType
import com.taximeter.persistence.sharedpref.Preference
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.LocaleManager
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.extras.Language
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class SettingsPresenter @Inject constructor(private val mSharedPref: SharedPref,
                                            private val mLocaleManager: LocaleManager,
                                            private val mReporter: Reporter,
                                            private val mMeasureServiceStateChecker: MeasureServiceStateChecker,
                                            private val mDialogFactory: DialogFactory,
                                            private val mFragmentHandler: MainActivityFragmentHandler)
    : SettingsContract.Presenter {

    internal lateinit var mView: SettingsContract.View

    override fun onViewAvailable(view: SettingsContract.View) {
        mView = view
        setupLanguageSection()
        setupShowTraffic()
        setupScreenAwake()
        mView.initListeners()
    }

    override fun setupLanguageSection() {
        val isHungarian = mLocaleManager.currentLanguage == Language.HUNGARIAN
        if (isHungarian) {
            mView.selectHungarian()
        } else {
            mView.selectEnglish()
        }
    }

    override fun setupShowTraffic() {
        mView.setTrafficEnabled(mSharedPref.getBoolean(Preference.TRAFFIC, false))
    }

    override fun setupScreenAwake() {
        mView.setScreenAwakeEnabled(mSharedPref.getBoolean(Preference.SCREEN_AWAKE, true))
    }

    override fun handleEnglishCheckChanged(isChecked: Boolean) {
        mView.selectEnglish()
        if (isChecked) {
            mReporter.reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToEnglish)
            mLocaleManager.setLocale(Language.ENGLISH)
        }
    }

    override fun handleHungarianCheckChanged(isChecked: Boolean) {
        mView.selectHungarian()
        if (isChecked) {
            mReporter.reportEvent(AnalyticsCategory.Settings, AnalyticsAction.LanguageSetToHungarian)
            mLocaleManager.setLocale(Language.HUNGARIAN)
        }
    }

    override fun hungarianCheckClicked(isChecked: Boolean) {
        if (!mMeasureServiceStateChecker.isMeasuring()) {
            handleHungarianCheckChanged(isChecked)
        } else {
            mDialogFactory.showDialogIfNeeded(DialogType.CHANGE_LANGUAGE_WHILE_MEASURING)
            setupLanguageSection()
        }
    }

    override fun englishCheckClicked(isChecked: Boolean) {
        if (!mMeasureServiceStateChecker.isMeasuring()) {
            handleEnglishCheckChanged(isChecked)
        } else {
            mDialogFactory.showDialogIfNeeded(DialogType.CHANGE_LANGUAGE_WHILE_MEASURING)
            setupLanguageSection()
        }
    }

    override fun trafficCheckClicked(isChecked: Boolean) {
        val action = if (isChecked) AnalyticsAction.UseTrafficOnMap else AnalyticsAction.DontUseTrafficOnMap
        mReporter.reportEvent(AnalyticsCategory.Settings, action)
        mSharedPref.saveBoolean(Preference.TRAFFIC, isChecked)
    }

    override fun screeAwakeCheckClicked(isChecked: Boolean) {
        val action = if (isChecked) AnalyticsAction.KeepScreenAwake else AnalyticsAction.DontKeepScreenAwake
        mReporter.reportEvent(AnalyticsCategory.Settings, action)
        mSharedPref.saveBoolean(Preference.SCREEN_AWAKE, isChecked)
    }

    override fun privacyPolicyClicked() {
        mReporter.reportEvent(AnalyticsCategory.Settings, AnalyticsAction.PrivacyPolicyClicked)
        mFragmentHandler.switchFragment(FragmentType.PRIVACY_POLICY)
    }

    override fun termsAndConditionsClicked() {
        mReporter.reportEvent(AnalyticsCategory.Settings, AnalyticsAction.TermsAndConditionsClicked)
        mFragmentHandler.switchFragment(FragmentType.TERMS_AND_CONDIOTIONS)
    }
}