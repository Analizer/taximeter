package com.taximeter.fragments.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.settings.mvp.SettingsContract
import dagger.android.support.DaggerFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingsFragment : Fragment(), Page {

    @Inject
    lateinit var mView: SettingsContract.View
    @Inject
    lateinit var mPresenter: SettingsContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_settings, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.SETTINGS
    }
}
