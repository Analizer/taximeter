package com.taximeter.fragments

import com.taximeter.reporting.AnalyticsLabel

interface ListListener<T> {

    fun onItemChanged(itemPosition: Int) {
        throw IllegalArgumentException("ListListener onItemChanged called while not expected!")
    }

    fun onItemClicked(item: T)

    fun deleteItem(position: Int, toReport: AnalyticsLabel)
}