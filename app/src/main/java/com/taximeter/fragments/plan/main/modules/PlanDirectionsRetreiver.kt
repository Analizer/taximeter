package com.taximeter.fragments.plan.main.modules

import com.taximeter.communication.DirectionsCommunicator
import com.taximeter.communication.endpoint.DirectionsEndpoint
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.disposables.Disposable
import retrofit2.Retrofit
import javax.inject.Inject

@FragmentScoped
class PlanDirectionsRetreiver @Inject constructor(private val mRetrofit: Retrofit.Builder,
                                                  private val mCommunicator: DirectionsCommunicator) {

    private var mDirectionsDisposable: Disposable? = null

    fun requestDirections(model: PlanContract.Model, onSuccess: (directionModel: DirectionModel) -> Unit, onError: () -> Unit) {
        model.updateDirections(null)
        val endpoint = mRetrofit.baseUrl(Extra.BASE_URL_DIRECTIONS.tag).build().create(DirectionsEndpoint::class.java)//TODO! move this to communicator
        mDirectionsDisposable = mCommunicator
                .getDirections(
                        endpoint,
                        listOf(model.source, model.destination),
                        { directionModel ->
                            model.updateDirections(directionModel)
                            onSuccess.invoke(directionModel)
                        },
                        onError)
    }

    fun tearDown() {
        mDirectionsDisposable?.dispose()
    }
}