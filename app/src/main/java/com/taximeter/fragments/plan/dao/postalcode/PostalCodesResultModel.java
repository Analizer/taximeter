package com.taximeter.fragments.plan.dao.postalcode;

import com.taximeter.utils.Validation;

import java.util.LinkedList;
import java.util.List;

public class PostalCodesResultModel {

    List<PostalCodeModel> postalCodes;

    public PostalCodesResultModel() {
        this.postalCodes = new LinkedList<>();
    }

    public String getPostalCode() {
        return Validation.notEmpty(postalCodes) ? postalCodes.get(0).getPostalCode() : "";
    }
}
