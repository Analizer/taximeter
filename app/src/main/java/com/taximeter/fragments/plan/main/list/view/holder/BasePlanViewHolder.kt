package com.taximeter.fragments.plan.main.list.view.holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.taximeter.R

open class BasePlanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val root: View = itemView.findViewById(R.id.plan_root)
    val arrowRow: PlanArrowViewHolder = PlanArrowViewHolder(itemView.findViewById(R.id.arrow_row))
}
