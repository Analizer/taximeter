package com.taximeter.fragments.plan.recents.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.R
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.recents.mvp.RecentsContract
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.utils.CalculationUtils.getFullDateString
import com.taximeter.utils.touchhelper.SwipeToDeleteCallback
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class RecentsAdapter @Inject constructor(private val mModel: RecentsContract.Model)
    : RecyclerView.Adapter<RecentsViewHolder>(), SwipeToDeleteCallback {

    private lateinit var mListener: ListListener<PlanContract.Model>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentsViewHolder {
        return RecentsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_recent_plan, parent, false))
    }

    override fun onBindViewHolder(holder: RecentsViewHolder, position: Int) {
        val plan = mModel.get(position)
        holder.root.setOnClickListener { mListener.onItemClicked(mModel.get(holder.adapterPosition)) }
        holder.titleTv.text = getFullDateString(plan.date)
        holder.contentTv.text = "${plan.source.getText()} -> ${plan.destination.getText()}" //TODO
        holder.deleteIcn.setOnClickListener { mListener.deleteItem(holder.adapterPosition, AnalyticsLabel.DeleteIcon) }
    }

    override fun getItemCount(): Int {
        return mModel.size()
    }

    fun init(listener: ListListener<PlanContract.Model>) {
        mListener = listener
    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder) {
        mListener.deleteItem(viewHolder.adapterPosition, AnalyticsLabel.Swipe)
    }
}
