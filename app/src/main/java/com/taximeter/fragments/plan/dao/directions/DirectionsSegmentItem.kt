package com.taximeter.fragments.plan.dao.directions

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DirectionsSegmentItem constructor(val text: String,
                                             val value: Int)
    : Parcelable
