package com.taximeter.fragments.plan.autosuggest.mvp

import com.taximeter.R
import com.taximeter.activity.main.ToolbarModule
import com.taximeter.fragments.plan.autosuggest.AutoSuggestCommunicator
import com.taximeter.fragments.plan.autosuggest.AutoSuggestTextWatcher
import com.taximeter.fragments.plan.autosuggest.header.AutoSuggestHeaderModule
import com.taximeter.fragments.plan.autosuggest.list.adapter.AutoSuggestAdapter
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.main.modules.CurrentAddressRetriever
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.location.LocationSubscriber
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.KeyboardHandler
import com.taximeter.utils.NetworkModule
import com.taximeter.utils.location.CurrentLocationRetriever
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@FragmentScoped
class AutoSuggestPresenter @Inject constructor(private val mReporter: Reporter,
                                               private val mAdapter: AutoSuggestAdapter,
                                               private val mCommunicator: AutoSuggestCommunicator,
                                               private val mTextWatcher: AutoSuggestTextWatcher,
                                               private val mNetworkModule: NetworkModule,
                                               private val mKeyboardHandler: KeyboardHandler,
                                               private val mNavigator: PlanNavigator,
                                               private val mToolbarModule: ToolbarModule,
                                               private val textWatcher: AutoSuggestTextWatcher,
                                               private val mModel: PlanContract.Model,
                                               private val mLocationRetriever: CurrentLocationRetriever,
                                               private val mAddressRetriever: CurrentAddressRetriever,
                                               private val mErrorHandler: ErrorHandler,
                                               private val mHeaderModule: AutoSuggestHeaderModule)
    : AutoSuggestContract.Presenter {

    private lateinit var mView: AutoSuggestContract.View
    private var mDisposable: Disposable? = null

    override fun onViewAvailable(view: AutoSuggestContract.View) {
        mView = view
        mLocationRetriever.addSubscriber(LocationSubscriber.AUTO_SUGGEST)
        mCommunicator.init(this)
        mTextWatcher.init(this)
        mAdapter.init(this)
    }

    override fun onResume() {
        mHeaderModule.bind(this)
        setupToolbar()
        showKeyboard()
        if (mModel.getItemToEdit().isRealStop()) mToolbarModule.loadSearchModel(mModel.getItemToEdit())//TODO ha vissza jovok ide, ne keressen
    }

    override fun tearDown() {
        mDisposable?.dispose()
        mCommunicator.stop()
        mLocationRetriever.tearDown()
        mToolbarModule.resetSearchSection()
    }

    override fun showProgress() {
        mView.showProgress()
    }

    override fun hideProgress() {
        mView.hideProgress()
    }

    override fun onCurrentLocation() {
        mAddressRetriever.requestAddress { stop -> onSingleResult(stop, AnalyticsAction.AutoSuggestCurrentLocation) }
    }

    override fun onFavorites() {
        hideKeyboard()
        mLocationRetriever.requestLocation(onSuccess = { location -> mNavigator.goToFavorites(mModel, location) })
    }

    override fun onRecents() {
        hideKeyboard()
        mNavigator.goToRecents(mModel)
    }

    override fun onMap() {
        hideKeyboard()
        mNavigator.goToAddressPickerMap(mModel)
    }

    override fun onAutoSuggestItem(stop: Stop) {
        hideKeyboard()
        mModel.updateItemToEdit(stop)
        mNavigator.returnToPlanPage(mModel)
    }

    override fun onSearchTextChanged(query: String) {
        mDisposable?.dispose()
        mCommunicator.stop()
        mAdapter.clear()
        if (query.trim().length >= 3) {
            showProgress()
            mDisposable = Observable
                    .interval(1, TimeUnit.SECONDS)
                    .firstElement()
                    .subscribe { requestResults(query) }
        } else {
            hideProgress()
        }
    }

    override fun onResult(results: MutableList<Stop>) {
        mReporter.reportEvent(AnalyticsCategory.AutoSuggest, AnalyticsAction.AutoSuggestResultsArrived)
        hideProgress()
        mAdapter.update(results)
    }

    override fun onSingleResult(result: Stop, toReport: AnalyticsAction) {
        mReporter.reportEvent(AnalyticsCategory.AutoSuggest, toReport)
        mModel.updateItemToEdit(result)
        mNavigator.returnToPlanPage(mModel)
    }

    override fun onResultError(toReport: AnalyticsAction, toShow: Int) {
        mErrorHandler.handleError(AnalyticsCategory.AutoSuggest, toReport, toShow)
    }

    private fun setupToolbar() {
        mToolbarModule.showSearchSection(
                textWatcher,
                { query: String ->
                    handleSearchButtonClicked(query)
                    mReporter.reportEvent(AnalyticsCategory.AutoSuggest, AnalyticsAction.AutoSuggestEdittextDeleteUsed)
                },
                {
                    showKeyboard()
                    mReporter.reportEvent(AnalyticsCategory.AutoSuggest, AnalyticsAction.AutoSuggestEdittextDeleteUsed)
                })
    }

    private fun requestResults(query: String) {
        if (mNetworkModule.isConnected()) {
            mLocationRetriever.requestLocation(onSuccess = { location -> mCommunicator.start(query, location) })
        } else {
            mErrorHandler.handleError(AnalyticsCategory.AutoSuggest, AnalyticsAction.InternetNotAvailable, R.string.error_message_no_internet)
        }
    }

    private fun handleSearchButtonClicked(query: String) {
        hideKeyboard()
        mCommunicator.stop()
        if (mNetworkModule.isConnected()) {
            mCommunicator.requestSingleResult(query)
        } else {
            mErrorHandler.handleError(AnalyticsCategory.AutoSuggest, AnalyticsAction.InternetNotAvailable, R.string.error_message_no_internet)
        }
    }

    private fun showKeyboard() {
        mKeyboardHandler.showKeyboard(mToolbarModule.mSearchField)
    }

    private fun hideKeyboard() {
        mKeyboardHandler.hideKeyboard(mToolbarModule.mSearchField)
    }

}