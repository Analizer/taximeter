package com.taximeter.fragments.plan.autosuggest.list.view

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.autosuggest_item_place.view.*

class AutoSuggestPlaceViewHolder(var root: View) : RecyclerView.ViewHolder(root) {
    var primaryText: TextView = root.autosuggest_primary_text
    var secondaryText: TextView = root.autosuggest_secondary_text
    var distance: TextView = root.autosuggest_distance
    var icon: ImageView = root.autosuggest_icon
}
