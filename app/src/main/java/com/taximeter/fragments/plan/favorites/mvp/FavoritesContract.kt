package com.taximeter.fragments.plan.favorites.mvp

import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.ListListener

interface FavoritesContract {

    interface Presenter : ListListener<Stop> {
        fun onViewAvailable(view: View)

        fun onResume()

        fun tearDown()
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun setup()

        fun showEmptyMessage()

        fun hideEmptyMessage()

        fun scrollToPosition(adapterPosition: Int)
    }

    interface Model {
        fun get(position: Int): Stop

        fun add(adapterPosition: Int, favorite: Stop)

        fun remove(position: Int)

        fun size(): Int

        fun init(elements: MutableList<Stop>?)
    }
}