package com.taximeter.fragments.plan.main.modules

import android.content.Intent
import android.location.Location
import com.taximeter.R
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.extras.Extra
import com.taximeter.utils.navigetion.FragmentNavigator
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanNavigator @Inject constructor(private val mFragmentNavigator: FragmentNavigator,
                                        private val mReporter: Reporter,
                                        private val mErrorHandler: ErrorHandler,
                                        private val mCalculator: GeoCalculator) {

    fun showAutoSuggest(model: PlanContract.Model) {
        mFragmentNavigator.goToAutoSuggest(model)
    }

    fun goToFavorites(model: PlanContract.Model, currentLocation: Location) {
        mFragmentNavigator.goToFavorites(model, currentLocation)
    }

    fun goToAddressPickerMap(model: PlanContract.Model) {
        mFragmentNavigator.goToAddressPickerMap(model)
    }

    fun goToRecents(model: PlanContract.Model) {
        mFragmentNavigator.goToRecents(model)
    }

    fun startMeasurement(model: PlanContract.Model, currentLocation: Location) {
        if (!isUserAtStartingPoint(model, currentLocation)) {
            mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.PlanCarFabUsed)
            mFragmentNavigator.goToMeasureFromPlan(model.getDirections()!!)
        } else {
            mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.PlanCarFabUseFailedAsUserWasNotAtStartingPoint, R.string.plan_car_error_message)
        }
    }

    fun returnToPlanPage(model: PlanContract.Model) {
        mFragmentNavigator.mFragment.activity!!.intent = Intent().putExtra(Extra.TAG_AUTO_SUGGEST_RESULT.tag, model)
        mFragmentNavigator.goBackToPlan()
    }

    private fun isUserAtStartingPoint(model: PlanContract.Model, currentLocation: Location): Boolean {
        return mCalculator.distance(currentLocation, model.get(0).position) < DISTANCE_TO_CURRENT_LOCATION_THRESHOLD//TODO
    }

    companion object {
        private const val DISTANCE_TO_CURRENT_LOCATION_THRESHOLD = 100
    }
}