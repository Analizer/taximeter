package com.taximeter.fragments.plan.main.list.view.component.address

interface PlanListAddressView {
    fun setText(text: String)

    fun setFavorite(isFavorite: Boolean)

    fun setFavoriteClickedListener(onClick: () -> Unit)

    fun setRootClickListener(onClick: () -> Unit)

    fun setDeleteClickedListener(onClick: () -> Unit)

    fun setDistance(distance: String)

    fun showArrow()

    fun hideArrow()

    fun clearArrow()

    fun setArrowDistance(distance: String)

    fun setArrowDuration(duration: String)
}

