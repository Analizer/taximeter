package com.taximeter.fragments.plan.dao.directions

import android.os.Parcelable
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.annotations.SerializedName
import com.google.maps.android.PolyUtil
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DirectionsRoutes constructor(@SerializedName("legs") val segments: List<DirectionsSegment>,
                                        @SerializedName("overview_polyline") private val polyline: DirectionsPolyLine)
    : Parcelable {

    @IgnoredOnParcel
    private var polylineOptionsNullable: PolylineOptions? = null

    val polylineOptions: PolylineOptions
        get() {
            if (polylineOptionsNullable == null) {
                polylineOptionsNullable = PolylineOptions().addAll(PolyUtil.decode(polyline.points))
            }
            return polylineOptionsNullable!!
        }
}
