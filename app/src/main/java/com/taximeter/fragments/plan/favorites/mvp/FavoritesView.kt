package com.taximeter.fragments.plan.favorites.mvp

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.SwipeableTouchHelperCallback
import com.taximeter.fragments.plan.favorites.FavoritesAdapter
import com.taximeter.utils.ui.RecyclerViewSetup
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_list.view.*
import javax.inject.Inject

@FragmentScoped
class FavoritesView @Inject constructor(private val mRecyclerViewSetup: RecyclerViewSetup<RecyclerView.ViewHolder>,
                                        private val mAdapter: FavoritesAdapter)
    : FavoritesContract.View {

    private lateinit var recyclerView: RecyclerView
    private lateinit var noElementsTv: TextView

    override fun bind(pageView: View) {
        recyclerView = pageView.recycler_view
        noElementsTv = pageView.no_elements_tv
        noElementsTv.setText(R.string.plan_favorites_no_elements)
    }

    override fun setup() {
        mRecyclerViewSetup.setup(recyclerView, SwipeableTouchHelperCallback(mAdapter))
    }

    override fun scrollToPosition(adapterPosition: Int) {
        recyclerView.scrollToPosition(adapterPosition)
    }

    override fun showEmptyMessage() {
        noElementsTv.visibility = View.VISIBLE
    }

    override fun hideEmptyMessage() {
        noElementsTv.visibility = View.GONE
    }
}