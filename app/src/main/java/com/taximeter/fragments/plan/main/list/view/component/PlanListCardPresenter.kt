package com.taximeter.fragments.plan.main.list.view.component

import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import com.taximeter.fragments.plan.main.mvp.PlanContract

interface PlanListCardPresenter {

    fun onViewAvailable(view: BasePlanViewHolder, presenter: PlanContract.Presenter)

    fun bind(index: Int)

    fun updateArrow()
}