package com.taximeter.fragments.plan.main.mvp

import android.view.View
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.taximeter.fragments.plan.autosuggest.SwipeableTouchHelperCallback
import com.taximeter.fragments.plan.main.list.PlanListAdapter
import com.taximeter.fragments.plan.main.modules.PlanAppBarHandler
import com.taximeter.utils.UiUtils.*
import com.taximeter.utils.ui.ProgressBarModule
import com.taximeter.utils.ui.RecyclerViewSetup
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_plan.view.*
import kotlinx.android.synthetic.main.plan_map.view.*
import kotlinx.android.synthetic.main.plan_map_header.view.*
import javax.inject.Inject

@FragmentScoped
class PlanView @Inject constructor(private val mPresenter: PlanContract.Presenter,
                                   private val mRecyclerViewSetup: RecyclerViewSetup<RecyclerView.ViewHolder>,
                                   private val appBarHandler: PlanAppBarHandler,
                                   private val mProgressBarModule: ProgressBarModule,
                                   private val mAdapter: PlanListAdapter)
    : PlanContract.View {

    private lateinit var appBarLayout: AppBarLayout
    private lateinit var carFab: FloatingActionButton

    private lateinit var currentLocationBtn: FloatingActionButton

    private lateinit var recyclerView: RecyclerView

    private lateinit var timeTv: TextView
    private lateinit var distanceTv: TextView
    private lateinit var priceTv: TextView
    private lateinit var headerRoot: View

    override fun bind(pageLayout: View) {
        timeTv = pageLayout.time_tv
        distanceTv = pageLayout.distance_tv
        priceTv = pageLayout.price_tv
        headerRoot = pageLayout.map_header

        currentLocationBtn = pageLayout.current_location_button
        currentLocationBtn.setOnClickListener { mPresenter.onCurrentLocationClickedOnMap() }

        recyclerView = pageLayout.recycler_view

        appBarLayout = pageLayout.appbar
        carFab = pageLayout.start_measurement_fab

        carFab.setOnClickListener { mPresenter.handleCarFabClicked() }

        mProgressBarModule.bind(pageLayout)
    }

    override fun showProgress() {
        mProgressBarModule.showProgress()
    }

    override fun hideProgress() {
        mProgressBarModule.hideProgress()
    }

    override fun showCarFab() {
        animateInView(carFab)
    }

    override fun hideCarFab() {
        animateOutView(carFab)
    }

    override fun setAppBarExpanded(appBarExpanded: Boolean) {
        appBarLayout.setExpanded(appBarExpanded)
    }

    override fun setupRecyclerView() {
        mRecyclerViewSetup.setup(recyclerView, SwipeableTouchHelperCallback(mAdapter))
    }

    override fun initAppBarLayout() {
        appBarLayout.addOnOffsetChangedListener(appBarHandler)
        val params = appBarLayout.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = AppBarLayout.Behavior()
        behavior.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                return false
            }
        })
        params.behavior = behavior
        appBarLayout.setExpanded(false)
    }

    override fun showHeader() {
        fadeInView(headerRoot)
    }

    override fun hideHeader() {
        fadeOutView(headerRoot)
    }

    override fun showCurrentLocationButton() {
        animateInView(currentLocationBtn)
    }

    override fun hideCurrentLocationButton() {
        animateOutView(currentLocationBtn)
    }

    override fun setDistanceText(distance: String) {
        distanceTv.text = distance
    }

    override fun setTimeText(time: String) {
        timeTv.text = time
    }

    override fun setPriceText(price: String) {
        priceTv.text = price
    }
}