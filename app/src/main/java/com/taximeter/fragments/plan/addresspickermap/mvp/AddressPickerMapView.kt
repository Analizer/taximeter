package com.taximeter.fragments.plan.addresspickermap.mvp

import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.taximeter.R
import com.taximeter.utils.UiUtils.animateInView
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_plan_picker_map.view.*
import javax.inject.Inject

@FragmentScoped
class AddressPickerMapView @Inject constructor(private val mPresenter: AddressPickerMapContract.Presenter,
                                               private val mProgressBarModule: ProgressBarModule)
    : AddressPickerMapContract.View {

    private lateinit var currentLocationBtn: FloatingActionButton
    private lateinit var doneBtn: Button
    private lateinit var root: View

    override fun bind(pageView: View) {
        currentLocationBtn = pageView.current_location_button
        doneBtn = pageView.plan_details_map_done_button
        root = pageView.root

        currentLocationBtn.setOnClickListener { mPresenter.onCurrentLocationClicked() }
        doneBtn.setOnClickListener { mPresenter.onDoneClicked() }
        mProgressBarModule.bind(pageView)
    }

    override fun showCurrentLocationButton() {
        animateInView(currentLocationBtn)
    }

    override fun showSelectButton() {
        val scaleAndFade = AnimationUtils.loadAnimation(doneBtn.context, R.anim.fade_in)//TODO AnimationUtils?
        doneBtn.animation = scaleAndFade
        doneBtn.visibility = View.VISIBLE
    }

    override fun showProgress() {
        mProgressBarModule.showProgress()
    }

    override fun hideProgress() {
        mProgressBarModule.hideProgress()
    }
}