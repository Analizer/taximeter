package com.taximeter.fragments.plan.main.list.view.component.currentlocation

interface PlanListCurrentLocationView {

        fun setText(text: String)

        fun setFavorite(isFavorite: Boolean)

        fun setFavoriteClickedListener(onClick: () -> Unit)

        fun setCurrentLocationClickedListener(onClick: () -> Unit)

        fun setRootClickListener(onClick: () -> Unit)

        fun setDeleteClickedListener(onClick: () -> Unit)

        fun showArrow()

        fun hideArrow()

        fun clearArrow()

        fun setArrowDistance(distance: String)

        fun setArrowDuration(duration: String)
}