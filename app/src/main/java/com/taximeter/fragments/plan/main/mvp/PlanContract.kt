package com.taximeter.fragments.plan.main.mvp

import android.os.Parcelable
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.directions.DirectionModel

interface PlanContract {

    interface Presenter {

        val model: Model

        fun onViewAvailable(view: View)

        fun onResume(planToRestore: PlanModel?)

        fun onDestroyView()

        fun onPause()

        fun showProgress()

        fun hideProgress()

        fun handleCarFabClicked()

        fun handleMapClicked()

        fun onRequestPermissionsResult()

        fun onCurrentLocationClickedOnMap()

        fun onDataSetChanged()

        fun onItemChanged(itemPosition: Int)

        fun onItemClicked(itemPosition: Int)

        fun heartItem(itemPosition: Int)

        fun unHeartItem(itemPosition: Int)

        fun deleteItem(itemPosition: Int)

        fun showSnackBar(messageId: Int, onUndoClick: (() -> Unit)? = null)
    }

    interface View {

        fun bind(pageLayout: android.view.View)

        fun showProgress()

        fun hideProgress()

        fun showCarFab()

        fun hideCarFab()

        fun setAppBarExpanded(appBarExpanded: Boolean)

        fun initAppBarLayout()

        fun showHeader()

        fun hideHeader()

        fun setDistanceText(distance: String)

        fun setTimeText(time: String)

        fun setPriceText(price: String)

        fun showCurrentLocationButton()

        fun hideCurrentLocationButton()

        fun setupRecyclerView()
    }

    interface Model : Parcelable {
        var date: Long

        var source: Stop

        var destination: Stop

        fun get(index: Int): Stop

        fun getStops(): List<Stop>

        fun resetStop(index: Int)

        fun updateStop(index: Int, stop: Stop)

        fun hasRealStops(): Boolean

        fun hasDirections(): Boolean

        fun getDirections(): DirectionModel?

        fun updateDirections(directions: DirectionModel?)

        fun prepareToEdit(indexToChange: Int)

        fun getItemToEdit(): Stop

        fun updateItemToEdit(stop: Stop)

        fun load(plan: Model)

        fun updateFavoriteStates(favorites: MutableList<Stop>?)
    }
}