package com.taximeter.fragments.plan.main.modules

import android.location.Location
import com.patloew.rxlocation.RxLocation
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.utils.AddressHandler
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.extras.Language
import com.taximeter.utils.location.CurrentLocationRetriever
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject

/**
 * Class responsible for retrieving the current location model for the source address card on the Plan page.
 */
@FragmentScoped
class CurrentAddressRetriever @Inject constructor(private val mCurrentLocationRetriever: CurrentLocationRetriever,
                                                  private val mRxLocation: RxLocation,
                                                  private val mErrorHandler: ErrorHandler,
                                                  private val mAddressHandler: AddressHandler) {

    private var mGeoCoderDisposable: Disposable? = null

    fun requestAddress(onSuccess: (stop: Stop) -> Unit) {
        tearDown()
        mCurrentLocationRetriever.requestLocation(onSuccess = { location -> requestAddressWithLocationAvailable(location, onSuccess) })
    }

    private fun requestAddressWithLocationAvailable(location: Location, onSuccess: (stop: Stop) -> Unit) {
        val locale = Locale(Language.currentAppLanguage.languageCode)
        mGeoCoderDisposable = mRxLocation
                .geocoding()
                .fromLocation(locale, location.latitude, location.longitude, 1)
                .toObservable()
                .map { addresses -> addresses[0] }
                .subscribe(
                        { address ->
                            if (mAddressHandler.isInBudapest(address)) {
                                val stop = mAddressHandler.getStopByAddress(address)
                                stop.type = AutoSuggestType.CURRENT_LOCATION
                                onSuccess.invoke(stop)
                            } else {
                                handleError()
                            }
                        },
                        { handleError() })
    }

    fun tearDown() {
        mGeoCoderDisposable?.dispose()
    }

    private fun handleError() {
        mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.AddressNotInBudapest, R.string.plan_budapest_error_msg)
    }
}
