package com.taximeter.fragments.plan.autosuggest.di

import androidx.recyclerview.widget.RecyclerView
import com.taximeter.activity.main.MainActivity
import com.taximeter.activity.main.ToolbarModule
import com.taximeter.communication.endpoint.PostalCodesEndpoint
import com.taximeter.communication.endpoint.StopsEndpoint
import com.taximeter.fragments.plan.autosuggest.AutoSuggestFragment
import com.taximeter.fragments.plan.autosuggest.list.adapter.AutoSuggestAdapter
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestPresenter
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestView
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import retrofit2.Retrofit

@InstallIn(FragmentComponent::class)
@Module
abstract class AutoSuggestFragmentModule {

    @Binds
    abstract fun view(view: AutoSuggestView): AutoSuggestContract.View

    @Binds
    abstract fun presenter(presenter: AutoSuggestPresenter): AutoSuggestContract.Presenter

    @Binds
    abstract fun adapter(adapter: AutoSuggestAdapter): RecyclerView.Adapter<RecyclerView.ViewHolder>

    companion object {

        @Provides
        internal fun postalCodesEndpoint(retrofit: Retrofit.Builder): PostalCodesEndpoint {
            return retrofit.baseUrl(Extra.BASE_URL_POSTAL_CODES.tag).build().create(PostalCodesEndpoint::class.java)
        }

        @Provides
        internal fun stopsEndpoint(retrofit: Retrofit.Builder): StopsEndpoint {
            return retrofit.baseUrl(Extra.BASE_URL_STOPS.tag).build().create(StopsEndpoint::class.java)
        }

        @Provides
        fun model(fragment: AutoSuggestFragment): PlanContract.Model {
            return fragment.requireArguments().getParcelable(Extra.TAG_MODEL.tag)!!
        }

        @Provides
        fun toolbarHandler(fragment: AutoSuggestFragment): ToolbarModule {
            return (fragment.activity as MainActivity).mToolbarHandler
        }
    }
}
