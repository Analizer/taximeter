package com.taximeter.fragments.plan.dao

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType

interface Stop : Parcelable {

    fun getText(): String

    fun getSecondaryText(): String = ""

    fun isRealStop(): Boolean {
        return type != AutoSuggestType.NONE
    }

    var type: AutoSuggestType

    var position: LatLng

    var postalCode: String

    var distance: Int

    var favorite: Boolean
}