package com.taximeter.fragments.plan.dao.directions

import android.os.Parcelable
import com.google.android.gms.maps.model.PolylineOptions
import com.taximeter.utils.calculator.PriceCalculator
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DirectionModel constructor(private val routes: List<DirectionsRoutes>)
    : Parcelable {

    val isValid: Boolean
        get() = route != null

    val polylineOptions: PolylineOptions?
        get() = route?.polylineOptions

    @IgnoredOnParcel
    var route: DirectionsRoutes? = null
        get() {
            return if (routes.isEmpty()) null else routes[0]
        }
        private set

    @IgnoredOnParcel
    var distance: Int = 0
        get() {
            if (field == 0) initParams()
            return field
        }

    @IgnoredOnParcel
    var duration: Int = 0
        get() {
            if (field == 0) initParams()
            return field
        }

    @IgnoredOnParcel
    var estimatedPrice: Int = 0
        get() {
            if (field == 0) initParams()
            return field
        }

    private val segments: List<DirectionsSegment>?
        get() = route?.segments

    private fun initParams() {
        var meters = 0
        var seconds = 0
        for (segment in segments!!) {
            meters += segment.distance.value
            seconds += segment.duration.value
        }
        val cost = PriceCalculator.getEstimatedPrice(meters, seconds)
        distance = meters
        duration = seconds
        estimatedPrice = cost
    }
}
