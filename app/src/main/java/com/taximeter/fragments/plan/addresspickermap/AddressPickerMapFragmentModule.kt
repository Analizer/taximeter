package com.taximeter.fragments.plan.addresspickermap

import com.taximeter.communication.endpoint.PostalCodesEndpoint
import com.taximeter.fragments.plan.addresspickermap.mvp.AddressPickerMapContract
import com.taximeter.fragments.plan.addresspickermap.mvp.AddressPickerMapPresenter
import com.taximeter.fragments.plan.addresspickermap.mvp.AddressPickerMapView
import com.taximeter.fragments.plan.main.mvp.PlanModel
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import retrofit2.Retrofit

@InstallIn(FragmentComponent::class)
@Module
abstract class AddressPickerMapFragmentModule {

    @Binds
    internal abstract fun presenter(presenter: AddressPickerMapPresenter): AddressPickerMapContract.Presenter

    @Binds
    internal abstract fun view(view: AddressPickerMapView): AddressPickerMapContract.View

    companion object {

        @Provides
        internal fun model(fragment: AddressPickerMapFragment): PlanModel {
            return fragment.requireArguments().getParcelable(Extra.TAG_MODEL.tag)!!
        }

        @Provides
        internal fun postalCodesEndpoint(retrofit: Retrofit.Builder): PostalCodesEndpoint {
            return retrofit.baseUrl(Extra.BASE_URL_POSTAL_CODES.tag).build().create(PostalCodesEndpoint::class.java)
        }
    }
}
