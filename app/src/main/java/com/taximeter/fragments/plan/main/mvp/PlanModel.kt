package com.taximeter.fragments.plan.main.mvp

import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.PlaceHolderModel
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@FragmentScoped
@Parcelize
data class PlanModel constructor(override var date: Long,
                                 override var source: Stop,
                                 override var destination: Stop)
    : PlanContract.Model {

    @IgnoredOnParcel
    private var directions: DirectionModel? = null

    @IgnoredOnParcel
    private var itemIndexToEdit: Int? = null

    @Inject
    constructor() : this(System.currentTimeMillis(), PlaceHolderModel(), PlaceHolderModel())

    override fun hasRealStops(): Boolean {
        return source.type != AutoSuggestType.NONE && destination.type != AutoSuggestType.NONE
    }

    override fun load(plan: PlanContract.Model) {
        directions = null
        source = plan.source
        destination = plan.destination
        date = System.currentTimeMillis()
    }

    override fun get(index: Int): Stop {
        return when (index) {
            0 -> source
            1 -> destination
            else -> throw IllegalArgumentException("wrong index $index passed when getting item from PlanModel")
        }
    }

    override fun getStops(): List<Stop> {
        return listOf(source, destination)
    }

    override fun updateStop(index: Int, stop: Stop) {
        when (index) {
            0 -> source = stop
            1 -> destination = stop
            else -> throw IllegalArgumentException("wrong index $index passed when setting item from PlanModel")
        }
    }

    override fun resetStop(index: Int) {
        when (index) {
            0 -> source = PlaceHolderModel()
            1 -> destination = PlaceHolderModel()
            else -> throw IllegalArgumentException("wrong index $index passed when resetting item from PlanModel")
        }
    }

    override fun prepareToEdit(indexToChange: Int) {
        itemIndexToEdit = indexToChange
    }

    override fun getItemToEdit(): Stop {
        return when (itemIndexToEdit) {
            0 -> source
            1 -> destination
            else -> throw IllegalArgumentException("Wrong index ($itemIndexToEdit) provided for editing PlanModel")
        }
    }

    override fun updateItemToEdit(stop: Stop) {
        updateStop(itemIndexToEdit!!, stop)
        itemIndexToEdit = null
    }

    override fun hasDirections(): Boolean {
        return directions?.isValid ?: false
    }

    override fun getDirections(): DirectionModel? {
        return directions
    }

    override fun updateDirections(directions: DirectionModel?) {
        this.directions = directions
    }

    override fun updateFavoriteStates(favorites: MutableList<Stop>?) {
        if (source.isRealStop()) source.favorite = false
        if (destination.isRealStop()) destination.favorite = false
        if (favorites != null && favorites.isNotEmpty()) {
            for (favorite in favorites) {
                if (source.isRealStop() && favorite.position == source.position) source.favorite = true
                else if (destination.isRealStop() && favorite.position == destination.position) destination.favorite = true
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        return source.position == (other as PlanContract.Model).source.position && destination.position == other.destination.position
    }
}