package com.taximeter.fragments.plan.autosuggest

import android.content.res.Resources
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.taximeter.R
import com.taximeter.communication.endpoint.StopsEndpoint
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import com.taximeter.fragments.plan.autosuggest.transformer.AddressTransformer
import com.taximeter.fragments.plan.autosuggest.transformer.PlaceTransformer
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.address.AddressModel
import com.taximeter.fragments.plan.dao.address.AddressesModel
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.fragments.plan.dao.place.PlacesModel
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.MaybeObserver
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@FragmentScoped
class AutoSuggestCommunicator @Inject constructor(private val mPlaceTransformer: PlaceTransformer,
                                                  private val mAddressTransformer: AddressTransformer,
                                                  private val mEndpoint: StopsEndpoint,
                                                  private val mResources: Resources,
                                                  private val mCalculator: GeoCalculator) {

    private var mDisposables: CompositeDisposable = CompositeDisposable()
    private lateinit var mPresenter: AutoSuggestContract.Presenter

    private val budapestCenter = LatLng(47.497866, 19.054884)
    private val radius = "20000"

    fun init(presenter: AutoSuggestContract.Presenter) {
        mPresenter = presenter
    }

    fun start(query: String, currentLocation: Location) {
        Observable.combineLatest<AddressesModel, PlacesModel, List<Stop>>(
                getAddresses(getExtendedQuery(query)),
                getPlaces(getExtendedQuery(query)),
                BiFunction<AddressesModel, PlacesModel, List<Stop>> { addressesModel: AddressesModel, placeModel: PlacesModel ->
                    val stops = mutableListOf<Stop>()
                    Observable.fromIterable(addressesModel.addresses)
                            .compose<Stop>(mAddressTransformer)
                            .concatWith(Observable.fromIterable<PlaceModel>(placeModel.results)
                                    .compose(mPlaceTransformer))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : Observer<Stop> {
                                override fun onSubscribe(disposable: Disposable) {
                                    mDisposables.add(disposable)
                                }

                                override fun onNext(stop: Stop) {
                                    stop.distance = mCalculator.distance(currentLocation, stop.position)
                                    stops.add(stop)
                                }

                                override fun onError(e: Throwable) {
                                    mPresenter.onResultError(AnalyticsAction.AutoSuggestResultsError, R.string.error_message_try_again)
                                }

                                override fun onComplete() {
                                    orderStops(stops)
                                    mPresenter.onResult(stops)
                                }
                            })
                    stops
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    mPresenter.onResultError(AnalyticsAction.AutoSuggestResultsUnknownError, R.string.error_message_try_again)
                }
                .onTerminateDetach()
                .subscribe()
    }

    fun requestSingleResult(query: String) {
        val model = AddressModel(getExtendedQuery(query))
        Observable.just(model)
                .compose(mAddressTransformer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .firstElement()
                .subscribe(object : MaybeObserver<Stop> {
                    override fun onSubscribe(disposable: Disposable) {
                        mDisposables.add(disposable)
                    }

                    override fun onSuccess(stop: Stop) {
                        mPresenter.onSingleResult(stop, AnalyticsAction.AutoSuggestAddressEnteredAndNotWaitingForResults)
                    }

                    override fun onError(e: Throwable) {
                        mPresenter.onResultError(AnalyticsAction.AutoSuggestAddressEnteredError, R.string.plan_text_not_recognized_msg)
                    }

                    override fun onComplete() {
                        //no results
                        mPresenter.onResultError(AnalyticsAction.AutoSuggestAddressEnteredError, R.string.plan_text_not_recognized_msg)
                    }
                })
    }

    fun stop() {
        mDisposables.dispose()
        mDisposables = CompositeDisposable()
    }

    private fun orderStops(stops: MutableList<Stop>) {
        val addresses = mutableListOf<Stop>()
        val places = mutableListOf<Stop>()
        for (model in stops) {
            if (model.type == AutoSuggestType.PLACE && !places.contains(model)) {
                places.add(model)
            } else if (model.type == AutoSuggestType.ADDRESS && !addresses.contains(model)) {
                addresses.add(model)
            }
        }
        addresses.sortWith(Comparator { address1: Stop, address2: Stop -> address1.distance.compareTo(address2.distance) })
        places.sortWith(Comparator { place1: Stop, place2: Stop -> place1.distance.compareTo(place2.distance) })
        stops.clear()
        stops.addAll(addresses)
        stops.addAll(places)
    }

    private fun getAddresses(query: String): Observable<AddressesModel> {
        return mEndpoint.getAddresses(query, mResources.getString(R.string.key_places), "country:hu", mResources.getString(R.string.language_code))
    }

    private fun getPlaces(query: String): Observable<PlacesModel> {
        val locationString = budapestCenter.latitude.toString() + "," + budapestCenter.longitude.toString()
        return mEndpoint.getPlaces(query, mResources.getString(R.string.key_places), mResources.getString(R.string.language_code), locationString, radius, "false")
    }

    private fun getExtendedQuery(query: String): String {
        return if (!query.contains(Extra.TAG_BUDAPEST.tag)) {
            StringBuilder(query).append(",").append(Extra.TAG_BUDAPEST.tag).toString()
        } else {
            query
        }
    }
}