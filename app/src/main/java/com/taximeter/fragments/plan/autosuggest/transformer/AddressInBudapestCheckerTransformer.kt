package com.taximeter.fragments.plan.autosuggest.transformer

import android.location.Address
import com.taximeter.utils.AddressHandler
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * Checks if the address in the stream is in Budapest, sends @NotInBudapestException otherwise
 */
class AddressInBudapestCheckerTransformer @Inject constructor(private val mAddressHandler: AddressHandler)
    : ObservableTransformer<Address, Address> {


    override fun apply(upstream: Observable<Address>): ObservableSource<Address> {
        return upstream.flatMap { address ->
            if (mAddressHandler.isInBudapest(address)) {
                Observable.just(address)
            } else {
                Observable.error(NotInBudapestException())
            }
        }
    }

    class NotInBudapestException : Exception()
}