package com.taximeter.fragments.plan.addresspickermap

import android.location.Address
import com.patloew.rxlocation.RxLocation
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.transformer.AddressInBudapestCheckerTransformer
import com.taximeter.fragments.plan.autosuggest.transformer.PostalCodeTransformer
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.utils.AddressHandler
import com.taximeter.utils.extras.Language
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

@FragmentScoped
class AddressPickerMapCommunicator @Inject constructor(private val mRxLocation: RxLocation,
                                                       private val mPostalCodeTransformer: PostalCodeTransformer,
                                                       private val mAddressInBudapestCheckerTransformer: AddressInBudapestCheckerTransformer,
                                                       private val mAddressHandler: AddressHandler) {
    private var mAddressDisposable: Disposable? = null

    fun tearDown() {
        mAddressDisposable?.dispose()
    }

    fun requestAddressForClickedPosition(latitude: Double,
                                         longitude: Double,
                                         distance: Int,
                                         onSuccess: (stop: Stop) -> Unit,
                                         onError: (toReport: AnalyticsAction, toShow: Int) -> Unit) {
        val locale = Locale(Language.currentAppLanguage.languageCode)
        mRxLocation.geocoding()
                .fromLocation(locale, latitude, longitude, 1)
                .toObservable()
                .map { addresses: MutableList<Address> -> addresses[0] }
                .compose(mAddressInBudapestCheckerTransformer)
                .map { address: Address -> mAddressHandler.getStopByAddress(address, distance) }
                .compose(mPostalCodeTransformer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<Stop> {
                    override fun onSubscribe(disposable: Disposable) {
                        mAddressDisposable = disposable
                    }

                    override fun onNext(stop: Stop) {
                        onSuccess.invoke(stop)
                    }

                    override fun onError(exception: Throwable) {
                        if (exception is AddressInBudapestCheckerTransformer.NotInBudapestException) {
                            onError(AnalyticsAction.PlanPickerMapAddressNotInBudapest, R.string.plan_budapest_error_msg)
                        } else {
                            onError(AnalyticsAction.PlanPickerMapAddressError, R.string.location_not_found_text)
                        }
                    }

                    override fun onComplete() {}
                })
    }
}