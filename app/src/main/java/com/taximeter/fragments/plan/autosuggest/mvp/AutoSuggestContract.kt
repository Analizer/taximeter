package com.taximeter.fragments.plan.autosuggest.mvp

import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.reporting.AnalyticsAction

interface AutoSuggestContract {

    interface Presenter {

        fun onViewAvailable(view: View)

        fun onResume()

        fun tearDown()

        fun showProgress()

        fun hideProgress()

        fun onSearchTextChanged(query: String)

        fun onSingleResult(result: Stop, toReport: AnalyticsAction)

        fun onResult(results: MutableList<Stop>)

        fun onResultError(toReport: AnalyticsAction, toShow: Int)

         fun onCurrentLocation()

         fun onFavorites()

         fun onRecents()

         fun onMap()

         fun onAutoSuggestItem(stop: Stop)
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun showProgress()

        fun hideProgress()
    }
}