package com.taximeter.fragments.plan.autosuggest

import android.text.Editable
import android.text.TextWatcher
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AutoSuggestTextWatcher @Inject constructor()
    : TextWatcher {

    private lateinit var mPresenter: AutoSuggestContract.Presenter

    fun init(presenter: AutoSuggestContract.Presenter) {
        mPresenter = presenter
    }

    override fun afterTextChanged(searchText: Editable?) {
        if (searchText != null) {
            mPresenter.onSearchTextChanged(searchText.toString())
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //NO-OP
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //NO-OP
    }
}