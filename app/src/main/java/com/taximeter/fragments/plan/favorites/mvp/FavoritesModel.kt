package com.taximeter.fragments.plan.favorites.mvp

import com.taximeter.fragments.plan.dao.Stop
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class FavoritesModel @Inject constructor() : FavoritesContract.Model {

    private lateinit var mElements: MutableList<Stop>

    override fun init(elements: MutableList<Stop>?) {
        mElements = elements ?: mutableListOf()
        orderItems()
    }

    override fun get(position: Int): Stop {
        return mElements[position]
    }

    override fun add(adapterPosition: Int, favorite: Stop) {
        mElements.add(adapterPosition, favorite)
    }

    override fun remove(position: Int) {
        mElements.removeAt(position)
    }

    override fun size(): Int {
        return mElements.size
    }

    private fun orderItems() {
        mElements.sortWith(Comparator { t1, t2 -> t2.toString().compareTo(t1.toString()) })//TODO needed?
    }
}