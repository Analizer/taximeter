package com.taximeter.fragments.plan.autosuggest.list.view

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.autosuggest_item_address.view.*

class AutoSuggestAddressViewHolder(var root: View) : RecyclerView.ViewHolder(root) {
    var primaryText: TextView = root.autosuggest_primary_text
    var distance: TextView = root.autosuggest_distance
}