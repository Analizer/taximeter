package com.taximeter.fragments.plan.dao.address

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressModel(@SerializedName("description") var address: String,
                        override var type: AutoSuggestType = AutoSuggestType.ADDRESS,
                        override var position: LatLng = LatLng(0.0, 0.0),
                        override var distance: Int = 0,
                        override var postalCode: String = "",
                        override var favorite: Boolean = false,
                        var streetNumber: String? = null)
    : Stop {

    override fun getText(): String {
        removeDuplicatePostalCode()
        removeUnnecessaryAddressParts() //I would put these into an onResume block but on autosuggest it isn't called unfortunately (Gson skips it or something)
        val sb = StringBuilder("$postalCode, ")
        sb.append(address)
        if (streetNumber != null) {
            sb.append(" $streetNumber")
        }
        return sb.toString()
    }

    /**
     * If the address.getThoroughfare() and address.getFeatureName() are not usable when building the address from a location, we just return the @address field, which already contains the postal code.
     * In these cases the postal code is appended to the end of the address String, e.g.: Csepel Fűtés- És Gáztechnikai Kft., 1., 1211 Hungary
     * Since I want the postal code to be at the beginning of the address, first I have to remove it from this String and then append it to the beginning.
     */
    private fun removeDuplicatePostalCode() {
        if (address.contains(", $postalCode")) {
            address = address.replace(", $postalCode", "")
        }
    }

    private fun removeUnnecessaryAddressParts() {
        address = address
                .replace("Budapest, ", "")
                .replace(", Hungary", "")
                .replace(" Hungary", "")
                .trim()
    }
}
