package com.taximeter.fragments.plan.main.list.view.component.placeholder

interface PlanListPlaceHolderView {
    fun setText(textResId: Int)

    fun showCurrentLocation()

    fun hideCurrentLocation()

    fun setCurrentLocationClickedListener(onClick: () -> Unit)

    fun setRootClickListener(onClick: () -> Unit)

    fun showArrow()

    fun hideArrow()
}