package com.taximeter.fragments.plan.main.di

import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.taximeter.fragments.plan.main.list.PlanListAdapter
import com.taximeter.fragments.plan.main.modules.PlanAppBarHandler
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.main.mvp.PlanModel
import com.taximeter.fragments.plan.main.mvp.PlanPresenter
import com.taximeter.fragments.plan.main.mvp.PlanView
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class PlanFragmentModule {

    @Binds
    internal abstract fun presenter(presenter: PlanPresenter): PlanContract.Presenter

    @Binds
    internal abstract fun view(view: PlanView): PlanContract.View

    @Binds
    internal abstract fun model(model: PlanModel): PlanContract.Model

    @Binds
    internal abstract fun appBarOffsetChangedListener(appBarHandler: PlanAppBarHandler): AppBarLayout.OnOffsetChangedListener

    @Binds
    internal abstract fun adapter(adapter: PlanListAdapter): RecyclerView.Adapter<RecyclerView.ViewHolder>
}
