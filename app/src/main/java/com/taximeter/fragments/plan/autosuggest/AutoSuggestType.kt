package com.taximeter.fragments.plan.autosuggest

enum class AutoSuggestType constructor(val tag: String) {

    CURRENT_LOCATION("CurrentLocation"),
    ADDRESS("Address"),
    PLACE("Place"),
    NONE("None")
}
