package com.taximeter.fragments.plan.dao.directions

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DirectionsPolyLine constructor(val points: String)
    : Parcelable
