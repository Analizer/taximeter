package com.taximeter.fragments.plan.addresspickermap.mvp

interface AddressPickerMapContract {

    interface Presenter {
        fun onViewAvailable(view: View)

        fun onDestroyView()

        fun onPause()

        fun onCurrentLocationClicked()

        fun onMapReady()

        fun onMapClick(latitude: Double, longitude: Double)

        fun onRequestPermissionsResult()

        fun onDoneClicked()
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun showCurrentLocationButton()

        fun showProgress()

        fun hideProgress()

        fun showSelectButton()
    }
}