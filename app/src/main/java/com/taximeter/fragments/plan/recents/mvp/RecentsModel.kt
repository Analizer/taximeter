package com.taximeter.fragments.plan.recents.mvp

import com.taximeter.fragments.plan.main.mvp.PlanContract
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class RecentsModel @Inject constructor() : RecentsContract.Model {

    private lateinit var mElements: MutableList<PlanContract.Model>

    override fun init(elements: MutableList<PlanContract.Model>?) {
        mElements = elements ?: mutableListOf()
        orderItems()
    }

    override fun get(position: Int): PlanContract.Model {
        return mElements[position]
    }

    override fun add(adapterPosition: Int, plan: PlanContract.Model) {
        mElements.add(adapterPosition, plan)
    }

    override fun remove(position: Int) {
        mElements.removeAt(position)
    }

    override fun size(): Int {
        return mElements.size
    }

    private fun orderItems() {
        mElements.sortWith(Comparator { t1, t2 -> t2.date.compareTo(t1.date) })
    }
}