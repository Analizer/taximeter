package com.taximeter.fragments.plan.recents.mvp

import com.taximeter.R
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.recents.adapter.RecentsAdapter
import com.taximeter.persistence.database.DatabaseModule
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.utils.SnackBarHandler
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class RecentsPresenter @Inject constructor(private val mAdapter: RecentsAdapter,
                                           private val mNavigator: PlanNavigator,
                                           private val mPlanModel: PlanContract.Model,
                                           private val mModel: RecentsContract.Model,
                                           private val mReporter: Reporter,
                                           private val mSnackBarHandler: SnackBarHandler,
                                           private val mDatabase: DatabaseModule)
    : RecentsContract.Presenter {

    internal lateinit var mView: RecentsContract.View

    override fun onViewAvailable(view: RecentsContract.View) {
        mView = view
    }

    override fun onResume() {
        mDatabase.getPlans { plans -> onModelReady(plans) }
    }

    override fun tearDown() {
        mSnackBarHandler.hideSnackBar()
    }

    override fun onItemClicked(item: PlanContract.Model) {
        mReporter.reportEvent(AnalyticsCategory.Recents, AnalyticsAction.OpenRecent)
        mPlanModel.load(item)
        mNavigator.returnToPlanPage(mPlanModel)
    }

    override fun deleteItem(position: Int, toReport: AnalyticsLabel) {
        mReporter.reportEvent(AnalyticsCategory.Recents, AnalyticsAction.DeleteRecent, toReport)
        val planToDelete = mModel.get(position)
        removeItemFromModel(position)
        mSnackBarHandler.showSnackBar(
                messageId = (R.string.recent_plan_deleted_snackbar_msg),
                onUndoClick = { undoDelete(position, planToDelete) },
                onSnackBarDismissed = { mDatabase.deletePlan(planToDelete.date) })
    }

    private fun onModelReady(plans: MutableList<PlanContract.Model>?) {
        mModel.init(plans)
        mAdapter.init(this)
        mView.setup()
        showOrHideEmptyMessage()
    }

    private fun removeItemFromModel(position: Int) {
        mModel.remove(position)
        mAdapter.notifyItemRemoved(position)
        mAdapter.notifyItemRangeChanged(position, mModel.size())
        showOrHideEmptyMessage()
    }

    private fun undoDelete(adapterPosition: Int, plan: PlanContract.Model) {
        mReporter.reportEvent(AnalyticsCategory.Favorites, AnalyticsAction.UndoDeleteFavorite)
        mModel.add(adapterPosition, plan)
        mAdapter.notifyItemInserted(adapterPosition)
        mAdapter.notifyItemRangeChanged(adapterPosition, mModel.size())
        mView.scrollToPosition(adapterPosition)
        mView.hideEmptyMessage()
    }

    private fun showOrHideEmptyMessage() {
        if (mAdapter.itemCount == 0) mView.showEmptyMessage() else mView.hideEmptyMessage()
    }
}