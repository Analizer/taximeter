package com.taximeter.fragments.plan.favorites

import android.location.Location
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.favorites.mvp.FavoritesContract
import com.taximeter.fragments.plan.favorites.mvp.FavoritesModel
import com.taximeter.fragments.plan.favorites.mvp.FavoritesPresenter
import com.taximeter.fragments.plan.favorites.mvp.FavoritesView
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class FavoritesModule {

    @Binds
    abstract fun presenter(presenter: FavoritesPresenter): FavoritesContract.Presenter

    @Binds
    abstract fun view(view: FavoritesView): FavoritesContract.View

    @Binds
    abstract fun model(model: FavoritesModel): FavoritesContract.Model

    @Binds
    abstract fun listListener(presenter: FavoritesContract.Presenter): ListListener<Stop>

    @Binds
    abstract fun adapter(adapter: FavoritesAdapter): RecyclerView.Adapter<RecyclerView.ViewHolder>

    companion object {

        @Provides
        fun planModel(fragment: FavoritesFragment): PlanContract.Model {
            return fragment.requireArguments().getParcelable(Extra.TAG_MODEL.tag)!!
        }

        @Provides
        fun location(fragment: FavoritesFragment): Location {
            return fragment.requireArguments().getParcelable(Extra.TAG_CURRENT_LOCATION.tag)!!
        }
    }
}
