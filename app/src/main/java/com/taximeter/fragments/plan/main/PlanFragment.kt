package com.taximeter.fragments.plan.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.main.mvp.PlanModel
import com.taximeter.utils.extras.Extra
import dagger.android.support.DaggerFragment
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class PlanFragment : DaggerFragment(), Page, HasSupportFragmentInjector {

    //TODO remove these
    @Inject
    internal lateinit var mView: PlanContract.View
    @Inject
    internal lateinit var mPresenter: PlanContract.Presenter
    @Inject
    internal lateinit var mModel: PlanContract.Model//TODO ezeket a nem szuksegeseket kivenni a banatba
    @Inject
    internal lateinit var mRootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_plan, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun onResume() {
        super.onResume()
        val modelToRestore = activity?.intent?.extras?.getParcelable<PlanModel>(Extra.TAG_AUTO_SUGGEST_RESULT.tag)
        mPresenter.onResume(modelToRestore)
    }

    override fun onPause() {
        mPresenter.onPause()
        super.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDestroyView()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPresenter.onRequestPermissionsResult()
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.PLAN
    }
}
