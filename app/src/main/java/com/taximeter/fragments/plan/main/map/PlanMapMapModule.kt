package com.taximeter.fragments.plan.main.map

import android.location.Location
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.taximeter.R
import com.taximeter.fragments.di.ChildFragmentManager
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.MapHelper
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanMapMapModule @Inject constructor(@ChildFragmentManager private val mFragmentManager: FragmentManager,
                                           private var mModel: PlanContract.Model,
                                           private val mInfoWindowAdapter: PlanInfoWindowAdapter,
                                           private val mMapHelper: MapHelper)
    : OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener {

    private var mMap: GoogleMap? = null
    private lateinit var mPresenter: PlanContract.Presenter


    fun init(presenter: PlanContract.Presenter) {
        mPresenter = presenter
        (mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)
    }

    fun update(model: PlanContract.Model) {
        mModel = model
        updateAll()
    }

    fun tearDown() {
        val mapFragment = mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        if (mapFragment != null) {
            mFragmentManager.beginTransaction().remove(mapFragment).commitAllowingStateLoss()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setupMap()
    }

    override fun onMapLoaded() {
        updateAll()
    }

    override fun onMapClick(latLng: LatLng) {
        mPresenter.handleMapClicked()
    }

    private fun updateAll() {
        if (mMap != null) {
            // If the plan page is starting up with some plan data to show, we have to wait for the map to load, then this will be ran again from onMapLoaded
            mMap!!.clear()
            updateMarkers()
            updateInfoWindow()
            updatePolyLine()
            zoomMap()
        }
    }

    private fun updateMarkers() {
        if (mModel.source.isRealStop()) {
            mMapHelper.addMarkerToMap(mMap!!, mModel.source.position)
        }
        if (mModel.destination.isRealStop()) {
            mMapHelper.addMarkerToMap(mMap!!, mModel.destination.position)
        }
    }

    private fun updateInfoWindow() {
        mInfoWindowAdapter.updateModel(mModel.getStops())
        mMap!!.setInfoWindowAdapter(mInfoWindowAdapter)
    }

    private fun updatePolyLine() {
        if (mModel.hasRealStops() && mModel.hasDirections()) {
            val direction: DirectionModel? = mModel.getDirections()
            mMap!!.addPolyline(direction!!.polylineOptions!!)
        }
    }

    private fun zoomMap() {
        when {
            mModel.hasDirections() -> mMapHelper.zoomToPolyline(mModel.getDirections()!!.polylineOptions!!.points, mMap!!)
            mModel.source.type != AutoSuggestType.NONE -> mMapHelper.zoomToLocation(mModel.source.position, mMap!!)
            mModel.destination.type != AutoSuggestType.NONE -> mMapHelper.zoomToLocation(mModel.destination.position, mMap!!)
        }
    }

    private fun setupMap() {
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.setAllGesturesEnabled(true)
        mMap!!.uiSettings.isMyLocationButtonEnabled = false

        mMap!!.setOnMapLoadedCallback(this)
        mMap!!.setOnMapClickListener(this)
        mMapHelper.zoomToBudapest(mMap!!)
    }

    fun onLocationPermissionGranted() {
        mMap!!.uiSettings.isMyLocationButtonEnabled = true//TODO!
    }

    fun zoomToLocation(location: Location) {
        mMapHelper.zoomToLocation(location, mMap!!)
    }
}