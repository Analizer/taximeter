package com.taximeter.fragments.plan.dao.postalcode;

public class PostalCodeModel {

    String postalCode;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
