package com.taximeter.fragments.plan.autosuggest.transformer

import com.google.android.gms.maps.model.LatLng
import com.patloew.rxlocation.RxLocation
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * Adds a postal code to the place models
 */
class PlaceTransformer @Inject constructor(private val mPostalCodeTransformer: PostalCodeTransformer,
                                           private val mRxLocation: RxLocation)
    : ObservableTransformer<Stop, Stop> {

    override fun apply(upstream: Observable<Stop>): ObservableSource<Stop> {
        return upstream
                .flatMap { placeModel ->
                    mRxLocation
                            .geocoding()
                            .fromLocationName(placeModel.getSecondaryText())
                            .toObservable()
                            .map { address ->
                                placeModel.type = AutoSuggestType.PLACE
                                placeModel.position = LatLng(address.latitude, address.longitude)
                                placeModel.postalCode = address.postalCode ?: ""
                                placeModel
                            }
                }
                .compose(mPostalCodeTransformer)
    }
}
