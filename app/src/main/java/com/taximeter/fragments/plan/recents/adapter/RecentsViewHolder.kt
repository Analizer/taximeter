package com.taximeter.fragments.plan.recents.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_recent_plan.view.*

class RecentsViewHolder(var root: View) : RecyclerView.ViewHolder(root) {
    var titleTv: TextView = root.recent_plan_title_tv
    var contentTv: TextView = root.recent_plan_content_tv
    var deleteIcn: ImageView = root.recent_plan_delete
}
