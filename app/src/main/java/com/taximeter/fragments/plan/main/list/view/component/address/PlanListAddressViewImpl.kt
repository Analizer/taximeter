package com.taximeter.fragments.plan.main.list.view.component.address

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.taximeter.R
import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import kotlinx.android.synthetic.main.plan_address_row.view.*

class PlanListAddressViewImpl constructor(itemView: View)
    : PlanListAddressView, BasePlanViewHolder(itemView) {

    private val distanceTv: TextView = itemView.plan_distance
    private val deleteIcn: ImageView = itemView.findViewById(R.id.plan_delete)
    private val addressTv: TextView = itemView.findViewById(R.id.plan_address)
    private val favoriteIcn: ImageView = itemView.findViewById(R.id.plan_favorite)

    override fun setText(text: String) {
        addressTv.text = text
    }

    override fun setFavorite(isFavorite: Boolean) {
        favoriteIcn.setImageResource(if (isFavorite) R.drawable.favorite else R.drawable.favorite_border)
    }

    override fun setFavoriteClickedListener(onClick: () -> Unit) {
        favoriteIcn.setOnClickListener { onClick.invoke() }
    }

    override fun setRootClickListener(onClick: () -> Unit) {
        root.setOnClickListener { onClick.invoke() }
    }

    override fun setDeleteClickedListener(onClick: () -> Unit) {
        deleteIcn.setOnClickListener { onClick.invoke() }
    }

    override fun setDistance(distance: String) {
        distanceTv.text = distance
    }

    override fun showArrow() {
        arrowRow.root.visibility = View.VISIBLE
    }

    override fun hideArrow() {
        arrowRow.root.visibility = View.GONE
    }

    override fun clearArrow() {
        arrowRow.distance.text = ""
        arrowRow.duration.text = ""
        //TODO price?
    }

    override fun setArrowDistance(distance: String) {
        arrowRow.distance.text = distance
    }

    override fun setArrowDuration(duration: String) {
        arrowRow.duration.text = duration
    }

}