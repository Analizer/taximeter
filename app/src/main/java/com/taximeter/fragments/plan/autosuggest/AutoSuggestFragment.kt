package com.taximeter.fragments.plan.autosuggest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class AutoSuggestFragment : DaggerFragment(), Page {

    @Inject
    lateinit var mPresenter: AutoSuggestContract.Presenter
    @Inject
    lateinit var mView: AutoSuggestContract.View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_autosuggest, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.tearDown()
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.AUTO_SUGGEST
    }
}
