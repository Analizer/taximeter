package com.taximeter.fragments.plan.autosuggest.transformer

import com.google.android.gms.maps.model.LatLng
import com.patloew.rxlocation.RxLocation
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * Adds a location and a postal code to the address models
 */
class AddressTransformer @Inject constructor(private val mRxLocation: RxLocation,
                                             private val mPostalCodeTransformer: PostalCodeTransformer)
    : ObservableTransformer<Stop, Stop> {

    override fun apply(upstream: Observable<Stop>): ObservableSource<Stop> {
        return upstream
                .flatMap { addressModel ->
                    mRxLocation
                            .geocoding()
                            .fromLocationName(addressModel.getText())
                            .toObservable()
                            .map { address ->
                                addressModel.type = AutoSuggestType.ADDRESS
                                addressModel.position = LatLng(address.latitude, address.longitude)
                                addressModel.postalCode = address.postalCode ?: ""
                                addressModel
                            }
                }
                .compose(mPostalCodeTransformer)

    }
}
