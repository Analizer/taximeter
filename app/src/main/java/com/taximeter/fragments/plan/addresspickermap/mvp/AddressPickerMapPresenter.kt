package com.taximeter.fragments.plan.addresspickermap.mvp

import com.taximeter.R
import com.taximeter.fragments.plan.addresspickermap.AddressPickerMap
import com.taximeter.fragments.plan.addresspickermap.AddressPickerMapCommunicator
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.fragments.plan.main.mvp.PlanModel
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.location.LocationSubscriber
import com.taximeter.utils.NetworkModule
import com.taximeter.utils.SnackBarHandler
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.location.CurrentLocationRetriever
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AddressPickerMapPresenter @Inject constructor(private val mNetworkModule: NetworkModule,
                                                    private val mModel: PlanModel,
                                                    private val mSnackBarHandler: SnackBarHandler,
                                                    private val mReporter: Reporter,
                                                    private val mCurrentLocationRetriever: CurrentLocationRetriever,
                                                    private val mCommunicator: AddressPickerMapCommunicator,
                                                    private val mGeoCalculator: GeoCalculator,
                                                    private val mMap: AddressPickerMap,
                                                    private val mNavigator: PlanNavigator,
                                                    private val mDialogFactory: DialogFactory)
    : AddressPickerMapContract.Presenter {

    internal lateinit var mView: AddressPickerMapContract.View
    private var mTmpModel: Stop? = null

    override fun onViewAvailable(view: AddressPickerMapContract.View) {
        mView = view
        mMap.init(this)
        mCurrentLocationRetriever.addSubscriber(LocationSubscriber.ADDRESS_PICKER_MAP)
        mCurrentLocationRetriever.checkPermission(onGranted = { mMap.showUserLocationOnMap() })
    }

    override fun onPause() {
        mCommunicator.tearDown()
    }

    override fun onDestroyView() {
        mMap.tearDown()
        mCurrentLocationRetriever.tearDown()
    }

    override fun onMapReady() {
        if (mModel.getItemToEdit().isRealStop()) {
            mMap.addMarker(mModel.getItemToEdit())
        }
        mView.showCurrentLocationButton()
        mView.showSelectButton()
    }

    override fun onMapClick(latitude: Double, longitude: Double) {
        if (mNetworkModule.isConnected()) {
            mCurrentLocationRetriever.requestLocation(onSuccess = { location ->
                mCommunicator.requestAddressForClickedPosition(
                        latitude,
                        longitude,
                        mGeoCalculator.distance(location, latitude, longitude),
                        { stop -> handleResult(stop) },
                        { toReport, toShow -> handleError(toReport, toShow) }
                )
                mMap.showUserLocationOnMap()
            })
        } else {
            handleError(AnalyticsAction.PlanPickerMapNetworkError, R.string.error_message_no_internet)
        }
    }

    override fun onRequestPermissionsResult() {
        mCurrentLocationRetriever.onRequestPermissionsResult()
    }

    override fun onCurrentLocationClicked() {
        mCurrentLocationRetriever.requestLocation(onSuccess = { location ->
            mView.hideProgress()
            mMap.zoomToCurrentLocation(location)
            mMap.showUserLocationOnMap()
        })
    }

    override fun onDoneClicked() {
        if (mTmpModel != null) {
            mReporter.reportEvent(AnalyticsCategory.PlanPickerMap, AnalyticsAction.PlanPickerMapAddressSelected)
            mModel.updateItemToEdit(mTmpModel!!)
            mNavigator.returnToPlanPage(mModel)
        } else {
            mDialogFactory.showDialogIfNeeded(DialogType.ADDRESS_PICKER_ERROR)
        }
    }

    private fun handleResult(stop: Stop) {
        mTmpModel = stop
        mReporter.reportEvent(AnalyticsCategory.PlanPickerMap, AnalyticsAction.PlanPickerMapAddressFound)
        mMap.addMarker(mTmpModel!!)
        mView.hideProgress()
    }

    private fun handleError(toReport: AnalyticsAction, toShow: Int) {
        mReporter.reportEvent(AnalyticsCategory.PlanPickerMap, toReport)
        mSnackBarHandler.showSnackBar(toShow)
        mView.hideProgress()
    }
}