package com.taximeter.fragments.plan.dao.directions

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DirectionsSegment constructor(val distance: DirectionsSegmentItem,
                                         val duration: DirectionsSegmentItem,
                                         @SerializedName("end_address") val endAddress: String,
                                         @SerializedName("start_address") val startAddress: String)
    : Parcelable