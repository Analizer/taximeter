package com.taximeter.fragments.plan.favorites.mvp

import com.taximeter.R
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.favorites.FavoritesAdapter
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.persistence.database.DatabaseModule
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.utils.SnackBarHandler
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class FavoritesPresenter @Inject constructor(private val mAdapter: FavoritesAdapter,
                                             private val mNavigator: PlanNavigator,
                                             private val mPlanModel: PlanContract.Model,
                                             private val mModel: FavoritesContract.Model,
                                             private val mReporter: Reporter,
                                             private val mSnackBarHandler: SnackBarHandler,
                                             private val mDatabase: DatabaseModule)
    : FavoritesContract.Presenter {

    internal lateinit var mView: FavoritesContract.View

    override fun onViewAvailable(view: FavoritesContract.View) {
        mView = view
    }

    override fun onResume() {
        mDatabase.getFavorites { favorites -> onModelReady(favorites) }
    }

    override fun tearDown() {
        mSnackBarHandler.hideSnackBar()
    }

    override fun onItemClicked(item: Stop) {
        mReporter.reportEvent(AnalyticsCategory.Favorites, AnalyticsAction.OpenFavorite)
        mPlanModel.updateItemToEdit(item)
        mNavigator.returnToPlanPage(mPlanModel)
    }

    override fun deleteItem(position: Int, toReport: AnalyticsLabel) {
        mReporter.reportEvent(AnalyticsCategory.Favorites, AnalyticsAction.DeleteFavorite, toReport)
        val favoriteToDelete = mModel.get(position)
        removeItemFromModel(position)
        mSnackBarHandler.showSnackBar(
                messageId = (R.string.saved_trip_deleted_snackbar_msg),
                onUndoClick = { undoDelete(position, favoriteToDelete) },
                onSnackBarDismissed = { mDatabase.deleteFavorite(favoriteToDelete.position) })
    }

    private fun onModelReady(favorites: MutableList<Stop>?) {
        mModel.init(favorites)
        mAdapter.init(this)
        mView.setup()
        showOrHideEmptyMessage()
    }

    private fun removeItemFromModel(position: Int) {
        mModel.remove(position)
        mAdapter.notifyItemRemoved(position)
        mAdapter.notifyItemRangeChanged(position, mModel.size())
        showOrHideEmptyMessage()
    }

    private fun undoDelete(adapterPosition: Int, favorite: Stop) {
        mReporter.reportEvent(AnalyticsCategory.Favorites, AnalyticsAction.UndoDeleteFavorite)
        mModel.add(adapterPosition, favorite)
        mAdapter.notifyItemInserted(adapterPosition)
        mAdapter.notifyItemRangeChanged(adapterPosition, mModel.size())
        mView.scrollToPosition(adapterPosition)
        mView.hideEmptyMessage()
    }

    private fun showOrHideEmptyMessage() {
        if (mAdapter.itemCount == 0) mView.showEmptyMessage() else mView.hideEmptyMessage()
    }
}