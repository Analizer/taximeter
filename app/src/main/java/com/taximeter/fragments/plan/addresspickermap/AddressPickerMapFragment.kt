package com.taximeter.fragments.plan.addresspickermap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.plan.addresspickermap.mvp.AddressPickerMapContract
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class AddressPickerMapFragment : DaggerFragment(), Page {

    @Inject
    lateinit var mPresenter: AddressPickerMapContract.Presenter
    @Inject
    lateinit var mView: AddressPickerMapContract.View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_plan_picker_map, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDestroyView()
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.ADDRESS_PICKER_MAP
    }

    override fun onPause() {
        super.onPause()
        mPresenter.onPause()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPresenter.onRequestPermissionsResult()
    }
}
