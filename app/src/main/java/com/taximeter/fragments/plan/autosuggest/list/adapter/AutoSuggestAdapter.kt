package com.taximeter.fragments.plan.autosuggest.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.autosuggest.list.view.AutoSuggestAddressViewHolder
import com.taximeter.fragments.plan.autosuggest.list.view.AutoSuggestPlaceViewHolder
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.utils.calculator.TimeDistanceFormatter
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AutoSuggestAdapter @Inject constructor(private val picasso: Picasso,
                                             private val mCalculator: TimeDistanceFormatter)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var presenter: AutoSuggestContract.Presenter
    private var elements: MutableList<Stop> = mutableListOf()

    fun init(presenter: AutoSuggestContract.Presenter) {
        this.presenter = presenter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val row: View
        when (viewType) {
            CARD_TYPE_ADDRESS -> {
                row = LayoutInflater.from(parent.context).inflate(R.layout.autosuggest_item_address, parent, false)
                viewHolder = AutoSuggestAddressViewHolder(row)
            }
            CARD_TYPE_PLACE -> {
                row = LayoutInflater.from(parent.context).inflate(R.layout.autosuggest_item_place, parent, false)
                viewHolder = AutoSuggestPlaceViewHolder(row)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = elements[position]
        val root = when (item.type) {
            AutoSuggestType.ADDRESS -> getAddressRow(holder as AutoSuggestAddressViewHolder, item)
            AutoSuggestType.PLACE -> getPlaceRow(holder as AutoSuggestPlaceViewHolder, item)
            else -> getAddressRow(holder as AutoSuggestAddressViewHolder, item) //TODO remove
        }
        root.setOnClickListener { presenter.onAutoSuggestItem(item) }
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    override fun getItemViewType(position: Int): Int {
        val stop = elements[position]
        return when (stop.type) {
            AutoSuggestType.ADDRESS -> CARD_TYPE_ADDRESS
            AutoSuggestType.PLACE -> CARD_TYPE_PLACE
            else -> throw IllegalArgumentException("Wrong AutoSuggestType provided in AutoSuggestAdapter / getItemViewType: ${stop.type}")
        }
    }

    fun update(newItems: MutableList<Stop>) {
        elements.clear()
        elements.addAll(newItems)
        notifyDataSetChanged()
    }

    fun clear() {
        elements.clear()
        notifyDataSetChanged()
    }

    private fun getAddressRow(holder: AutoSuggestAddressViewHolder, stop: Stop): View {
        holder.primaryText.text = stop.getText()
        holder.distance.text = mCalculator.getKilometersTextByMeters(stop.distance)
        return holder.root
    }

    private fun getPlaceRow(holder: AutoSuggestPlaceViewHolder, stop: Stop): View {
        holder.primaryText.text = stop.getText()
        holder.secondaryText.text = stop.getSecondaryText()
        holder.distance.text = mCalculator.getKilometersTextByMeters(stop.distance)
        picasso.load((stop as PlaceModel).icon).into(holder.icon)
        return holder.root
    }

    companion object {
        private const val CARD_TYPE_ADDRESS = 1
        private const val CARD_TYPE_PLACE = 2
    }
}
