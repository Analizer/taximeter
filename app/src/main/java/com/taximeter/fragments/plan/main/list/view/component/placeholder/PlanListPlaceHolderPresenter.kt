package com.taximeter.fragments.plan.main.list.view.component.placeholder

import com.taximeter.R
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.main.list.view.component.PlanListCardPresenter
import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import com.taximeter.fragments.plan.main.modules.CurrentAddressRetriever
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import javax.inject.Inject

class PlanListPlaceHolderPresenter @Inject constructor(private val mCurrentAddressRetriever: CurrentAddressRetriever,
                                                       private val mReporter: Reporter)
    : PlanListCardPresenter {

    private lateinit var mView: PlanListPlaceHolderView
    private lateinit var mPresenter: PlanContract.Presenter

    override fun onViewAvailable(view: BasePlanViewHolder, presenter: PlanContract.Presenter) {
        mView = view as PlanListPlaceHolderView
        mPresenter = presenter
    }

    override fun bind(index: Int) {
        when (index) {
            0 -> setupSource()
            1 -> setupDestination()
        }
    }

    override fun updateArrow() {
        //NO-OP
    }

    private fun setupSource() {
        mView.showArrow()
        mView.showCurrentLocation()
        mView.setText(R.string.plan_source_card_text)
        mView.setRootClickListener { mPresenter.onItemClicked(0) }
        mView.setCurrentLocationClickedListener { fillCurrentLocationCard() }
    }

    private fun setupDestination() {
        mView.hideArrow()
        mView.hideCurrentLocation()
        mView.setText(R.string.plan_destination_card_text)
        mView.setRootClickListener { mPresenter.onItemClicked(1) }
    }

    private fun fillCurrentLocationCard() {
        mCurrentAddressRetriever.requestAddress(
                onSuccess = { stop: Stop ->
                    mPresenter.hideProgress()
                    mPresenter.model.source = stop
                    mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.CurrentLocationOnPlan)
                    mPresenter.onItemChanged(0)
                })
    }
}