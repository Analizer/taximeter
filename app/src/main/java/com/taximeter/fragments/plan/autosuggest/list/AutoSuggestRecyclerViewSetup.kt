package com.taximeter.fragments.plan.autosuggest.list

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import com.taximeter.utils.KeyboardHandler
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AutoSuggestRecyclerViewSetup @Inject constructor(private val mAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
                                                       private val mKeyboardHandler: KeyboardHandler) {

    fun setup(recyclerView: RecyclerView) {
        recyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = mAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == SCROLL_STATE_DRAGGING) {
                    mKeyboardHandler.hideKeyboard(recyclerView)
                }
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }
}