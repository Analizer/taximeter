package com.taximeter.fragments.plan.main.mvp

import com.taximeter.R
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.fragments.plan.main.list.PlanListAdapter
import com.taximeter.fragments.plan.main.map.PlanMapHeaderBinder
import com.taximeter.fragments.plan.main.map.PlanMapMapModule
import com.taximeter.fragments.plan.main.modules.PlanAppBarHandler
import com.taximeter.fragments.plan.main.modules.PlanDirectionsRetreiver
import com.taximeter.fragments.plan.main.modules.PlanNavigator
import com.taximeter.persistence.database.DatabaseModule
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.service.location.LocationSubscriber
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.SnackBarHandler
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.location.CurrentLocationRetriever
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanPresenter @Inject constructor(override val model: PlanContract.Model,
                                        private val mSnackBarHandler: SnackBarHandler,
                                        private val mMap: PlanMapMapModule,
                                        private val mListAdapter: PlanListAdapter,
                                        private val mDialogFactory: DialogFactory,
                                        private val mReporter: Reporter,
                                        private val mNavigator: PlanNavigator,
                                        private val mHeaderBinder: PlanMapHeaderBinder,
                                        private val mDirectionsHandler: PlanDirectionsRetreiver,
                                        private val mLocationRetriever: CurrentLocationRetriever,
                                        private val mAppBarHandler: PlanAppBarHandler,
                                        private val mErrorHandler: ErrorHandler,
                                        private val mDatabase: DatabaseModule)
    : PlanContract.Presenter {

    lateinit var mView: PlanContract.View

    private var mFinishedPlanning = true

    override fun onViewAvailable(view: PlanContract.View) {
        mView = view
        mLocationRetriever.addSubscriber(LocationSubscriber.PLAN)
        mMap.init(this)
        mHeaderBinder.init(mView)//TODO! innen valamiket nem kene onresumeba?
        mAppBarHandler.init(mView)
        mView.initAppBarLayout()
        mDialogFactory.showDialogIfNeeded(DialogType.PLAN)
    }

    override fun onResume(planToRestore: PlanModel?) {
        mView.setupRecyclerView()
        mListAdapter.init(this)
        mFinishedPlanning = true
        if (planToRestore != null) loadPlanModel(planToRestore)
        subscribeToFavoriteStates()
    }

    override fun onPause() {
        saveModelIfComplete()
        mDirectionsHandler.tearDown()
        mView.hideCarFab()
    }

    override fun onDestroyView() {
        mLocationRetriever.tearDown()
        mMap.tearDown()
    }

    override fun onDataSetChanged() {
        mListAdapter.notifyDataSetChanged()
        mMap.update(model)
        mHeaderBinder.update(model)
        requestDirections()
    }

    override fun onItemChanged(itemPosition: Int) {
        mListAdapter.notifyItemChanged(itemPosition)
    }

    override fun onItemClicked(itemPosition: Int) {
        mFinishedPlanning = false
        hideProgress()
        model.prepareToEdit(itemPosition)
        mNavigator.showAutoSuggest(model)
    }

    override fun heartItem(itemPosition: Int) {
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.FavoriteIconUsed, AnalyticsLabel.FavoriteAdded)
        mDatabase.saveFavorite(model.get(itemPosition)) { showSnackBar(R.string.added_to_favorites_msg) }
    }

    override fun unHeartItem(itemPosition: Int) {
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.FavoriteIconUsed, AnalyticsLabel.FavoriteRemoved)
        mDatabase.deleteFavorite(model.get(itemPosition).position) { showSnackBar(R.string.removed_from_favorites_msg) }
    }

    override fun deleteItem(itemPosition: Int) {
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.DeletePlanItem, AnalyticsLabel.Swipe)
        val stopToDelete = model.get(itemPosition)
        model.resetStop(itemPosition)
        mListAdapter.notifyItemChanged(itemPosition)
        if (itemPosition == 1) mListAdapter.updateArrow()
        mMap.update(model)
        mSnackBarHandler.showSnackBar(
                messageId = (R.string.plan_snackbar_msg),
                onUndoClick = { undoDelete(itemPosition, stopToDelete) })
    }

    override fun showSnackBar(messageId: Int, onUndoClick: (() -> Unit)?) {
        mSnackBarHandler.showSnackBar(messageId, onUndoClick)
    }

    override fun showProgress() {
        mView.showProgress()
    }

    override fun hideProgress() {
        mView.hideProgress()
    }

    override fun handleCarFabClicked() {
        if (model.hasDirections()) {
            mDialogFactory.showDialogIfNeeded(DialogType.START_MEASUREMENT_FROM_PLAN,
                    { startMeasurement() },
                    { mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.MeasurementCancelledFromPlanDialog) })
        } else {
            throw IllegalArgumentException("Car fab should not be visible without a DirectionsModel")
        }
    }

    override fun handleMapClicked() {
        mAppBarHandler.switch()
    }

    override fun onRequestPermissionsResult() {
        mLocationRetriever.onRequestPermissionsResult()
    }

    override fun onCurrentLocationClickedOnMap() {
        mLocationRetriever.requestLocation(onSuccess = { location ->
            hideProgress()
            mMap.zoomToLocation(location)
        })
    }

    private fun startMeasurement() {
        mLocationRetriever.requestLocation(onSuccess = { location ->
            hideProgress()
            mNavigator.startMeasurement(model, location)
        })
    }

    private fun loadPlanModel(planToRestore: PlanModel) {
        model.load(planToRestore)
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.PlanItemsAdded)
        mListAdapter.update(model)
        mMap.update(model)
        requestDirections()
    }

    private fun subscribeToFavoriteStates() {
        mDatabase.getFavorites { favorites ->
            model.updateFavoriteStates(favorites)
            mListAdapter.notifyDataSetChanged()
        }
    }

    private fun requestDirections() {
        if (model.hasRealStops()) {
            showProgress()
            mDirectionsHandler.requestDirections(model,
                    { directionModel -> handleDirectionsArrived(directionModel) },
                    { mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.PlanDirectionsError, R.string.plan_directions_error_msg) })
        }
        mHeaderBinder.hideHeader()
        mView.hideCarFab()
        mView.hideCurrentLocationButton()
    }

    private fun handleDirectionsArrived(direction: DirectionModel) {
        hideProgress()
        model.updateDirections(direction)
        if (direction.isValid) {
            mListAdapter.updateArrow()
            mMap.update(model)
            mHeaderBinder.update(model)
        } else {
            mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.PlanDirectionsError, R.string.error_message_try_again)
        }
    }

    private fun undoDelete(adapterPosition: Int, stop: Stop) {
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.UndoDeleteStop)
        model.updateStop(adapterPosition, stop)
        mListAdapter.notifyItemChanged(adapterPosition)
        if (adapterPosition == 1) mListAdapter.updateArrow()
        mMap.update(model)
    }

    private fun saveModelIfComplete() {
        if (mFinishedPlanning && model.hasRealStops()) {
            mDatabase.getPlans { plans ->
                if (plans != null && !plans.contains(model)) {
                    mDatabase.savePlan(model)
                }
            }
        }
    }
}