package com.taximeter.fragments.plan.main.list.view.holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.plan_arrow_row.view.*

class PlanArrowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val distance: TextView = itemView.plan_arrow_distance
    val duration: TextView = itemView.plan_arrow_time
    val arrow: ImageView = itemView.arrow_image
    val root: View = itemView.arrow_row
}
