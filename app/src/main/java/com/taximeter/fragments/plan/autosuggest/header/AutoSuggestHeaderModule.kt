package com.taximeter.fragments.plan.autosuggest.header

import android.view.View
import com.taximeter.fragments.plan.autosuggest.mvp.AutoSuggestContract
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.auto_suggest_header.view.*
import javax.inject.Inject

@FragmentScoped
class AutoSuggestHeaderModule @Inject constructor() {

    private lateinit var currentLocationRow: View
    private lateinit var favoritesRow: View
    private lateinit var recentsRow: View
    private lateinit var pickerMapRow: View

    fun onViewAvailable(view: View) {
        currentLocationRow = view.auto_suggest_current_location_row
        favoritesRow = view.auto_suggest_favorites_row
        recentsRow = view.auto_suggest_recents_row
        pickerMapRow = view.auto_suggest_address_picker_map_row
    }

     fun bind(presenter: AutoSuggestContract.Presenter) {
        currentLocationRow.setOnClickListener { presenter.onCurrentLocation() }
        favoritesRow.setOnClickListener { presenter.onFavorites() }
        recentsRow.setOnClickListener { presenter.onRecents() }
        pickerMapRow.setOnClickListener { presenter.onMap() }
    }
}