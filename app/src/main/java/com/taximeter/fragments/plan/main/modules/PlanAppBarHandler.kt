package com.taximeter.fragments.plan.main.modules

import com.google.android.material.appbar.AppBarLayout
import com.taximeter.fragments.plan.main.mvp.PlanContract
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanAppBarHandler @Inject constructor(): AppBarLayout.OnOffsetChangedListener {

    private var mAppbarExpanded: Boolean = false
    private lateinit var mView: PlanContract.View

    fun init(view: PlanContract.View){
        mView = view
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        mAppbarExpanded = verticalOffset == 0
        if (mAppbarExpanded) mView.showCurrentLocationButton() else mView.hideCurrentLocationButton()
    }

    fun switch() {
        mAppbarExpanded = !mAppbarExpanded
        mView.setAppBarExpanded(mAppbarExpanded)
    }
}