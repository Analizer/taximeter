package com.taximeter.fragments.plan.autosuggest.mvp

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.plan.autosuggest.header.AutoSuggestHeaderModule
import com.taximeter.fragments.plan.autosuggest.list.AutoSuggestRecyclerViewSetup
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_autosuggest.view.*
import javax.inject.Inject

@FragmentScoped
class AutoSuggestView @Inject constructor(private val mHeaderModule: AutoSuggestHeaderModule,
                                          private val mRecyclerViewSetup: AutoSuggestRecyclerViewSetup,
                                          private val mProgressBarModule: ProgressBarModule)
    : AutoSuggestContract.View {

    private lateinit var recyclerView: RecyclerView

    override fun bind(pageView: View) {
        mHeaderModule.onViewAvailable(pageView)
        mProgressBarModule.bind(pageView)
        recyclerView = pageView.recycler_view
        setupRecyclerView()
    }

    override fun showProgress() {
        mProgressBarModule.showProgress()
    }

    override fun hideProgress() {
        mProgressBarModule.hideProgress()
    }

    private fun setupRecyclerView() {
        mRecyclerViewSetup.setup(recyclerView)
    }
}