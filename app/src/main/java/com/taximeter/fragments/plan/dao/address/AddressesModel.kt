package com.taximeter.fragments.plan.dao.address

import com.google.gson.annotations.SerializedName

data class AddressesModel constructor(@SerializedName("predictions") var addresses: List<AddressModel>)
