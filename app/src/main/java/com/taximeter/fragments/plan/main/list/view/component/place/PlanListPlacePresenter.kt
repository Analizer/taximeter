package com.taximeter.fragments.plan.main.list.view.component.place

import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.fragments.plan.main.list.view.component.PlanListCardPresenter
import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.calculator.TimeDistanceFormatter

class PlanListPlacePresenter constructor(private val mCalculator: TimeDistanceFormatter)
    : PlanListCardPresenter {

    private lateinit var mView: PlanListPlaceView
    private lateinit var mPresenter: PlanContract.Presenter

    override fun onViewAvailable(view: BasePlanViewHolder, presenter: PlanContract.Presenter) {
        mView = view as PlanListPlaceView
        mPresenter = presenter
    }

    override fun bind(index: Int) {
        val model: PlaceModel = mPresenter.model.get(index) as PlaceModel
        mView.setText(model.getText())
        mView.setSubtext(model.getSecondaryText())
        mView.setDistance(mCalculator.getKilometersTextByMeters(model.distance))
        mView.setFavorite(model.favorite)
        mView.setFavoriteClickedListener { if (!model.favorite) mPresenter.heartItem(index) else mPresenter.unHeartItem(index) }
        mView.setDeleteClickedListener { mPresenter.deleteItem(index) }
        mView.setIcon(model.icon)
        when (index) {
            0 -> setupSource()
            1 -> setupDestination()
            else -> throw IllegalArgumentException("Wrong argument index ($index) in PlanPlacePresenter bind")
        }
    }

    override fun updateArrow() {
        //The index is always 0 here
        mView.showArrow()
        if (mPresenter.model.hasRealStops() && mPresenter.model.hasDirections()) {
            mView.setArrowDistance(mCalculator.getKilometersTextByMeters(mPresenter.model.getDirections()!!.distance))
            mView.setArrowDuration(mCalculator.getFormattedTimeBySeconds(mPresenter.model.getDirections()!!.duration.toLong(), true))
        } else {
            mView.clearArrow()
        }
    }

    private fun setupSource() {
        mView.setRootClickListener { mPresenter.onItemClicked(0) }
        updateArrow()
    }

    private fun setupDestination() {
        mView.setRootClickListener { mPresenter.onItemClicked(1) }
        mView.hideArrow()
    }
}