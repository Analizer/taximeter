package com.taximeter.fragments.plan.recents

import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.recents.adapter.RecentsAdapter
import com.taximeter.fragments.plan.recents.adapter.RecentsViewHolder
import com.taximeter.fragments.plan.recents.mvp.RecentsContract
import com.taximeter.fragments.plan.recents.mvp.RecentsModel
import com.taximeter.fragments.plan.recents.mvp.RecentsPresenter
import com.taximeter.fragments.plan.recents.mvp.RecentsView
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class RecentsModule {

    @Binds
    abstract fun presenter(presenter: RecentsPresenter): RecentsContract.Presenter

    @Binds
    abstract fun view(view: RecentsView): RecentsContract.View

    @Binds
    abstract fun model(model: RecentsModel): RecentsContract.Model

    @Binds
    abstract fun listListener(presenter: RecentsContract.Presenter): ListListener<PlanContract.Model>

    @Binds
    abstract fun adapter(adapter: RecentsAdapter): RecyclerView.Adapter<RecentsViewHolder>

    companion object {

        @Provides
        fun planModel(fragment: RecentsFragment): PlanContract.Model {
            return fragment.requireArguments().getParcelable(Extra.TAG_MODEL.tag)!!
        }
    }
}
