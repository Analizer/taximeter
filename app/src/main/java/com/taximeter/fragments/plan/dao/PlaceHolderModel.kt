package com.taximeter.fragments.plan.dao

import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceHolderModel constructor(override var type: AutoSuggestType,
                                        override var position: LatLng,
                                        override var postalCode: String,
                                        override var distance: Int,
                                        override var favorite: Boolean = false)
    : Stop {

    override fun getText(): String {
        return ""//TODO
    }

    constructor() : this(AutoSuggestType.NONE, LatLng(0.0, 0.0), "", 0)
}