package com.taximeter.fragments.plan.autosuggest.transformer

import com.google.android.gms.maps.model.LatLng
import com.taximeter.communication.endpoint.PostalCodesEndpoint
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.postalcode.PostalCodesResultModel
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * Checks if the @Stop model provided already has a postal code, requests one and adds it to the model if not. Returns the original model otherwise.
 */
class PostalCodeTransformer @Inject constructor(private val mEndpoint: PostalCodesEndpoint)
    : ObservableTransformer<Stop, Stop> {

    override fun apply(upstream: Observable<Stop>): ObservableSource<Stop> {
        return upstream
                .flatMap { addressModel -> addPostalCodeToModelIfNeeded(addressModel) }
    }

    private fun addPostalCodeToModelIfNeeded(model: Stop): Observable<Stop> {
        return if (model.postalCode.isEmpty()) {
            getPostalCodes(model.position)
                    .map { postalCodesResultModel ->
                        model.postalCode = postalCodesResultModel.postalCode
                        model
                    }
        } else {
            Observable.just(model)
        }
    }

    private fun getPostalCodes(location: LatLng): Observable<PostalCodesResultModel> {
        return mEndpoint.getPostalCodes(location.latitude, location.longitude, "HU", 10, "Analizer")
    }
}