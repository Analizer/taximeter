package com.taximeter.fragments.plan.addresspickermap

import android.annotation.SuppressLint
import android.location.Location
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.taximeter.R
import com.taximeter.fragments.di.ChildFragmentManager
import com.taximeter.fragments.plan.addresspickermap.mvp.AddressPickerMapContract
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.main.map.PlanInfoWindowAdapter
import com.taximeter.utils.MapHelper
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AddressPickerMap @Inject constructor(private val mInfoWindowAdapter: PlanInfoWindowAdapter,
                                           @ChildFragmentManager private val mFragmentManager: FragmentManager,
                                           private val mMapHelper: MapHelper)
    : OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private var mMap: GoogleMap? = null
    private lateinit var mPresenter: AddressPickerMapContract.Presenter
    private var mMarker: Marker? = null

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setupMap()
        mPresenter.onMapReady()
    }

    override fun onMapClick(position: LatLng) {
        mPresenter.onMapClick(position.latitude, position.longitude)
    }

    fun init(presenter: AddressPickerMapContract.Presenter) {
        mPresenter = presenter
        (mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)
    }

    fun tearDown() {
        val fragment = mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        if (fragment != null) {
            mFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        }
    }

    fun addMarker(stop: Stop) {
        mMarker?.remove()
        mMarker = mMapHelper.addMarkerToMap(mMap!!, stop.position)
        mMapHelper.zoomToLocation(stop.position, mMap!!)
        setupInfoWindow(stop)
        mMarker!!.showInfoWindow()
    }

    fun zoomToCurrentLocation(location: Location) {
        mMapHelper.zoomToLocation(location, mMap!!)
    }

    @SuppressLint("MissingPermission")
    fun showUserLocationOnMap() {
        mMap!!.isMyLocationEnabled = true
    }

    private fun setupMap() {
        setupMapSettings()
        mMapHelper.zoomToBudapest(mMap!!)
        mMap!!.setOnMapClickListener(this)
    }

    private fun setupInfoWindow(stop: Stop) {
        val newModel = listOf(stop)
        mInfoWindowAdapter.updateModel(newModel)
        mMap!!.setInfoWindowAdapter(mInfoWindowAdapter)
    }

    private fun setupMapSettings() {
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.setAllGesturesEnabled(true)
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
    }
}