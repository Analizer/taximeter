package com.taximeter.fragments.plan.main.list.view.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.taximeter.R
import kotlinx.android.synthetic.main.plan_address_row.view.*

class PlanAddressViewHolder(itemView: View) : BasePlanViewHolder(itemView) {

    val distanceTv: TextView = itemView.plan_distance
    val deleteIcn: ImageView = itemView.findViewById(R.id.plan_delete)
    val addressTv: TextView = itemView.findViewById(R.id.plan_address)
    val favoriteIcn: ImageView = itemView.findViewById(R.id.plan_favorite)
}
