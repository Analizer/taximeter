package com.taximeter.fragments.plan.dao.place

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceGeometryModel constructor(var location: CoordinatesModel) : Parcelable
