package com.taximeter.fragments.plan.recents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.plan.recents.adapter.RecentsAdapter
import com.taximeter.fragments.plan.recents.mvp.RecentsContract
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class RecentsFragment : DaggerFragment(), Page {

    @Inject
    lateinit var mAdapter: RecentsAdapter
    @Inject
    lateinit var mPresenter: RecentsContract.Presenter
    @Inject
    lateinit var mView: RecentsContract.View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_list, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.RECENT_PLANS
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.tearDown()
    }
}
