package com.taximeter.fragments.plan.main.list.view.component.placeholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import kotlinx.android.synthetic.main.plan_placeholder_row.view.*

class PlanListPlaceHolderViewImpl constructor(itemView: View)
    : PlanListPlaceHolderView, BasePlanViewHolder(itemView) {

    val text: TextView = itemView.plan_place_holder_text
    private val currentLocation: ImageView = itemView.plan_placeholder_current_location

    override fun setText(textResId: Int) {
        text.setText(textResId)
    }

    override fun showCurrentLocation() {
        currentLocation.visibility = View.VISIBLE
    }

    override fun hideCurrentLocation() {
        currentLocation.visibility = View.GONE
    }

    override fun setRootClickListener(onClick: () -> Unit) {
        root.setOnClickListener { onClick.invoke() }
    }

    override fun setCurrentLocationClickedListener(onClick: () -> Unit) {
        currentLocation.setOnClickListener { onClick.invoke() }
    }

    override fun showArrow() {
        arrowRow.root.visibility = View.VISIBLE
    }

    override fun hideArrow() {
        arrowRow.root.visibility = View.GONE
    }
}