package com.taximeter.fragments.plan.main.list.view.component.place

interface PlanListPlaceView {

        fun setText(text: String)

        fun setSubtext(subtext: String)

        fun setFavorite(isFavorite: Boolean)

        fun setFavoriteClickedListener(onClick: () -> Unit)

        fun setRootClickListener(onClick: () -> Unit)

        fun setDeleteClickedListener(onClick: () -> Unit)

        fun setDistance(distance: String)

        fun setIcon(iconUrl: String)

        fun showArrow()

        fun hideArrow()

        fun clearArrow()

        fun setArrowDistance(distance: String)

        fun setArrowDuration(duration: String)
}