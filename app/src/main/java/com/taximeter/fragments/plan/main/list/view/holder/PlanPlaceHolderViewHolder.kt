package com.taximeter.fragments.plan.main.list.view.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.plan_placeholder_row.view.*

class PlanPlaceHolderViewHolder(itemView: View) : BasePlanViewHolder(itemView) {

    val text: TextView = itemView.plan_place_holder_text
    val currentLocation: ImageView = itemView.plan_placeholder_current_location
}
