package com.taximeter.fragments.plan.main.list.view.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.taximeter.R
import kotlinx.android.synthetic.main.plan_place_row.view.*

class PlanPlaceViewHolder(itemView: View) : BasePlanViewHolder(itemView) {

    val deleteIcn: ImageView = itemView.findViewById(R.id.plan_delete)
    val addressTv: TextView = itemView.findViewById(R.id.plan_address)
    val favoriteIcn: ImageView = itemView.findViewById(R.id.plan_favorite)
    var icon: ImageView = itemView.plan_place_type_img
    var placeNameTv: TextView = itemView.plan_place_name
    var distanceTv: TextView = itemView.plan_distance
}
