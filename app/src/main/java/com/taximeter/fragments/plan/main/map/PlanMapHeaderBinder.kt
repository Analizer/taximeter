package com.taximeter.fragments.plan.main.map

import android.content.res.Resources
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.utils.CalculationUtils.*
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanMapHeaderBinder @Inject constructor(private val mResources: Resources,
                                              private var mModel: PlanContract.Model) {

    private lateinit var mView: PlanContract.View

    fun init(view: PlanContract.View) {
        mView = view
        updateHeader()
    }

    fun update(model: PlanContract.Model) {
        mModel = model
        updateHeader()
    }

    fun hideHeader() {
        mView.hideHeader()
    }

    private fun updateHeader() {
        if (mModel.hasDirections()) {
            fillData()
            mView.showHeader()
        } else {
            hideHeader()
        }
    }

    private fun fillData() {
        mView.setDistanceText(getKilometersTextByMeters(mModel.getDirections()!!.distance, mResources))
        mView.setTimeText(getFormattedTimeBySeconds(mModel.getDirections()!!.duration.toLong(), mResources, true))
        mView.setPriceText(getFormattedPrice(getRoundedPrice(mModel.getDirections()!!.estimatedPrice), mResources))
    }
}
