package com.taximeter.fragments.plan.dao.directions;

import android.os.Parcel;
import android.os.Parcelable;

import com.taximeter.fragments.plan.dao.place.CoordinatesModel;

/**
 * Created by Peter_Goczan on 10/28/2016.
 */

public class DirectionsBounds implements Parcelable {

    CoordinatesModel northeast;
    CoordinatesModel southwest;

    public CoordinatesModel getNortheast() {
        return northeast;
    }

    public void setNortheast(CoordinatesModel northeast) {
        this.northeast = northeast;
    }

    public CoordinatesModel getSouthwest() {
        return southwest;
    }

    public void setSouthwest(CoordinatesModel southwest) {
        this.southwest = southwest;
    }

    protected DirectionsBounds(Parcel in) {
        northeast = (CoordinatesModel) in.readValue(CoordinatesModel.class.getClassLoader());
        southwest = (CoordinatesModel) in.readValue(CoordinatesModel.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(northeast);
        dest.writeValue(southwest);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DirectionsBounds> CREATOR = new Parcelable.Creator<DirectionsBounds>() {
        @Override
        public DirectionsBounds createFromParcel(Parcel in) {
            return new DirectionsBounds(in);
        }

        @Override
        public DirectionsBounds[] newArray(int size) {
            return new DirectionsBounds[size];
        }
    };
}
