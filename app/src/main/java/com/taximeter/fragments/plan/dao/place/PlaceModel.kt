package com.taximeter.fragments.plan.dao.place

import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceModel constructor(var geometry: PlaceGeometryModel,
                                  var icon: String,
                                  var name: String,
                                  var vicinity: String,
                                  override var type: AutoSuggestType = AutoSuggestType.PLACE,
                                  override var position: LatLng = LatLng(geometry.location.lat, geometry.location.lng),
                                  override var distance: Int = 0,
                                  override var postalCode: String = "",
                                  override var favorite: Boolean = false)
    : Stop {

    override fun getText(): String {
        return name
    }

    override fun getSecondaryText(): String {
        removeUnnecessaryAddressParts() //I would put these into an onResume block but on autosuggest it isn't called unfortunately (Gson skips it or something)
        val sb = StringBuilder("$postalCode, ")
        sb.append(vicinity)
        return sb.toString()
    }

    private fun removeUnnecessaryAddressParts() {
        vicinity = vicinity
                .replace("Budapest, ", "")
                .replace(", Hungary", "")
                .replace(" Hungary", "")
                .trim()
    }
}
