package com.taximeter.fragments.plan.dao.place

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CoordinatesModel constructor(var lat: Double, var lng: Double) : Parcelable
