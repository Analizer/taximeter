package com.taximeter.fragments.plan.favorites

import android.location.Location
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.taximeter.R
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.fragments.plan.favorites.mvp.FavoritesContract
import com.taximeter.fragments.plan.main.list.view.holder.PlanAddressViewHolder
import com.taximeter.fragments.plan.main.list.view.holder.PlanPlaceViewHolder
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.calculator.TimeDistanceFormatter
import com.taximeter.utils.touchhelper.SwipeToDeleteCallback
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class FavoritesAdapter @Inject constructor(private val mModel: FavoritesContract.Model,
                                           private val mPicasso: Picasso,
                                           private val mGeoCalculator: GeoCalculator,
                                           private val mFormatter: TimeDistanceFormatter,
                                           private val mCurrentLocation: Location)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), SwipeToDeleteCallback {

    private lateinit var mListener: ListListener<Stop>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CARD_TYPE_ADDRESS -> PlanAddressViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.plan_address_row, parent, false))
            CARD_TYPE_PLACE -> PlanPlaceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.plan_place_row, parent, false))
            else -> throw IllegalArgumentException("Wrong viewType in FavoriteListAdapter onCreateViewHolder ($viewType)")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (mModel.get(position).type) {
            AutoSuggestType.CURRENT_LOCATION, AutoSuggestType.ADDRESS -> initAddressCard(holder as PlanAddressViewHolder, position)
            AutoSuggestType.PLACE -> initPlaceCard(holder as PlanPlaceViewHolder, position)
            AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong favorite type in FavoriteListAdapter onBindViewHolder (AutoSuggestType.NONE)")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mModel.get(position).type) {
            AutoSuggestType.CURRENT_LOCATION, AutoSuggestType.ADDRESS -> CARD_TYPE_ADDRESS
            AutoSuggestType.PLACE -> CARD_TYPE_PLACE
            AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong favorite type in FavoriteListAdapter getItemViewType (AutoSuggestType.NONE)")
        }
    }

    override fun getItemCount(): Int {
        return mModel.size()
    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder) {
        mListener.deleteItem(viewHolder.adapterPosition, AnalyticsLabel.Swipe)
    }

    fun init(listener: ListListener<Stop>) {
        mListener = listener
    }

    private fun initAddressCard(holder: PlanAddressViewHolder, position: Int) {
        val model = mModel.get(position)
        model.distance = mGeoCalculator.distance(mCurrentLocation, model.position)
        holder.addressTv.text = model.getText()
        holder.distanceTv.text = mFormatter.getKilometersTextByMeters(mGeoCalculator.distance(mCurrentLocation, model.position))
        holder.root.setOnClickListener { mListener.onItemClicked(model) }
        holder.deleteIcn.setOnClickListener { mListener.deleteItem(holder.adapterPosition, AnalyticsLabel.DeleteIcon) }
        holder.favoriteIcn.visibility = View.GONE
    }

    private fun initPlaceCard(holder: PlanPlaceViewHolder, position: Int) {
        val model = mModel.get(position)
        model.distance = mGeoCalculator.distance(mCurrentLocation, model.position)
        holder.placeNameTv.text = model.getText()
        holder.distanceTv.text = mFormatter.getKilometersTextByMeters(mGeoCalculator.distance(mCurrentLocation, model.position))
        holder.root.setOnClickListener { mListener.onItemClicked(model) }
        holder.deleteIcn.setOnClickListener { mListener.deleteItem(holder.adapterPosition, AnalyticsLabel.DeleteIcon) }
        holder.favoriteIcn.visibility = View.GONE
        mPicasso.load((model as PlaceModel).icon).into(holder.icon)
    }

    companion object {
        private const val CARD_TYPE_ADDRESS = 0
        private const val CARD_TYPE_PLACE = 1
    }
}
