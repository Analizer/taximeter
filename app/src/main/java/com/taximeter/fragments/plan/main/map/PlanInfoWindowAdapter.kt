package com.taximeter.fragments.plan.main.map

import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Picasso
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.calculator.TimeDistanceFormatter
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanInfoWindowAdapter @Inject constructor(private val mInflater: LayoutInflater,
                                                private val mPicasso: Picasso,
                                                private val mReporter: Reporter,
                                                private val mCalculator: TimeDistanceFormatter)
    : GoogleMap.InfoWindowAdapter {

    private lateinit var mModel: List<Stop>

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View? {
        mReporter.reportEvent(AnalyticsCategory.Plan, AnalyticsAction.MarkerClickedOnMap)
        val stop = getStopByMarker(marker)
        return when (stop.type) {
            AutoSuggestType.ADDRESS -> getAddressView(stop)
            AutoSuggestType.PLACE -> getPlaceView(stop)
            AutoSuggestType.CURRENT_LOCATION -> getCurrentLocationView(stop)
            AutoSuggestType.NONE -> throw IllegalArgumentException("wrong autosuggest type (NONE) in PlanInfoWindowAdapter / getInfoContents")
        }
    }

    private fun getStopByMarker(marker: Marker): Stop {
        for (stop: Stop in mModel) {
            if (marker.position == stop.position) {
                return stop
            }
        }
        throw IllegalArgumentException("Marker not found in PlanInfoWindowAdapter")
    }

    private fun getAddressView(stop: Stop): View {
        val root = mInflater.inflate(R.layout.plan_info_window_address, null)
        val title = root.findViewById<TextView>(R.id.plan_info_window_title)
        val distance = root.findViewById<TextView>(R.id.plan_info_window_distance)
        title.text = stop.getText()
        distance.text = mCalculator.getKilometersTextByMeters(stop.distance)
        return root
    }

    private fun getPlaceView(stop: Stop): View {
        val root = mInflater.inflate(R.layout.plan_info_window_place, null)
        val title = root.findViewById<TextView>(R.id.plan_info_window_title)
        val address = root.findViewById<TextView>(R.id.plan_info_window_secondary_title)
        val distance = root.findViewById<TextView>(R.id.plan_info_window_distance)
        val img = root.findViewById<ImageView>(R.id.plan_info_window_img)
        val model = stop as PlaceModel
        title.text = model.getText()
        address.text = stop.getSecondaryText()
        distance.text = mCalculator.getKilometersTextByMeters(stop.distance)
        mPicasso.load(model.icon).into(img)
        return root
    }

    private fun getCurrentLocationView(stop: Stop): View {
        val root = mInflater.inflate(R.layout.plan_info_window_current_location, null)
        val title = root.findViewById<TextView>(R.id.plan_info_window_title)
        title.text = stop.getText()
        return root
    }

    fun updateModel(model: List<Stop>) {
        mModel = model
    }
}
