package com.taximeter.fragments.plan.main.list.view.holder

import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.plan_current_location_row.view.*

class PlanCurrentLocationViewHolder(itemView: View) : BasePlanViewHolder(itemView) {

    val currentLocationIcon: ImageView = itemView.plan_current_location
}
