package com.taximeter.fragments.plan.recents.mvp

import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.ListListener

interface RecentsContract {

    interface Presenter : ListListener<PlanContract.Model> {
        fun onViewAvailable(view: View)

        fun onResume()

        fun tearDown()
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun setup()

        fun showEmptyMessage()

        fun hideEmptyMessage()

        fun scrollToPosition(adapterPosition: Int)
    }

    interface Model {
        fun get(position: Int): PlanContract.Model

        fun add(adapterPosition: Int, plan: PlanContract.Model)

        fun remove(position: Int)

        fun size(): Int

        fun init(elements: MutableList<PlanContract.Model>?)
    }
}