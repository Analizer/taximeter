package com.taximeter.fragments.plan.main.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.taximeter.R
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.main.list.view.component.PlanListCardPresenter
import com.taximeter.fragments.plan.main.list.view.component.address.PlanListAddressPresenter
import com.taximeter.fragments.plan.main.list.view.component.address.PlanListAddressViewImpl
import com.taximeter.fragments.plan.main.list.view.component.currentlocation.PlanListCurrentLocationPresenter
import com.taximeter.fragments.plan.main.list.view.component.currentlocation.PlanListCurrentLocationViewImpl
import com.taximeter.fragments.plan.main.list.view.component.place.PlanListPlacePresenter
import com.taximeter.fragments.plan.main.list.view.component.place.PlanListPlaceViewImpl
import com.taximeter.fragments.plan.main.list.view.component.placeholder.PlanListPlaceHolderPresenter
import com.taximeter.fragments.plan.main.list.view.component.placeholder.PlanListPlaceHolderViewImpl
import com.taximeter.fragments.plan.main.list.view.holder.BasePlanViewHolder
import com.taximeter.fragments.plan.main.modules.CurrentAddressRetriever
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.reporting.Reporter
import com.taximeter.utils.calculator.TimeDistanceFormatter
import com.taximeter.utils.touchhelper.SwipeToDeleteCallback
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class PlanListAdapter @Inject constructor(private var mModel: PlanContract.Model,
                                          private val mCurrentAddressRetriever: CurrentAddressRetriever,
                                          private val mFormatter: TimeDistanceFormatter,
                                          private val mReporter: Reporter,
                                          private val mPicasso: Picasso)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), SwipeToDeleteCallback {

    private lateinit var mSourcePresenter: PlanListCardPresenter
    private lateinit var mPresenter: PlanContract.Presenter

    fun init(presenter: PlanContract.Presenter) {
        mPresenter = presenter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasePlanViewHolder = when (viewType) {
        CARD_TYPE_PLACE_HOLDER -> PlanListPlaceHolderViewImpl(LayoutInflater.from(parent.context).inflate(R.layout.plan_placeholder_row, parent, false))
        CARD_TYPE_CURRENT_LOCATION -> PlanListCurrentLocationViewImpl(LayoutInflater.from(parent.context).inflate(R.layout.plan_current_location_row, parent, false))
        CARD_TYPE_ADDRESS -> PlanListAddressViewImpl(LayoutInflater.from(parent.context).inflate(R.layout.plan_address_row, parent, false))
        CARD_TYPE_PLACE -> PlanListPlaceViewImpl(LayoutInflater.from(parent.context).inflate(R.layout.plan_place_row, parent, false), mPicasso)
        else -> {
            throw IllegalArgumentException("Plan list viewholder should be an instance of BasePlanViewHolder")
        }
    }


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val cardPresenter = when (mModel.get(position).type) {
            AutoSuggestType.NONE -> PlanListPlaceHolderPresenter(mCurrentAddressRetriever, mReporter)
            AutoSuggestType.ADDRESS -> PlanListAddressPresenter(mFormatter)
            AutoSuggestType.PLACE -> PlanListPlacePresenter(mFormatter)
            AutoSuggestType.CURRENT_LOCATION -> PlanListCurrentLocationPresenter(mFormatter)
        }
        if (position == 0) {
            mSourcePresenter = cardPresenter
        }
        cardPresenter.onViewAvailable(viewHolder as BasePlanViewHolder, mPresenter)
        cardPresenter.bind(position)
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun getItemViewType(position: Int): Int {
        return when (mModel.get(position).type) {
            AutoSuggestType.NONE -> CARD_TYPE_PLACE_HOLDER
            AutoSuggestType.ADDRESS -> CARD_TYPE_ADDRESS
            AutoSuggestType.PLACE -> CARD_TYPE_PLACE
            AutoSuggestType.CURRENT_LOCATION -> CARD_TYPE_CURRENT_LOCATION
        }
    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder) {
        mPresenter.deleteItem(viewHolder.adapterPosition)
    }

    fun update(model: PlanContract.Model) {
        mModel = model
        notifyDataSetChanged()
    }

    fun updateArrow() {
        mSourcePresenter.updateArrow()
    }

    companion object {
        private const val CARD_TYPE_PLACE_HOLDER = 0
        private const val CARD_TYPE_ADDRESS = 1
        private const val CARD_TYPE_CURRENT_LOCATION = 2
        private const val CARD_TYPE_PLACE = 3
    }
}
