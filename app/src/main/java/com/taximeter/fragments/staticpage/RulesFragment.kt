package com.taximeter.fragments.staticpage

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Html
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import kotlinx.android.synthetic.main.fragment_static_page.view.*

class RulesFragment : Fragment(), Page {

    private lateinit var contentTv: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_static_page, container, false)
        contentTv = pageLayout.static_page_content_tv
        init()
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.RULES
    }

    private fun init() {
        contentTv.text = Html.fromHtml(resources.getString(R.string.rules_of_calculation_text))
        contentTv.setLinkTextColor(Color.BLUE)
        Linkify.addLinks(contentTv, Linkify.WEB_URLS)
    }

}