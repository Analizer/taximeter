package com.taximeter.fragments.staticpage

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import kotlinx.android.synthetic.main.fragment_static_page.view.*

class WhatToDoFragment : Fragment(), Page {

    private lateinit var contentTv: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_static_page, container, false)
        contentTv = pageLayout.static_page_content_tv
        contentTv.text = Html.fromHtml(resources.getString(R.string.what_to_do_text))
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.WHAT_TO_DO
    }
}
