package com.taximeter.fragments.di;

import com.taximeter.fragments.plan.addresspickermap.AddressPickerMapFragment;
import com.taximeter.fragments.plan.addresspickermap.AddressPickerMapFragmentModule;
import com.taximeter.fragments.plan.autosuggest.AutoSuggestFragment;
import com.taximeter.fragments.plan.autosuggest.di.AutoSuggestFragmentModule;
import com.taximeter.fragments.plan.favorites.FavoritesFragment;
import com.taximeter.fragments.plan.favorites.FavoritesModule;
import com.taximeter.fragments.plan.main.PlanFragment;
import com.taximeter.fragments.plan.main.di.PlanFragmentModule;
import com.taximeter.fragments.plan.recents.RecentsFragment;
import com.taximeter.fragments.plan.recents.RecentsModule;
import com.taximeter.fragments.trip.tripslist.TripListFragment;
import com.taximeter.fragments.trip.tripslist.TripListModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.android.scopes.FragmentScoped;
import dagger.hilt.migration.DisableInstallInCheck;

@InstallIn(ActivityComponent.class)
@DisableInstallInCheck
@Module
public abstract class FragmentBinderModule {

    @ContributesAndroidInjector(modules = TripListModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract TripListFragment provideTripListFragment();

    @ContributesAndroidInjector(modules = PlanFragmentModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract PlanFragment providePlanFragment();

    @ContributesAndroidInjector(modules = AutoSuggestFragmentModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract AutoSuggestFragment provideAutoSuggestFragment();

    @ContributesAndroidInjector(modules = RecentsModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract RecentsFragment provideRecentsFragment();

    @ContributesAndroidInjector(modules = FavoritesModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract FavoritesFragment provideFavoritesFragment();

    @ContributesAndroidInjector(modules = AddressPickerMapFragmentModule.class)
    @DisableInstallInCheck
    @FragmentScoped
    abstract AddressPickerMapFragment provideAddressPickerMapFragment();
}
