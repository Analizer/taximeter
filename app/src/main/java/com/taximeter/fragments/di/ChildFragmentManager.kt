package com.taximeter.fragments.di

import javax.inject.Qualifier

@Qualifier
annotation class ChildFragmentManager
