package com.taximeter.fragments.di

import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
object FragmentsModule {

    @Provides
    @ChildFragmentManager
    fun fragmentManager(fragment: Fragment): FragmentManager {
        return fragment.childFragmentManager
    }

    @Provides
    fun layoutInflater(fragment: Fragment): LayoutInflater {
        return fragment.layoutInflater
    }

    @Provides
    fun rootView(fragment: Fragment): View {
        return fragment.requireActivity().window.decorView.findViewById(android.R.id.content)
    }
}