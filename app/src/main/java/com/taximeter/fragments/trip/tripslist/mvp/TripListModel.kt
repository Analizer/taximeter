package com.taximeter.fragments.trip.tripslist.mvp

import com.taximeter.fragments.trip.Trip
import dagger.hilt.android.scopes.FragmentScoped
import java.util.Collections.sort
import javax.inject.Inject

@FragmentScoped
class TripListModel @Inject constructor() : TripListContract.Model {

    private lateinit var mElements: MutableList<Trip>

    override fun init(elements: MutableList<Trip>?) {
        mElements = elements ?: mutableListOf()
        orderItems()
    }

    override fun get(position: Int): Trip {
        return mElements[position]
    }

    override fun add(adapterPosition: Int, trip: Trip) {
        mElements.add(adapterPosition, trip)
    }

    override fun remove(position: Int) {
        mElements.removeAt(position)
    }

    override fun size(): Int {
        return mElements.size
    }

    private fun orderItems() {
        sort<Trip>(mElements) { t1, t2 -> t2.departureTime.compareTo(t1.departureTime) }
    }
}