package com.taximeter.fragments.trip.tripdetails.di

import androidx.fragment.app.Fragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripdetails.TripInfoWindowAdapter
import com.taximeter.fragments.trip.tripdetails.map.TripDetailsMap
import com.taximeter.fragments.trip.tripdetails.map.TripDetailsMapImpl
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsContract
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsPresenter
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsView
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class TripDetailsModule {

    @Binds
    internal abstract fun presenter(presenter: TripDetailsPresenter): TripDetailsContract.Presenter

    @Binds
    internal abstract fun view(view: TripDetailsView): TripDetailsContract.View

    @Binds
    internal abstract fun infoWindowAdapter(infoWindowAdapter: TripInfoWindowAdapter): GoogleMap.InfoWindowAdapter

    @Binds
    internal abstract fun map(map: TripDetailsMapImpl): TripDetailsMap

    companion object {

        @Provides
        internal fun tripPoints(fragment: Fragment, gson: Gson): List<LatLng> {
            val trip = fragment.requireArguments().getParcelable(Extra.TAG_TRIP.tag) as Trip?
            return trip!!.getPoints(gson)
        }

        @Provides
        @TripDetailsFromList
        internal fun fromList(fragment: Fragment): Boolean {
            return fragment.requireArguments().getBoolean(Extra.TAG_TRIP_FROM_LIST.tag)
        }
    }
}
