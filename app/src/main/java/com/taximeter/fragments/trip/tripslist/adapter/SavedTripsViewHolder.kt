package com.taximeter.fragments.trip.tripslist.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.row_saved_trip_list.view.*

class SavedTripsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    lateinit var root: View
    lateinit var deleteView: ImageView
    lateinit var dateTv: TextView
    lateinit var calculatedPriceTv: TextView
    lateinit var mapImage: ImageView
    lateinit var sourceAddressTv: TextView
    lateinit var destinationAddressTv: TextView
    lateinit var distanceTv: TextView

    init {
        initViews(v)
    }

    private fun initViews(row: View) {
        root = row
        deleteView = row.delete_trip
        dateTv = row.date
        calculatedPriceTv = row.price
        mapImage = row.map_image
        sourceAddressTv = row.source_address
        destinationAddressTv = row.destination_address
        distanceTv = row.distance
    }
}
