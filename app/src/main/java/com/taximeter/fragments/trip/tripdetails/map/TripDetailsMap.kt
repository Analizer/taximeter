package com.taximeter.fragments.trip.tripdetails.map

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsContract

interface TripDetailsMap : OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    fun init(presenter: TripDetailsContract.Presenter)

    fun showUserLocationOnMap()

    fun zoomToLocation(location: LatLng)

    fun tearDown()
}