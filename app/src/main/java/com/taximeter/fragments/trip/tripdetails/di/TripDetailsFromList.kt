package com.taximeter.fragments.trip.tripdetails.di

import javax.inject.Qualifier

@Qualifier
annotation class TripDetailsFromList
