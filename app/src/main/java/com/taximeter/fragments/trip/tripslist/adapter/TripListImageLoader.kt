package com.taximeter.fragments.trip.tripslist.adapter

import android.graphics.Bitmap
import android.widget.ImageView
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.ImageUtils
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@FragmentScoped
class TripListImageLoader @Inject constructor(private val mReporter: Reporter) {

    private var mImageLoaderDisposable: Disposable? = null

    fun load(imageView: ImageView, imageThumbnailString: String?) {
        val mapImage = ImageUtils.getImageBitmap(imageView.context, imageThumbnailString)
        if (mapImage != null) {
            mImageLoaderDisposable = Observable.just(mapImage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableObserver<Bitmap>() {
                        override fun onNext(@NonNull bitmap: Bitmap) {
                            imageView.setImageBitmap(bitmap)
                        }

                        override fun onError(@NonNull e: Throwable) {
                            mReporter.reportEvent(AnalyticsCategory.SavedTrips, AnalyticsAction.ErrorLoadingImage)
                        }

                        override fun onComplete() {

                        }
                    })
        }
    }

    fun tearDown(){
        mImageLoaderDisposable?.dispose()
    }
}