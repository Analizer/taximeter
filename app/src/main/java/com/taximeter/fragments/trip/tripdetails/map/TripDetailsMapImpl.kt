package com.taximeter.fragments.trip.tripdetails.map

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.taximeter.R
import com.taximeter.fragments.di.ChildFragmentManager
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsContract
import com.taximeter.utils.MapHelper
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class TripDetailsMapImpl @Inject constructor(private val mInfoWindowAdapter: GoogleMap.InfoWindowAdapter,
                                             @ChildFragmentManager private val mFragmentManager: FragmentManager,
                                             private val mTripPoints: List<LatLng>,
                                             private val mMapHelper: MapHelper)
    : TripDetailsMap {

    private var mMap: GoogleMap? = null
    private lateinit var mPresenter: TripDetailsContract.Presenter

    override fun init(presenter: TripDetailsContract.Presenter) {
        mPresenter = presenter
        (mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.setOnMapLoadedCallback(this)
        setupMap()
        mPresenter.onMapReady()
    }

    override fun onMapLoaded() {
        addPolyline()
    }

    @SuppressLint("MissingPermission")
    override fun showUserLocationOnMap() {
        mMap!!.isMyLocationEnabled = true
    }

    override fun zoomToLocation(location: LatLng) {
        mMapHelper.zoomToLocation(location, mMap!!)
    }

    override fun tearDown() {
        val fragment = mFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        if (fragment != null) {
            mFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        }
    }

    private fun addPolyline() {
        mMapHelper.addMarkerToMap(mMap!!, mTripPoints[0])
        mMapHelper.addMarkerToMap(mMap!!, mTripPoints[mTripPoints.size - 1])
        val pathOptions = PolylineOptions().addAll(mTripPoints)
        mMap!!.addPolyline(pathOptions)
        mMapHelper.zoomToPolyline(mTripPoints, mMap!!)
    }

    private fun setupMap() {
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.uiSettings.setAllGesturesEnabled(true)
        mMap!!.setInfoWindowAdapter(mInfoWindowAdapter)
    }
}