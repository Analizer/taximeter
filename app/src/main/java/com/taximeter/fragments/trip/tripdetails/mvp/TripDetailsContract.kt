package com.taximeter.fragments.trip.tripdetails.mvp

import com.taximeter.fragments.trip.Trip

interface TripDetailsContract {

    interface Presenter {

        fun onViewAvailable(view: View, trip: Trip)

        fun zoomToCurrentLocation()

        fun tearDown()

        fun onRequestPermissionsResult()

        fun onMapReady()
    }

    interface View {

        fun bind(pageView: android.view.View)

        fun setDistance(distance: String)

        fun setTime(time: String)

        fun setPrice(price: String)

        fun setSpeed(speed: String)

        fun showCurrentLocationButton()

        fun showProgress()

        fun hideProgress()
    }
}