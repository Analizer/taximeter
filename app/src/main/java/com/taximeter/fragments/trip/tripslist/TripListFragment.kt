package com.taximeter.fragments.trip.tripslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.trip.tripslist.mvp.TripListContract
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TripListFragment : DaggerFragment(), Page {

    @Inject
    internal lateinit var mView: TripListContract.View

    @Inject
    internal lateinit var mPresenter: TripListContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_list, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView)
        return pageLayout
    }

    override fun onResume() {
        super.onResume()
        mPresenter.init()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.tearDown()
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.TRIPS_LIST
    }

}