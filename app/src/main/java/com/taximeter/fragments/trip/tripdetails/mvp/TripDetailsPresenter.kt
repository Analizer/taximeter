package com.taximeter.fragments.trip.tripdetails.mvp

import android.content.res.Resources
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripdetails.di.TripDetailsFromList
import com.taximeter.fragments.trip.tripdetails.map.TripDetailsMap
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.location.LocationSubscriber
import com.taximeter.utils.calculator.GeoCalculator
import com.taximeter.utils.dialog.DialogFactory
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.location.CurrentLocationRetriever
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class TripDetailsPresenter @Inject constructor(@TripDetailsFromList private val mFromList: Boolean,
                                               private val mDialogFactory: DialogFactory,
                                               private val mMap: TripDetailsMap,
                                               private val mReporter: Reporter,
                                               private val mCurrentLocationRetriever: CurrentLocationRetriever,
                                               private val mGeoCalculator: GeoCalculator,
                                               private val mResources: Resources)
    : TripDetailsContract.Presenter {

    internal lateinit var mView: TripDetailsContract.View

    override fun onViewAvailable(view: TripDetailsContract.View, trip: Trip) {
        mView = view
        mMap.init(this)
        setupHeader(trip)
        if (!mFromList) {
            mDialogFactory.showDialogIfNeeded(DialogType.TRIP_DETAILS)
        }
        mCurrentLocationRetriever.addSubscriber(LocationSubscriber.TRIP_DETAILS)
        mCurrentLocationRetriever.checkPermission(onGranted = { mMap.showUserLocationOnMap() })
    }

    override fun zoomToCurrentLocation() {
        mReporter.reportEvent(AnalyticsCategory.TripDetails, AnalyticsAction.CurrentLocationOnMap)
        mCurrentLocationRetriever.requestLocation(onSuccess = { location ->
            mView.hideProgress()
            mMap.zoomToLocation(mGeoCalculator.toLatLng(location))
            mMap.showUserLocationOnMap()
        })
    }

    override fun tearDown() {
        mMap.tearDown()
        mCurrentLocationRetriever.tearDown()
    }

    override fun onRequestPermissionsResult() {
        mCurrentLocationRetriever.onRequestPermissionsResult()
    }

    override fun onMapReady() {
        mView.showCurrentLocationButton()
    }

    private fun setupHeader(trip: Trip) {
        mView.setDistance(trip.getFormattedDistance(mResources))
        mView.setTime(trip.getFormattedTime(mResources))
        mView.setPrice(trip.getFormattedPrice(mResources))
        mView.setSpeed(trip.getFormattedSpeed(mResources))
    }
}