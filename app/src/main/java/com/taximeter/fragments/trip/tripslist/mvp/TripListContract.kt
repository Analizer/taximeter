package com.taximeter.fragments.trip.tripslist.mvp

import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.ListListener

interface TripListContract {

    interface Presenter : ListListener<Trip> {
        fun onViewAvailable(view: View)

        fun init()

        fun tearDown()
    }

    interface View {
        fun bind(pageView: android.view.View)

        fun setup()

        fun showEmptyMessage()

        fun hideEmptyMessage()

        fun scrollToPosition(adapterPosition: Int)
    }

    interface Model {
        fun init(elements: MutableList<Trip>?)

        fun get(position: Int): Trip

        fun add(adapterPosition: Int, trip: Trip)

        fun remove(position: Int)

        fun size(): Int
    }
}