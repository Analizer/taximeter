package com.taximeter.fragments.trip.tripslist.mvp

import com.taximeter.R
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripslist.adapter.TripsListAdapter
import com.taximeter.persistence.database.DatabaseModule
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.utils.SnackBarHandler
import com.taximeter.utils.navigetion.FragmentNavigator
import javax.inject.Inject

class TripListPresenter @Inject constructor(private val mAdapter: TripsListAdapter,
                                            private val mFragmentNavigator: FragmentNavigator,
                                            private val mSnackBarHandler: SnackBarHandler,
                                            private val mModel: TripListContract.Model,
                                            private val mReporter: Reporter,
                                            private val mDatabase: DatabaseModule)
    : TripListContract.Presenter {

    internal lateinit var mView: TripListContract.View

    override fun onViewAvailable(view: TripListContract.View) {
        mView = view
    }

    override fun init() {
        mDatabase.getTrips { trips -> onModelReady(trips) }
    }

    override fun tearDown() {
        mSnackBarHandler.hideSnackBar()
        mAdapter.tearDown()
    }

    override fun onItemClicked(item: Trip) {
        mReporter.reportEvent(AnalyticsCategory.SavedTrips, AnalyticsAction.OpenTripDetails)
        mFragmentNavigator.goToTripDetailsFromTripList(item)
    }

    override fun deleteItem(position: Int, toReport: AnalyticsLabel) {
        mReporter.reportEvent(AnalyticsCategory.SavedTrips, AnalyticsAction.DeleteTrip, toReport)
        val tripToDelete = mModel.get(position)
        removeItemFromModel(position)
        mSnackBarHandler.showSnackBar(
                messageId = (R.string.saved_trip_deleted_snackbar_msg),
                onUndoClick = { undoDelete(position, tripToDelete) },
                onSnackBarDismissed = { mDatabase.deleteTrip(tripToDelete.departureTime) })
    }

    private fun onModelReady(trips: MutableList<Trip>?) {
        mModel.init(trips)
        mAdapter.init(this)
        mView.setup()
        showOrHideEmptyMessage()
    }

    private fun removeItemFromModel(position: Int) {
        mModel.remove(position)
        mAdapter.notifyItemRemoved(position)
        mAdapter.notifyItemRangeChanged(position, mModel.size())
        showOrHideEmptyMessage()
    }

    private fun undoDelete(adapterPosition: Int, trip: Trip) {
        mReporter.reportEvent(AnalyticsCategory.SavedTrips, AnalyticsAction.UndoDeleteTrip)
        mModel.add(adapterPosition, trip)
        mAdapter.notifyItemInserted(adapterPosition)
        mAdapter.notifyItemRangeChanged(adapterPosition, mModel.size())
        mView.scrollToPosition(adapterPosition)
        mView.hideEmptyMessage()
    }

    private fun showOrHideEmptyMessage() {
        if (mAdapter.itemCount == 0) mView.showEmptyMessage() else mView.hideEmptyMessage()
    }
}