package com.taximeter.fragments.trip.tripdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.trip.tripdetails.mvp.TripDetailsContract
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TripDetailsFragment : Fragment(), Page {

    @Inject
    internal lateinit var mView: TripDetailsContract.View

    @Inject
    internal lateinit var mPresenter: TripDetailsContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val pageLayout = inflater.inflate(R.layout.fragment_trip_details, container, false)
        mView.bind(pageLayout)
        mPresenter.onViewAvailable(mView, requireArguments().getParcelable(Extra.TAG_TRIP.tag)!!)
        return pageLayout
    }

    override fun getFragmentType(): FragmentType {
        return FragmentType.TRIP_DETAILS
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.tearDown()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPresenter.onRequestPermissionsResult()
    }
}
