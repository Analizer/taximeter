package com.taximeter.fragments.trip.tripdetails

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import com.taximeter.R
import com.taximeter.fragments.trip.Trip
import com.taximeter.utils.UiUtils.formatDate
import kotlinx.android.synthetic.main.trip_info_window.view.*
import javax.inject.Inject

class TripInfoWindowAdapter @Inject
constructor(private val mLayoutInflater: LayoutInflater,
            private val mTrip: Trip,
            private val mResources: Resources,
            private val mGson: Gson)
    : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        val root = mLayoutInflater.inflate(R.layout.trip_info_window, null)
        val titleTv = root.info_window_title
        val addressTv = root.info_window_address
        val dateTv = root.info_window_date

        titleTv.text = getTitle(marker)
        addressTv.text = getAddress(marker)
        dateTv.text = getTime(marker)

        return root
    }

    private fun getTitle(marker: Marker): String {
        return if (isDeparture(marker)) mResources.getString(R.string.trip_details_departure) else mResources.getString(R.string.trip_details_arrival)
    }

    private fun getAddress(marker: Marker): String {
        return if (isDeparture(marker)) mTrip.sourceAddress!! else mTrip.destinationAddress!!
    }

    private fun getTime(marker: Marker): String {
        return formatDate(if (isDeparture(marker)) mTrip.departureTime else mTrip.arrivalTime)
    }

    private fun isDeparture(marker: Marker): Boolean {
        return marker.position == mTrip.getPoints(mGson)[0]
    }
}
