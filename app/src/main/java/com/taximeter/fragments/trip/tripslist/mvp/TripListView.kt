package com.taximeter.fragments.trip.tripslist.mvp

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.plan.autosuggest.SwipeableTouchHelperCallback
import com.taximeter.fragments.trip.tripslist.adapter.SavedTripsViewHolder
import com.taximeter.fragments.trip.tripslist.adapter.TripsListAdapter
import com.taximeter.utils.ui.RecyclerViewSetup
import kotlinx.android.synthetic.main.fragment_list.view.*
import javax.inject.Inject

class TripListView @Inject constructor(private val mRecyclerViewSetup: RecyclerViewSetup<SavedTripsViewHolder>,
                                       private val mAdapter: TripsListAdapter)
    : TripListContract.View {

    private lateinit var recyclerView: RecyclerView
    private lateinit var noElementsTv: TextView

    override fun bind(pageView: View) {
        recyclerView = pageView.recycler_view
        noElementsTv = pageView.no_elements_tv
    }

    override fun setup() {
        mRecyclerViewSetup.setup(recyclerView, SwipeableTouchHelperCallback(mAdapter))
    }

    override fun scrollToPosition(adapterPosition: Int) {
        recyclerView.scrollToPosition(adapterPosition)
    }

    override fun showEmptyMessage() {
        noElementsTv.visibility = View.VISIBLE
    }

    override fun hideEmptyMessage() {
        noElementsTv.visibility = View.GONE
    }
}