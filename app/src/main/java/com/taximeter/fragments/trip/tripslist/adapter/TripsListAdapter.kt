package com.taximeter.fragments.trip.tripslist.adapter

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taximeter.R
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripslist.mvp.TripListContract
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.utils.CalculationUtils.getFormattedPrice
import com.taximeter.utils.CalculationUtils.getKilometersTextByMeters
import com.taximeter.utils.UiUtils.getFormattedTripTimeRange
import com.taximeter.utils.touchhelper.SwipeToDeleteCallback
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class TripsListAdapter @Inject constructor(private val mResources: Resources,
                                           private val mModel: TripListContract.Model,
                                           private val mImageLoader: TripListImageLoader)
    : RecyclerView.Adapter<SavedTripsViewHolder>(), SwipeToDeleteCallback {

    private lateinit var mListener: ListListener<Trip>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedTripsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_saved_trip_list, parent, false)
        return SavedTripsViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: SavedTripsViewHolder, position: Int) {
        val trip = mModel.get(position)
        viewHolder.root.setOnClickListener { mListener.onItemClicked(mModel.get(viewHolder.adapterPosition)) }
        viewHolder.dateTv.text = getFormattedTripTimeRange(trip.departureTime, trip.arrivalTime)
        viewHolder.calculatedPriceTv.text = getFormattedPrice(trip.roundedPrice, mResources)
        viewHolder.sourceAddressTv.text = trip.sourceAddress
        viewHolder.destinationAddressTv.text = trip.destinationAddress
        viewHolder.distanceTv.text = String.format(getKilometersTextByMeters(trip.distance, mResources))

        mImageLoader.load(viewHolder.mapImage, trip.mapThumbnailString)
        viewHolder.deleteView.setOnClickListener { mListener.deleteItem(viewHolder.adapterPosition, AnalyticsLabel.DeleteIcon) }
    }

    override fun getItemCount(): Int {
        return mModel.size()
    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder) {
        mListener.deleteItem(viewHolder.adapterPosition, AnalyticsLabel.Swipe)
    }

    fun init(listener: ListListener<Trip>) {
        mListener = listener
    }

    fun tearDown() {
        mImageLoader.tearDown()
    }
}