package com.taximeter.fragments.trip.tripdetails.mvp

import android.view.View
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.taximeter.utils.UiUtils.animateInView
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.fragment_trip_details.view.*
import kotlinx.android.synthetic.main.measure_map_header.view.*
import javax.inject.Inject

@FragmentScoped
class TripDetailsView @Inject constructor(private val mPresenter: TripDetailsContract.Presenter,
                                          private val mProgressBarModule: ProgressBarModule)
    : TripDetailsContract.View {

    private lateinit var timeTv: TextView
    private lateinit var distanceTv: TextView
    private lateinit var priceTv: TextView
    private lateinit var speedTv: TextView
    private lateinit var dataHolder: View
    private lateinit var currentLocationBtn: FloatingActionButton

    override fun bind(pageView: View) {
        timeTv = pageView.time_tv
        distanceTv = pageView.distance_tv
        priceTv = pageView.price_tv
        speedTv = pageView.speed_tv
        dataHolder = pageView.root

        currentLocationBtn = pageView.current_location_button
        currentLocationBtn.setOnClickListener { mPresenter.zoomToCurrentLocation() }

        mProgressBarModule.bind(pageView)
    }

    override fun setDistance(distance: String) {
        distanceTv.text = distance
    }

    override fun setTime(time: String) {
        timeTv.text = time
    }

    override fun setPrice(price: String) {
        priceTv.text = price
    }

    override fun setSpeed(speed: String) {
        speedTv.text = speed
    }

    override fun showCurrentLocationButton() {
        animateInView(currentLocationBtn)
    }

    override fun showProgress() {
        mProgressBarModule.showProgress()
    }

    override fun hideProgress() {
        mProgressBarModule.hideProgress()
    }
}