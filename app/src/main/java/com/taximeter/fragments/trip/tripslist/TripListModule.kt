package com.taximeter.fragments.trip.tripslist

import androidx.recyclerview.widget.RecyclerView
import com.taximeter.fragments.ListListener
import com.taximeter.fragments.trip.Trip
import com.taximeter.fragments.trip.tripslist.adapter.SavedTripsViewHolder
import com.taximeter.fragments.trip.tripslist.adapter.TripsListAdapter
import com.taximeter.fragments.trip.tripslist.mvp.TripListContract
import com.taximeter.fragments.trip.tripslist.mvp.TripListModel
import com.taximeter.fragments.trip.tripslist.mvp.TripListPresenter
import com.taximeter.fragments.trip.tripslist.mvp.TripListView
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class TripListModule {

    @Binds
    abstract fun presenter(presenter: TripListPresenter): TripListContract.Presenter

    @Binds
    abstract fun view(view: TripListView): TripListContract.View

    @Binds
    abstract fun model(model: TripListModel): TripListContract.Model

    @Binds
    abstract fun listListener(presenter: TripListContract.Presenter): ListListener<Trip>

    @Binds
    abstract fun adapter(adapter: TripsListAdapter): RecyclerView.Adapter<SavedTripsViewHolder>
}
