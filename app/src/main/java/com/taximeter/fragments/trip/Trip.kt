package com.taximeter.fragments.trip

import android.content.res.Resources
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.taximeter.utils.CalculationUtils.*
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class Trip constructor(var departureTime: Long = 0/*time of starting the trip, this is the key of the trip in shared preferences*/,
                            var distance: Int = 0/*in meters*/,
                            var arrivalTime: Long = 0,
                            var price: Int = 0,
                            var sourceAddress: String? = null,
                            var destinationAddress: String? = null,
                            var mapThumbnailString: String? = null /* when in a listView, only the smaller thumbnails should be shown in order to avoid outOfMemoryError*/,
                            var averageSpeed: Float = 0f,
                            var pointsString: String = "")
    : Parcelable {

    @Inject
    constructor() : this(0) //This has to be here since in the primary constructor all the fields have default values and Kotlin generates an empty one for it and in the background there are gonna be 2 constructors generated

    @IgnoredOnParcel
    private var points: MutableList<LatLng> = mutableListOf()

    val roundedPrice: Int
        get() = getRoundedPrice(price)

    fun getPoints(gson: Gson): MutableList<LatLng> {
        if (points.isEmpty()) {
            val listType = object : TypeToken<MutableList<LatLng>>() {}.type
            points = gson.fromJson(pointsString, listType)
        }
        return points
    }

    fun updatePoints(newPoints: MutableList<LatLng>) {
        points = newPoints
    }

    fun getFormattedDistance(resources: Resources): String {
        return getKilometersTextByMeters(distance, resources)
    }

    fun getFormattedTime(resources: Resources): String {
        return getFormattedTimeByMillis(arrivalTime - departureTime, resources, false)
    }

    fun getFormattedPrice(resources: Resources): String {
        return getFormattedPrice(roundedPrice, resources)
    }

    fun getFormattedSpeed(resources: Resources): String {
        return getFormattedSpeed(Math.round(averageSpeed * 3.6).toInt(), resources)
    }
}