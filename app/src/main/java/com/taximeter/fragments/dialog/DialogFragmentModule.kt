package com.taximeter.fragments.dialog

import androidx.fragment.app.Fragment
import com.taximeter.fragments.dialog.mvp.TaxiDialogContract
import com.taximeter.fragments.dialog.mvp.TaxiDialogPresenter
import com.taximeter.fragments.dialog.mvp.TaxiDialogView
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.extras.Extra
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
abstract class DialogFragmentModule {

    @Binds
    internal abstract fun presenter(presenter: TaxiDialogPresenter): TaxiDialogContract.Presenter

    @Binds
    internal abstract fun view(presenter: TaxiDialogView): TaxiDialogContract.View

    companion object {

        @Provides
        fun dialogType(fragment: Fragment): DialogType {
            return fragment.requireArguments().getSerializable(Extra.TAG_DIALOG_TYPE.tag) as DialogType
        }

        @Provides
        fun dialogPositiveListener(fragment: Fragment): TaxiDialogFragment.PositiveButtonListener? {
            return fragment.requireArguments().getSerializable(Extra.TAG_DIALOG_POSITIVE_LISTENER.tag) as TaxiDialogFragment.PositiveButtonListener?
        }

        @Provides
        fun dialogNegativeListener(fragment: Fragment): TaxiDialogFragment.NegativeButtonListener? {
            return fragment.requireArguments().getSerializable(Extra.TAG_DIALOG_NEGATIVE_LISTENER.tag) as TaxiDialogFragment.NegativeButtonListener?
        }
    }
}