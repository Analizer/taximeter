package com.taximeter.fragments.dialog.mvp

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import kotlinx.android.synthetic.main.dialog_fragment.view.*
import javax.inject.Inject

class TaxiDialogView @Inject constructor() : TaxiDialogContract.View {

    private lateinit var contentTv: TextView
    private lateinit var checkBox: CheckBox

    override fun bind(pageView: View) {
        contentTv = pageView.dialog_text
        checkBox = pageView.dialog_cb
    }

    override fun hideCheckBox() {
        checkBox.visibility = View.GONE
    }

    override fun isCheckboxChecked(): Boolean {
        return checkBox.isChecked
    }

    override fun setText(textResId: Int) {
        contentTv.setText(textResId)
    }
}