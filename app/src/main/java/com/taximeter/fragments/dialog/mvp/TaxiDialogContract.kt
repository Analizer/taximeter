package com.taximeter.fragments.dialog.mvp

interface TaxiDialogContract {

    interface Presenter {

        fun onViewAvailable(view: View)

        fun onPositiveClicked()

        fun onNegativeClicked()

        fun hasNegativeOption(): Boolean

        fun getPositiveButtonTextId(): Int
    }

    interface View {

        fun bind(pageView: android.view.View)

        fun isCheckboxChecked(): Boolean

        fun setText(textResId: Int)

        fun hideCheckBox()
    }
}