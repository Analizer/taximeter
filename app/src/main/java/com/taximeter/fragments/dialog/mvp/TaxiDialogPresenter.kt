package com.taximeter.fragments.dialog.mvp

import com.taximeter.R
import com.taximeter.fragments.dialog.TaxiDialogFragment
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.dialog.DialogType
import javax.inject.Inject

class TaxiDialogPresenter @Inject constructor(private val mSharedPref: SharedPref,
                                              private val mReporter: Reporter,
                                              private val mType: DialogType,
                                              private val mPositiveListener: TaxiDialogFragment.PositiveButtonListener?,
                                              private val mNegativeListener: TaxiDialogFragment.NegativeButtonListener?)
    : TaxiDialogContract.Presenter {

    internal lateinit var mView: TaxiDialogContract.View

    override fun onViewAvailable(view: TaxiDialogContract.View) {
        mView = view
        mView.setText(mType.textResId)
        if (!hasCheckBox()) mView.hideCheckBox()
    }

    override fun onPositiveClicked() {
        savePrefIfNeeded()
        mPositiveListener?.onPositiveButtonClicked()
    }

    override fun onNegativeClicked() {
        savePrefIfNeeded()
        mNegativeListener?.onNegativeButtonClicked()
    }

    override fun hasNegativeOption(): Boolean {
        return mNegativeListener != null
    }

    override fun getPositiveButtonTextId(): Int {
        return if (hasNegativeOption()) R.string.general_yes else R.string.general_ok
    }

    private fun hasCheckBox(): Boolean {
        return mType.preference != null
    }

    private fun savePrefIfNeeded() {
        if (hasCheckBox() && mView.isCheckboxChecked()) {
            mReporter.reportEvent(AnalyticsCategory.Dialog, mType.analyticsAction)
            mSharedPref.saveBoolean(mType.preference!!, false)
        }
    }
}