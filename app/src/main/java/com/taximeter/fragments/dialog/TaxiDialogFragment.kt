package com.taximeter.fragments.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.taximeter.R
import com.taximeter.fragments.dialog.mvp.TaxiDialogContract
import com.taximeter.utils.dialog.DialogType
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.AndroidEntryPoint
import java.io.Serializable
import javax.inject.Inject

@AndroidEntryPoint
open class TaxiDialogFragment : DialogFragment() {

    @Inject
    lateinit var mPresenter: TaxiDialogContract.Presenter

    @Inject
    lateinit var mView: TaxiDialogContract.View

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogView = requireActivity().layoutInflater.inflate(R.layout.dialog_fragment, null)
        mView.bind(dialogView)
        mPresenter.onViewAvailable(mView)

        val builder = AlertDialog.Builder(requireActivity())
                .setCancelable(true)
                .setOnCancelListener { dismiss() }
                .setView(dialogView)
                .setPositiveButton(requireActivity().getString(mPresenter.getPositiveButtonTextId()))
                { _, _ ->
                    mPresenter.onPositiveClicked()
                    dismiss()
                }
        if (mPresenter.hasNegativeOption()) {
            builder.setNegativeButton(requireActivity().getString(R.string.general_no)
            ) { _, _ ->
                mPresenter.onNegativeClicked()
                dismiss()
            }
        }
        return builder.create()
    }

    companion object {
        fun create(type: DialogType, onPositive: (() -> Unit)? = null, onNegative: (() -> Unit)? = null): TaxiDialogFragment {
            val dialog = TaxiDialogFragment()
            val bundle = Bundle()
            bundle.putSerializable(Extra.TAG_DIALOG_TYPE.tag, type)
            if (onPositive != null) addPositiveListener(bundle, onPositive)
            if (onNegative != null) addNegativeListener(bundle, onNegative)
            dialog.arguments = bundle
            return dialog
        }

        private fun addPositiveListener(bundle: Bundle, onPositive: (() -> Unit)) {
            bundle.putSerializable(Extra.TAG_DIALOG_POSITIVE_LISTENER.tag, object : PositiveButtonListener {
                override fun onPositiveButtonClicked() {
                    onPositive.invoke()
                }
            })
        }

        private fun addNegativeListener(bundle: Bundle, onNegative: (() -> Unit)) {
            bundle.putSerializable(Extra.TAG_DIALOG_NEGATIVE_LISTENER.tag, object : NegativeButtonListener {
                override fun onNegativeButtonClicked() {
                    onNegative.invoke()
                }
            })
        }
    }

    interface PositiveButtonListener : Serializable {
        fun onPositiveButtonClicked()
    }

    interface NegativeButtonListener : Serializable {
        fun onNegativeButtonClicked()
    }
}