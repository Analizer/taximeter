package com.taximeter.application.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import com.google.android.gms.location.LocationRequest
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.picasso.OkHttp3Downloader
import com.patloew.rxlocation.RxLocation
import com.squareup.picasso.Picasso
import com.taximeter.BuildConfig
import com.taximeter.R
import com.taximeter.application.TaxiApplication
import com.taximeter.persistence.database.TaxiDatabase
import com.taximeter.utils.Logger
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class AppModule {

    @Binds
    internal abstract fun context(@ApplicationContext application: Context): Context

    companion object {

        @Provides
        @Singleton
        internal fun resources(@ApplicationContext context: Context): Resources {
            return context.resources
        }

        @Provides
        @Singleton
        internal fun retrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit.Builder {
            return Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        }

        @Provides
        @Singleton
        internal fun gson(): Gson {
            return GsonBuilder().create()
        }

        @Provides
        @Singleton
        internal fun picasso(context: Context, okHttpDownloader: OkHttp3Downloader): Picasso {
            return Picasso.Builder(context).downloader(okHttpDownloader).build()
        }

        @Provides
        @Singleton
        internal fun okHttpDownloader(okHttpClient: OkHttpClient): OkHttp3Downloader {
            return OkHttp3Downloader(okHttpClient)
        }

        @Provides
        @Singleton
        internal fun tracker(applicationContext: Context): Tracker {
            val tracker = GoogleAnalytics.getInstance(applicationContext).newTracker(R.xml.app_tracker)
            tracker.setAppVersion(BuildConfig.VERSION_NAME)
            return tracker
        }

        @Provides
        @Singleton
        internal fun okHttpClient(loggingInterceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {
            return OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .cache(cache)
                    .build()
        }

        @Provides
        @Singleton
        internal fun httpLoggingInterceptor(): HttpLoggingInterceptor {
            val interceptor = HttpLoggingInterceptor { message -> Logger.log("http logger: $message") }
            interceptor.level = HttpLoggingInterceptor.Level.BASIC
            return interceptor
        }

        @Provides
        @Singleton
        internal fun cache(cacheFile: File): Cache {
            return Cache(cacheFile, (10 * 1000 * 1000).toLong())
        }

        @Provides
        @Singleton
        internal fun file(context: Context): File {
            return File(context.cacheDir, "taxi_cache")
        }

        @Provides
        @Singleton
        internal fun rxLocation(context: Context): RxLocation {
            return RxLocation(context)
        }

        @Provides
        @Singleton
        internal fun locationRequest(): LocationRequest {
            return LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(2000)
        }

        @Provides
        @Singleton
        internal fun database(context: Context): TaxiDatabase {
            return TaxiDatabase.get(context)
        }
    }
}
