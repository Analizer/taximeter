package com.taximeter.application


import android.app.Application
import androidx.multidex.MultiDex
import com.taximeter.persistence.sharedpref.SharedPref
import com.taximeter.utils.LocaleManager
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class TaxiApplication : Application() {

    @Inject
    lateinit var mSharedPref: SharedPref

    @Inject
    lateinit var mLocaleManager: LocaleManager

    override fun onCreate() {
        MultiDex.install(this)//TODO!
        super.onCreate()
        mSharedPref.clearSharedPrefsIfNeeded()
        mLocaleManager.updateContext()
    }
}