package com.taximeter.communication

import android.content.res.Resources
import com.taximeter.R
import com.taximeter.communication.endpoint.DirectionsEndpoint
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DirectionsCommunicator @Inject constructor(private val mResources: Resources) {


    fun getDirections(endpoint: DirectionsEndpoint,
                      stops: List<Stop>,
                      onSuccess: (directionModel: DirectionModel) -> Unit,
                      onError: () -> Unit)
            : Disposable {
        val apiKey = mResources.getString(R.string.key_places)//TODO! gradle.properties-be
        val startLocation = getDirectionsCoordinates(stops, 0)
        val endLocation = getDirectionsCoordinates(stops, stops.size - 1)
        return if (stops.size > 2) {
            val wayPoints = StringBuilder(TAG_OPTIMIZE)
            for (i in 1 until stops.size - 1) {
                if (i != 1) {
                    wayPoints.append(URL_DELIMITER)
                }
                wayPoints.append(getDirectionsCoordinates(stops, i))
            }
            endpoint.getDirectionsSeveralStops(startLocation, endLocation, apiKey, wayPoints.toString())//TODO! not needed
        } else {
            endpoint.getDirectionsTwoStops(startLocation, endLocation, apiKey)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<DirectionModel>() {
                    override fun onNext(@NonNull directionModel: DirectionModel) {
                        onSuccess.invoke(directionModel)
                    }

                    override fun onError(@NonNull e: Throwable) {
                        onError.invoke()
                    }

                    override fun onComplete() {}
                })
    }

    private fun getDirectionsCoordinates(stops: List<Stop>, position: Int): String {
        return StringBuilder(stops[position].position.latitude.toString())
                .append(",")
                .append(stops[position].position.longitude.toString()).toString()
    }

    companion object {
        private const val TAG_OPTIMIZE = "optimize:true|"
        private const val URL_DELIMITER = "|"
    }
}