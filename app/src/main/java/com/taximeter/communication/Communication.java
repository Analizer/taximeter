package com.taximeter.communication;

import com.google.android.gms.maps.model.LatLng;
import com.taximeter.communication.endpoint.PostalCodesEndpoint;
import com.taximeter.fragments.plan.dao.postalcode.PostalCodesResultModel;

import io.reactivex.Observable;

public class Communication {

    //TODO cleanup
    public static Observable<PostalCodesResultModel> getPostalCodes(PostalCodesEndpoint endpoint, LatLng location) {
        return endpoint.getPostalCodes(location.latitude, location.longitude, "HU", 10, "Analizer");
    }
}
