package com.taximeter.communication.endpoint;


import com.taximeter.fragments.plan.dao.address.AddressesModel;
import com.taximeter.fragments.plan.dao.place.PlacesModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StopsEndpoint {

    @GET("autocomplete/json")
    Observable<AddressesModel> getAddresses(@Query("input") String query, @Query("key") String apiKey, @Query("components") String components, @Query("language") String language);

    @GET("search/json")
    Observable<PlacesModel> getPlaces(@Query("name") String query, @Query("key") String apiKey, @Query("language") String language, @Query("location") String location, @Query("radius") String radius, @Query("sensor") String sensor);
}
