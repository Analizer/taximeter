package com.taximeter.communication.endpoint;

import com.taximeter.fragments.plan.dao.directions.DirectionModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DirectionsEndpoint {

    @GET("json")
    Observable<DirectionModel> getDirectionsTwoStops(@Query("origin") String startLocation, @Query("destination") String endLocation, @Query("key") String apipKey);

    @GET("json")
    Observable<DirectionModel> getDirectionsSeveralStops(@Query("origin") String startLocation, @Query("destination") String endLocation, @Query("key") String apipKey, @Query("waypoints") String waypoints);
}
