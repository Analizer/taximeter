package com.taximeter.communication.endpoint;

import com.taximeter.fragments.plan.dao.postalcode.PostalCodesResultModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PostalCodesEndpoint {

    @GET("findNearbyPostalCodesJSON")
    Observable<PostalCodesResultModel> getPostalCodes(@Query("lat") double latitude, @Query("lng") double longitude, @Query("country") String country, @Query("radius") int radius, @Query("username") String userName);
}
