package com.taximeter.activity.main

import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.taximeter.R
import com.taximeter.fragments.plan.dao.Stop
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.android.synthetic.main.toolbar_autosuggest_section.view.*
import javax.inject.Inject

@ActivityScoped
class ToolbarModule @Inject constructor() {

    private lateinit var mToolbar: Toolbar
    private lateinit var mToolbarSearchSection: View
    private lateinit var mTextWatcher: TextWatcher
    internal lateinit var mSearchField: EditText

    fun init(activity: AppCompatActivity) {
        mToolbar = activity.findViewById<View>(R.id.material_toolbar) as Toolbar
        activity.setSupportActionBar(mToolbar)
        mToolbarSearchSection = mToolbar.toolbar_autosuggest_section
        mSearchField = mToolbarSearchSection.auto_suggest_destination
    }

    fun setTitle(titleId: Int) {
        mToolbar.setTitle(titleId)
    }

    fun getToolbar(): Toolbar {
        return mToolbar
    }

    fun setupToolbar(activity: AppCompatActivity, fragmentHandler: MainActivityFragmentHandler, drawerController: DrawerModule) {
        mToolbar.setNavigationOnClickListener {
            if (fragmentHandler.getCurrentContent()!!.fragmentType.hasDrawer()) {
                drawerController.openDrawer()
            } else {
                activity.onBackPressed()
            }
        }
    }

    fun showSearchSection(textWatcher: TextWatcher, searchButtonClickedAction: (query: String) -> Unit, clearButtonPressedAction: () -> Unit) {
        mTextWatcher = textWatcher
        mToolbarSearchSection.visibility = View.VISIBLE
        setupSearchField(searchButtonClickedAction)
        setupToolbarDeleteButton(clearButtonPressedAction)
    }

    fun resetSearchSection() {
        mSearchField.removeTextChangedListener(mTextWatcher)
        mToolbarSearchSection.visibility = View.GONE
        mSearchField.setText("")
    }

    fun loadSearchModel(model: Stop) {
        mSearchField.setText(model.getText())
    }

    private fun setupSearchField(searchButtonClickedAction: (query: String) -> Unit) {
        mSearchField.addTextChangedListener(mTextWatcher)
        mSearchField.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchButtonClickedAction.invoke(mSearchField.text.toString())
            }
            true
        }
    }

    private fun setupToolbarDeleteButton(clearButtonPressedAction: () -> Unit) {
        val deleteIv = mToolbarSearchSection.auto_suggest_clear_btn
        deleteIv.setOnClickListener {
            mSearchField.setText("")
            clearButtonPressedAction.invoke()
        }
    }
}