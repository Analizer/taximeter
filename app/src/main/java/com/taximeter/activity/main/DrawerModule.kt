package com.taximeter.activity.main

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.taximeter.fragments.FragmentType
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class DrawerModule @Inject constructor(private val mActivity: AppCompatActivity) {

    internal lateinit var mDrawer: Drawer

    fun init(fragmentHandler: MainActivityFragmentHandler, toolbar: Toolbar) {
        mDrawer = DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withDrawerItems(getDrawerItems())
                .withOnDrawerItemClickListener { view, position, drawerItem ->
                    mDrawer.closeDrawer()
                    fragmentHandler.switchFragment(FragmentType.getTypeFromIdentifier(drawerItem.identifier))
                    true
                }
                .build()
    }

    fun updateDrawer(fragmentType: FragmentType) {
        mDrawer.setSelection(fragmentType.id.toLong(), false)
        if (fragmentType.hasDrawer()) {
            mActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(false) //TODO itt valahogy a toolbart hasznalni?
            mDrawer.actionBarDrawerToggle.isDrawerIndicatorEnabled = true
        } else {
            mDrawer.actionBarDrawerToggle.isDrawerIndicatorEnabled = false
            mActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    fun openDrawer() {
        mDrawer.openDrawer()
    }

    private fun getDrawerItems(): List<IDrawerItem<*, *>> {
        val drawerItems = ArrayList<IDrawerItem<*, *>>()
        drawerItems.add(getDrawerItem(FragmentType.MAIN_PAGE))
        drawerItems.add(getDrawerItem(FragmentType.MEASURE))
        drawerItems.add(getDrawerItem(FragmentType.CALL))
        drawerItems.add(getDrawerItem(FragmentType.PLAN))
        drawerItems.add(getDrawerItem(FragmentType.TRIPS_LIST))
        drawerItems.add(DividerDrawerItem())
        drawerItems.add(getDrawerItem(FragmentType.WHAT_TO_DO))
        drawerItems.add(getDrawerItem(FragmentType.RULES))
        drawerItems.add(DividerDrawerItem())
        drawerItems.add(getDrawerItem(FragmentType.SETTINGS))
        return drawerItems
    }

    private fun getDrawerItem(fragmentType: FragmentType): PrimaryDrawerItem {
        return PrimaryDrawerItem()
                .withIdentifier(fragmentType.id.toLong())
                .withName(fragmentType.titleId)
                .withTypeface(Typeface.create("sans-serif", Typeface.NORMAL))
                .withIcon(fragmentType.iconId)
                .withSelectedIcon(fragmentType.getmIconIdActive())
    }
}