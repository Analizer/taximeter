package com.taximeter.activity.main

import android.app.AlertDialog
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.taximeter.R
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.UiUtils.hideKeyboard
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class MainActivityEventsModule @Inject constructor(private val mActivity: AppCompatActivity,
                                                   private val mReporter: Reporter) {

    fun hideKeyboard() {
        hideKeyboard(mActivity)
    }

    fun showSnackBar(messageResId: Int) {
        Snackbar.make(mActivity.findViewById<View>(R.id.fragment_holder), messageResId, Snackbar.LENGTH_SHORT).show()
    }

    fun showGooglePlayServicesNotAvailableDialog() {
        AlertDialog.Builder(mActivity)
                .setMessage(R.string.error_message_no_google_play_services)
                .setPositiveButton(R.string.general_ok) { _, _ ->
                    mReporter.reportEvent(AnalyticsCategory.General, AnalyticsAction.GooglePlayServicesNotAvailable)
                    mActivity.finish()
                }
                .show()
    }

    fun goBack() {
        mActivity.onBackPressed()
    }

    fun finish() {
        mActivity.finish()
    }
}