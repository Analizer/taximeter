package com.taximeter.activity.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.taximeter.R
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.Page
import com.taximeter.fragments.measure.MeasureFragment
import com.taximeter.fragments.plan.main.PlanFragment
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.service.measure.MeasureServiceStateChecker
import com.taximeter.utils.DeviceUtils.isNetworkAvailable
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class MainActivityFragmentHandler @Inject constructor(
        private val mMeasureServiceStateChecker: MeasureServiceStateChecker,
        private val mFragmentManager: FragmentManager,
        private val mToolbarHandler: ToolbarModule,
        private val mDrawerController: DrawerModule,
        private val mReporter: Reporter,
        private val mContext: Context,
        private val mActivityEventHandler: MainActivityEventsModule,
        private var mIntent: Intent
) {

    fun getCurrentContent(): Page? {
        val tag = mFragmentManager.getBackStackEntryAt(mFragmentManager.backStackEntryCount - 1).name
        return mFragmentManager.findFragmentByTag(tag) as Page?
    }

    fun updateIntent(intent: Intent) {
        mIntent = intent
    }

    fun removeFragmentFromBackStack(toRemove: Fragment) {
        val trans = mFragmentManager.beginTransaction()
        trans.remove(toRemove)
        trans.commit()
        mFragmentManager.popBackStack()
    }

    fun goBack() {
        mActivityEventHandler.goBack()
    }

    private val isComingFromNotification: Boolean
        get() = (Extra.TAG_START_MEASURE_ROUTE_FRAGMENT.tag == mIntent.action
                && mMeasureServiceStateChecker.isMeasuring())

    private val isComingFromAutoSuggest: Boolean
        get() = Extra.ACTION_COMING_FROM_AUTO_SUGGEST.tag == mIntent.action

    internal fun switchToInitialFragment() {
        when {
            isComingFromNotification -> handleComingFromNotification()
            isComingFromAutoSuggest -> handleComingFromAutoSuggest()
            else -> switchFragment(FragmentType.MAIN_PAGE)
        }
    }

    @JvmOverloads
    fun switchFragment(fragmentType: FragmentType, bundle: Bundle? = null) {
        mActivityEventHandler.hideKeyboard()
        if (isNetworkAvailable(mContext) || !fragmentType.isNetworkNeeded) {
            val fragment = fragmentType.fragment
            val tag = fragment!!.javaClass.name
            fragment.arguments = bundle
            mFragmentManager.beginTransaction().replace(R.id.fragment_holder, fragment, tag)
                    .addToBackStack(tag).commit()
        } else {
            //TODO check if this report goes through
            mReporter.reportEvent(AnalyticsCategory.General, AnalyticsAction.InternetNotAvailable)
            mActivityEventHandler.showSnackBar(R.string.error_message_no_internet)
        }
    }

    internal fun handleBackStackChanged() {
        val currentPage = getCurrentContent()
        if (currentPage != null) {
            val fragmentType = currentPage.fragmentType
            mReporter.sendScreenReport(fragmentType.analyticsId)
            if (fragmentType.hasTitle()) {
                mToolbarHandler.setTitle(fragmentType.titleId)
            }
            mDrawerController.updateDrawer(fragmentType)
        }
    }

    internal fun handleBackPressed() {
        val currentPage = getCurrentContent()
        if (currentPage != null) {
            when (currentPage.fragmentType) {
                FragmentType.MAIN_PAGE -> mActivityEventHandler.finish()
                FragmentType.WHAT_TO_DO,
                FragmentType.RULES,
                FragmentType.CALL,
                FragmentType.MEASURE,
                FragmentType.SETTINGS,
                FragmentType.TRIPS_LIST -> {
                    mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)//TODO why?
                    switchFragment(FragmentType.MAIN_PAGE)
                }
                else -> mFragmentManager.popBackStack()
            }
            mDrawerController.updateDrawer(currentPage.fragmentType)
        }
    }

    private fun handleComingFromNotification() {
        mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.OpenMeasurementFromNotification)
        val backStackCount = mFragmentManager.backStackEntryCount
        val topFragmentTag = if (backStackCount != 0) mFragmentManager.getBackStackEntryAt(backStackCount - 1).name else ""
        if (MeasureFragment::class.java.name != topFragmentTag) {
            if (backStackCount == 0) {
                switchFragment(FragmentType.MAIN_PAGE)
            }
            switchFragment(FragmentType.MEASURE)
        }
    }

    private fun handleComingFromAutoSuggest() {
        val backStackCount = mFragmentManager.backStackEntryCount
        val topFragmentTag = if (backStackCount != 0) mFragmentManager.getBackStackEntryAt(backStackCount - 1).name else ""
        if (PlanFragment::class.java.name != topFragmentTag) {
            if (backStackCount == 0) {
                switchFragment(FragmentType.MAIN_PAGE)
            }
            switchFragment(FragmentType.PLAN, mIntent.extras)
        }
    }

    fun goBackToPlan() {
        mFragmentManager.popBackStack(PlanFragment::class.java.name, 0)
    }
}