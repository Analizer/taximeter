package com.taximeter.activity.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.taximeter.BuildConfig
import com.taximeter.R
import com.taximeter.utils.LocaleManager
import com.taximeter.utils.UiUtils.hideKeyboard
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mDrawerController: DrawerModule

    @Inject
    lateinit var mFragmentHandler: MainActivityFragmentHandler

    @Inject
    lateinit var mToolbarHandler: ToolbarModule

    @Inject
    lateinit var mEventsHandler: MainActivityEventsModule

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleManager(base).updateContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!BuildConfig.DEBUG) {
//            Mint.initAndStartSession(applicationContext, getString(R.string.key_splunk_mint))
            //megszunt a splunk :(
        }
        setContentView(R.layout.activity_main)
        if (isGooglePlayServicesAvailable()) {
            init()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Integer.valueOf(Extra.AUTO_SUGGEST_REQUEST_CODE.tag) &&
                resultCode == Activity.RESULT_OK) {
            data!!.action = Extra.ACTION_COMING_FROM_AUTO_SUGGEST.tag
            intent = data
            mFragmentHandler.updateIntent(intent)
            mFragmentHandler.switchToInitialFragment()
        }
    }

    override fun onNewIntent(newIntent: Intent) {
        super.onNewIntent(intent)
        intent = newIntent
        mFragmentHandler.updateIntent(intent)
        mFragmentHandler.switchToInitialFragment()
    }

    override fun onBackPressed() {
        hideKeyboard(this)
        if (mDrawerController.mDrawer.isDrawerOpen) {
            mDrawerController.mDrawer.closeDrawer()
        } else {
            mFragmentHandler.handleBackPressed()
        }
    }

    fun getFragmentHandler(): MainActivityFragmentHandler {
        return mFragmentHandler
    }

    private fun init() {
        mToolbarHandler.init(this)
        mDrawerController.init(mFragmentHandler, mToolbarHandler.getToolbar())
        mToolbarHandler.setupToolbar(this, mFragmentHandler, mDrawerController)
        mFragmentHandler.switchToInitialFragment()
        supportFragmentManager.addOnBackStackChangedListener { mFragmentHandler.handleBackStackChanged() }
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        return if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            true
        } else {
            mEventsHandler.showGooglePlayServicesNotAvailableDialog()
            false
        }
    }
}
