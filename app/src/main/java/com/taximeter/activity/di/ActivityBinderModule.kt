package com.taximeter.activity.di

import com.taximeter.activity.main.MainActivity
import com.taximeter.fragments.di.FragmentBinderModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.hilt.InstallIn
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class ActivityBinderModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainActivityModule::class, FragmentBinderModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}
