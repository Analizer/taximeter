package com.taximeter.activity.di

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext

@InstallIn(ActivityComponent::class)
@Module
abstract class MainActivityModule {

    companion object {
        @Provides
        fun provideActivity(@ActivityContext activity: Context): AppCompatActivity = activity as AppCompatActivity

        @Provides
        fun provideFragmentManager(activity: AppCompatActivity): FragmentManager = activity.supportFragmentManager

        @Provides
        fun provideIntent(activity: AppCompatActivity): Intent = activity.intent
    }
}