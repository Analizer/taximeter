package com.taximeter.service.location;

import android.location.Location;

public interface LocationConsumer {

    void onLocation(Location location);

    void onLocationError(Throwable e);
}
