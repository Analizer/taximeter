package com.taximeter.service.location.di;

import javax.inject.Qualifier;

@Qualifier
public @interface LocationObservable {
}
