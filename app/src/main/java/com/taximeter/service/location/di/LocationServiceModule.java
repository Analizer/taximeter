package com.taximeter.service.location.di;

import android.annotation.SuppressLint;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ServiceComponent;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InstallIn(ServiceComponent.class)
@Module
public class LocationServiceModule {

    private static final long LOCATION_SERVICE_EXPIRATION_TIME = 10;

    @SuppressLint("MissingPermission")
    @Provides
    @LocationObservable
    // This is only called from the @CurrentLocationRetriever and @MeasureServiceLocationHandlerModule which checks the permission in advance
    static Observable locationObservable(RxLocation rxLocation, LocationRequest locationRequest) {
        return rxLocation.location()//TODO!
                .updates(locationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Provides
    @TimerObservable
    static Observable timerObservable() {
        return Observable.interval(LOCATION_SERVICE_EXPIRATION_TIME, LOCATION_SERVICE_EXPIRATION_TIME, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
