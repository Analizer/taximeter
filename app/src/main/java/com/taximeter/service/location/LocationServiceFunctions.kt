package com.taximeter.service.location


import android.location.Location

interface LocationServiceFunctions {

    val location: Location

    fun subscribe(consumer: LocationConsumer, subscriber: LocationSubscriber)

    fun unSubscribe(subscriber: LocationSubscriber)

    fun hasLocation(): Boolean

    fun isSubscribed(subscriber: LocationSubscriber): Boolean
}
