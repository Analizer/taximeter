package com.taximeter.service.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;
import com.taximeter.service.location.di.LocationObservable;
import com.taximeter.service.location.di.TimerObservable;

import java.util.HashMap;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

@AndroidEntryPoint
public class LocationService extends Service implements Observer<Location> {

    @Inject
    @LocationObservable
    Observable mLocationObservable;
    @Inject
    @TimerObservable
    Observable mTimerObservable;
    @Inject
    RxLocation mRxLocation;
    @Inject
    LocationRequest mLocationRequest;
    @Inject
    Tracker mTracker;

    private long mTimeStamp;
    private final Binder mBinder = new LocationBinder();
    private Location mLocation;
    private Disposable mTimerDisposable;
    private Disposable mLocationDisposable;
    private HashMap<LocationConsumer, LocationSubscriber> mClients = new HashMap<>();

    private static final long LOCATION_VALIDITY_TIME = 10000;


    public static void connect(Context context, ServiceConnection serviceConnection) {
        Intent serviceIntent = new Intent(context, LocationService.class);
        context.startService(serviceIntent); //if not, this gets destroyed after unbind
        context.bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        disposeTimer();
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        disposeTimer();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (mClients.isEmpty()) {
            startTimer();
        }
        return true;
    }

    @Override
    public void onDestroy() {
        mClients.clear();
        disposeLocation();
        disposeTimer();
        super.onDestroy();
    }

    @Override
    public void onSubscribe(Disposable disposable) {
        mLocationDisposable = disposable;
    }

    @Override
    public void onNext(Location location) {
        mLocation = location;
        mTimeStamp = System.currentTimeMillis();
        for (LocationConsumer client : mClients.keySet()) {
            client.onLocation(location);
        }
    }

    @Override
    public void onError(Throwable e) {
        for (LocationConsumer client : mClients.keySet()) {
            client.onLocationError(e);
        }
    }

    @Override
    public void onComplete() {
        //NO-OP
    }

    private void startTimer() {
        disposeTimer();
        mTimerDisposable = mTimerObservable.subscribe(
                seconds -> stopSelf());
    }

    private void disposeTimer() {
        if (mTimerDisposable != null) {
            mTimerDisposable.dispose();
        }
    }

    private void disposeLocation() {
        if (mLocationDisposable != null) {
            mLocationDisposable.dispose();
        }
    }

    public class LocationBinder extends Binder implements LocationServiceFunctions {

        @Override
        public void subscribe(LocationConsumer locationConsumer, LocationSubscriber subscriber) {
            if (!isSubscribed(subscriber)) {
                mClients.put(locationConsumer, subscriber);
                disposeTimer();
                mLocationObservable.subscribe(LocationService.this);
            }
        }

        @Override
        public void unSubscribe(LocationSubscriber subscriber) {
            for (HashMap.Entry<LocationConsumer, LocationSubscriber> entry : mClients.entrySet()) {
                if (subscriber == entry.getValue()) {
                    mClients.remove(entry.getKey());
                    break;
                }
            }
        }

        @Override
        public Location getLocation() {
            return mLocation;
        }

        @Override
        public boolean hasLocation() {
            return mLocation != null && System.currentTimeMillis() - mTimeStamp > LOCATION_VALIDITY_TIME;
        }

        @Override
        public boolean isSubscribed(LocationSubscriber subscriber) {
            return mClients.containsValue(subscriber);
        }

    }

}
