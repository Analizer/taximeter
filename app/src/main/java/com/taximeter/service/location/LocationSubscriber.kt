package com.taximeter.service.location

enum class LocationSubscriber {

    PLAN,
    MEASURE,
    ADDRESS_PICKER_MAP,
    AUTO_SUGGEST,
    TRIP_DETAILS
}
