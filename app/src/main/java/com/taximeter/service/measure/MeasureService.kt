package com.taximeter.service.measure

import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Binder
import android.os.IBinder
import com.taximeter.fragments.measure.mvp.MeasureContract
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@AndroidEntryPoint
class MeasureService : Service(), MeasureServiceTerminateCallback {

    @Inject
    lateinit var mModel: MeasureContract.Model

    @Inject
    lateinit var mTimerObservable: Observable<Long>

    @Inject
    lateinit var mNotification: TripNotification

    @Inject
    lateinit var mTerminateReceiver: MeasureTerminateReceiver

    @Inject
    lateinit var mLocationHandler: MeasureServiceLocationHandlerModule

    var mClient: MeasureFunctions? = null
    private var mTimerDisposable: Disposable? = null
    private var mPreviousLocation: Location? = null
    private val mBinder = MeasureBinder()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Extra.ACTION_STOP_FOREGROUND.tag == intent?.action) {
            terminateProcess()
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    private fun onTime(seconds: Long) {
        mNotification.update(mModel.trip.roundedPrice, mModel.trip.distance, seconds)
        mClient?.onTime(seconds)
    }

    override fun terminateCalled() {
        terminateProcess()
        mClient?.onTerminateProgress()
    }

    private fun handleLocation(location: Location) {
        if (mPreviousLocation == null) {
            startTrip(location)
        } else {
            updateTrip(location)
        }
        mPreviousLocation = location
    }

    private fun startTrip(location: Location) {
        mTerminateReceiver.init(this)
        mModel.start(location)
        mNotification.start(this)
        mTimerDisposable = mTimerObservable.subscribe { onTime(it) }
        mClient?.onTripUpdated(mModel)
    }

    private fun updateTrip(location: Location) {
        mModel.update(mPreviousLocation!!, location)
        mClient?.onTripUpdated(mModel)
        mPreviousLocation = location
    }

    private fun terminateProcess() {
        mTerminateReceiver.tearDown()
        mLocationHandler.tearDown()
        mTimerDisposable?.dispose()
        stopForeground(true)
        stopSelf()
    }

    inner class MeasureBinder : Binder(), MeasureServiceBinder {

        override fun subscribe(client: MeasureFunctions?) {
            mClient = client
            mLocationHandler.requestLocations { location -> handleLocation(location) }
        }

        override fun unSubscribe() {
            mClient = null
        }
    }
}
