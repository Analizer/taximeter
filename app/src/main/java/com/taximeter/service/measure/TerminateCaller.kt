package com.taximeter.service.measure

import com.taximeter.reporting.AnalyticsAction

enum class TerminateCaller constructor(val analyticsAction: AnalyticsAction) {

    MEASURE_CANCEL_BUTTON(AnalyticsAction.TerminateMeasurementFromMeasureCancelButton),
    NOTIFICATION(AnalyticsAction.TerminateMeasurementFromNotification),
    LOCATION_DISABLED(AnalyticsAction.LocationDisabled)
}
