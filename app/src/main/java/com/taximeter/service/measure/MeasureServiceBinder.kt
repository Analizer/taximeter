package com.taximeter.service.measure

interface MeasureServiceBinder {

    fun subscribe(client: MeasureFunctions?)

    fun unSubscribe()
}
