package com.taximeter.service.measure

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.AnalyticsLabel
import com.taximeter.reporting.Reporter
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.ServiceScoped
import javax.inject.Inject

@ServiceScoped
class MeasureTerminateReceiver @Inject constructor(private val mContext: Context,
                                                   private val mReporter: Reporter)
    : BroadcastReceiver() {

    private lateinit var mCallback: MeasureServiceTerminateCallback

    override fun onReceive(context: Context, intent: Intent) {
        if (Extra.ACTION_TERMINATE.tag == intent.action) {
            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.cancel(0)
            sendAnalytics(intent)
            mCallback.terminateCalled()
        }
    }

    fun init(callback: MeasureServiceTerminateCallback) {
        mCallback = callback
        setupCloseAppReceiver()
    }

    fun tearDown() {
        try {
            mContext.unregisterReceiver(this)
        } catch (e: IllegalArgumentException) {
            mReporter.reportEvent(AnalyticsCategory.Measurement, AnalyticsAction.Error, AnalyticsLabel.ErrorInMeasureTerminateBroadcastModule)
            //TODO ugly hack :(
        }
    }

    private fun setupCloseAppReceiver() {
        val terminateAndFinishNeededFilter = IntentFilter(Extra.ACTION_TERMINATE.tag)
        mContext.registerReceiver(this, terminateAndFinishNeededFilter)
    }

    private fun sendAnalytics(intent: Intent) {
        val action = (intent.getSerializableExtra(Extra.TAG_TERMINATE_CALLER.tag) as TerminateCaller).analyticsAction
        mReporter.reportEvent(AnalyticsCategory.Measurement, action)
    }
}