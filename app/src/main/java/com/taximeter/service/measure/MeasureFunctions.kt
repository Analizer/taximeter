package com.taximeter.service.measure

import com.taximeter.fragments.measure.mvp.MeasureContract

interface MeasureFunctions {

    fun onTime(seconds: Long)

    fun onTripUpdated(model: MeasureContract.Model)

    fun onTerminateProgress()
}
