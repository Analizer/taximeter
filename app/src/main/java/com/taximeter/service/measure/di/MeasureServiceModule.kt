package com.taximeter.service.measure.di

import com.taximeter.fragments.measure.mvp.MeasureContract
import com.taximeter.fragments.measure.mvp.MeasureModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@InstallIn(ServiceComponent::class)
@Module
abstract class MeasureServiceModule {

    @Binds
    abstract fun model(model: MeasureModel): MeasureContract.Model

    companion object {

        @Provides
        internal fun timerObservable(): Observable<Long> {
            return Observable.interval(1, 1, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}
