package com.taximeter.service.measure

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.location.Location
import android.os.IBinder
import com.taximeter.service.location.LocationConsumer
import com.taximeter.service.location.LocationService
import com.taximeter.service.location.LocationServiceFunctions
import com.taximeter.service.location.LocationSubscriber
import dagger.hilt.android.scopes.ServiceScoped
import javax.inject.Inject

@ServiceScoped
class MeasureServiceLocationHandlerModule @Inject constructor(private val mContext: Context)
    : ServiceConnection {

    private lateinit var mOnLocationAction: (location: Location) -> Unit
    private var mLocationServiceFunctions: LocationServiceFunctions? = null
    private var mLastLocation: Location? = null


    override fun onServiceConnected(name: ComponentName, binder: IBinder) {
        mLocationServiceFunctions = binder as LocationServiceFunctions
        subscribeToLocationService()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        tearDown()
    }

    fun requestLocations(onLocationAction: (location: Location) -> Unit) {
        mOnLocationAction = onLocationAction
        LocationService.connect(mContext, this)
    }

    fun tearDown() {
        mLocationServiceFunctions?.unSubscribe(LocationSubscriber.MEASURE)
        mLocationServiceFunctions = null
        mContext.unbindService(this)
    }

    private fun handleLocation(location: Location) {
        mOnLocationAction.invoke(location)
        mLastLocation = location
    }

    private fun subscribeToLocationService() {
        mLocationServiceFunctions!!.subscribe(mLocationConsumer, LocationSubscriber.MEASURE)
    }

    private val mLocationConsumer = object : LocationConsumer {

        override fun onLocation(location: Location) {
            handleLocation(location)
        }

        override fun onLocationError(e: Throwable) {}
    }
}