package com.taximeter.service.measure

interface MeasureServiceTerminateCallback {

    fun terminateCalled()
}