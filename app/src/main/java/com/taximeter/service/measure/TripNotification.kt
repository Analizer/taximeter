package com.taximeter.service.measure


import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.taximeter.R
import com.taximeter.activity.main.MainActivity
import com.taximeter.utils.CalculationUtils.*
import com.taximeter.utils.calculator.PriceCalculator
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.ServiceScoped
import javax.inject.Inject

@ServiceScoped
class TripNotification @Inject constructor(private val mContext: Context, private val mResources: Resources) {

    private val mNotificationManager: NotificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val mNotificationBuilder: NotificationCompat.Builder = createNotificationBuilder()

    private var mSmallView: RemoteViews = RemoteViews(mContext.packageName, R.layout.notification_small)
    private var mBigView: RemoteViews = RemoteViews(mContext.packageName, R.layout.notification_big)

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "com.taximeter.measureservice"
        private const val CHANNEL_NAME = "taxi_notification"
        private const val NOTIFICATION_ID = 101
    }

    private val terminateIntent: PendingIntent
        get() {
            val terminateIntent = Intent()
                    .setAction(Extra.ACTION_TERMINATE.tag)
                    .putExtra(Extra.TAG_TERMINATE_CALLER.tag, TerminateCaller.NOTIFICATION)
            val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            } else {
                PendingIntent.FLAG_UPDATE_CURRENT
            }
            return PendingIntent.getBroadcast(mContext, 0, terminateIntent, flags)
        }

    private val openMeasurePageIntent: PendingIntent
        get() {
            val pendingIntentFlags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) PendingIntent.FLAG_IMMUTABLE else 0
            val launchMeasureIntentFlags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            val launchMeasureIntent = Intent(mContext, MainActivity::class.java)
                    .setAction(Extra.TAG_START_MEASURE_ROUTE_FRAGMENT.tag)
                    .setFlags(launchMeasureIntentFlags)
            return PendingIntent.getActivity(mContext, 0, launchMeasureIntent, pendingIntentFlags)
        }

    init {
        mSmallView.setOnClickPendingIntent(R.id.close_btn, terminateIntent)
        mBigView = RemoteViews(mContext.packageName, R.layout.notification_big)
        mBigView.setOnClickPendingIntent(R.id.close_btn, terminateIntent)
    }

    fun start(service: MeasureService) {
        createNotificationBuilder()
        service.startForeground(NOTIFICATION_ID, getNotification(PriceCalculator.BASE_PRICE, 0, 0))
    }

    fun update(price: Int, meters: Int, time: Long) {
        mNotificationManager.notify(NOTIFICATION_ID, getNotification(price, meters, time))
    }

    private fun getNotification(price: Int, meters: Int, seconds: Long): Notification {
        updateViews(price, seconds, meters)
        return mNotificationBuilder
                .setCustomContentView(mSmallView)
                .setCustomBigContentView(mBigView)
                .build()
    }

    private fun updateViews(price: Int, seconds: Long, meters: Int) {
        val priceText = getFormattedPrice(price, mResources)
        val timeText = getFormattedTimeBySeconds(seconds, mResources, false)
        val distanceText = getKilometersTextByMeters(meters, mResources)
        mBigView.setTextViewText(R.id.price, priceText)
        mBigView.setTextViewText(R.id.time, timeText)
        mBigView.setTextViewText(R.id.distance, distanceText)
        mSmallView.setTextViewText(R.id.price, priceText)
    }

    private fun createNotificationBuilder(): NotificationCompat.Builder {
        val builder = if (Build.VERSION.SDK_INT < 26) {
            NotificationCompat.Builder(mContext)
        } else {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW)
            (mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)
            NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID)
        }
        return builder
                .setSmallIcon(R.drawable.taxi)
                .setOngoing(true)
                .setContentIntent(openMeasurePageIntent)
    }
}
