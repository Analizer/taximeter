package com.taximeter.service.measure

import android.app.ActivityManager
import android.content.Context
import javax.inject.Inject

class MeasureServiceStateChecker @Inject constructor(private val mContext: Context) {

    fun isMeasuring(): Boolean {
        val manager = mContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MeasureService::class.java.name == service.service.className) {
                return true
            }
        }
        return false
    }
}