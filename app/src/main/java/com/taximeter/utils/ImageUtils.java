package com.taximeter.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ImageUtils {

    private static final int THUMBNAIL_HEIGHT = 300;

    // departuretime is gonna be the name
    public static void saveImage(Context context, Bitmap thumb, String name) {
        FileOutputStream out;
        try {
            out = context.openFileOutput(name, Context.MODE_PRIVATE);
            int newWidth = getMapImageWidth(THUMBNAIL_HEIGHT, thumb);
            Bitmap.createScaledBitmap(thumb, newWidth, THUMBNAIL_HEIGHT, false).compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Keeping the aspect ratio of the original image.
     */
    private static int getMapImageWidth(int newHeight, Bitmap thumb) {
        int originalHeight = thumb.getHeight();
        int originalWidth = thumb.getWidth();
        int newWidth = newHeight * originalWidth / originalHeight;
        return newWidth;
    }

    public static Bitmap getImageBitmap(Context context, String name) {
        Bitmap toReturn = null;
        try {
            FileInputStream fis = context.openFileInput(name);
            toReturn = BitmapFactory.decodeStream(fis);
            fis.close();
        } catch (Exception e) {
            Logger.logE("Error in getImageBitmap: " + e.getMessage());
        }
        return toReturn;
    }
}
