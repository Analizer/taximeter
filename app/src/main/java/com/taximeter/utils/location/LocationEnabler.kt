package com.taximeter.utils.location

import android.Manifest
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * First it checks if the location access is enabled on the device, then if the app level location permission is granted.
 */
@FragmentScoped
class LocationEnabler @Inject constructor(private val mFragment: Fragment,
                                          private val mRxLocation: RxLocation,
                                          private val mLocationRequest: LocationRequest,
                                          private val mReporter: Reporter) {

    private var mLocationSettingsDisposable: Disposable? = null
    private lateinit var mGranted: () -> Unit
    private lateinit var mDenied: () -> Unit

    fun check(onGranted: () -> Unit, onDenied: () -> Unit) {
        mGranted = onGranted
        mDenied = onDenied
        checkDeviceLocationEnabled()
    }

    fun onRequestPermissionsResult() {
        if (isGranted()) mGranted.invoke() else mDenied.invoke()
    }

    fun tearDown() {
        mLocationSettingsDisposable?.dispose()
    }

    private fun checkDeviceLocationEnabled() {
        mLocationSettingsDisposable = mRxLocation
                .settings()
                .checkAndHandleResolution(mLocationRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { enabled ->
                            mLocationSettingsDisposable!!.dispose()
                            mReporter.reportEvent(AnalyticsCategory.Location, if (enabled) AnalyticsAction.DeviceLocationEnabledInLocationService else AnalyticsAction.DeviceLocationNotEnabledInLocationService)
                            handleDeviceLocationEnabledResult(enabled)
                        },
                        {
                            mLocationSettingsDisposable!!.dispose()
                            mReporter.reportEvent(AnalyticsCategory.Location, AnalyticsAction.DeviceLocationErrorInLocationService)
                        }
                )
    }

    private fun handleDeviceLocationEnabledResult(enabled: Boolean) {
        if (enabled) checkAppLevelPermission() else mDenied.invoke()
    }

    private fun checkAppLevelPermission() {
        if (isGranted()) mGranted.invoke() else requestLocationPermission()
    }

    private fun requestLocationPermission() {
        mFragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), Extra.PERMISSION_REQUEST_LOCATION.tag.toInt())
    }

    private fun isGranted(): Boolean {
        return ContextCompat.checkSelfPermission(mFragment.context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(mFragment.context!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
}
