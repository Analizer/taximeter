package com.taximeter.utils.location

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.location.Location
import android.os.IBinder
import com.taximeter.R
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.service.location.LocationConsumer
import com.taximeter.service.location.LocationService
import com.taximeter.service.location.LocationServiceFunctions
import com.taximeter.service.location.LocationSubscriber
import com.taximeter.utils.ErrorHandler
import com.taximeter.utils.Logger.log
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CurrentLocationRetriever @Inject constructor(private val mContext: Context,
                                                   private val mProgressBarModule: ProgressBarModule,
                                                   private val mLocationEnabler: LocationEnabler,
                                                   private val mErrorHandler: ErrorHandler)
    : ServiceConnection {

    private var mSubscriber: LocationSubscriber? = null
    private var mLocationServiceFunctions: LocationServiceFunctions? = null
    private lateinit var mOnSuccess: (location: Location) -> Unit
    private lateinit var mOnWaiting: () -> Unit
    private lateinit var mOnError: () -> Unit
    private lateinit var mOnDisabled: () -> Unit

    override fun onServiceConnected(name: ComponentName, service: IBinder) {
        mLocationServiceFunctions = service as LocationServiceFunctions
        subscribeToLocationService()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        tearDown()
    }

    fun addSubscriber(subscriber: LocationSubscriber) {
        mSubscriber = subscriber
    }

    fun requestLocation(onWaiting: () -> Unit = { mProgressBarModule.showProgress() },
                        onSuccess: (location: Location) -> Unit,
                        onError: () -> Unit = { handleError() },
                        onDisabled: () -> Unit = { handleDisabled() }) {
        mOnSuccess = onSuccess
        mOnError = onError
        mOnDisabled = onDisabled
        mOnWaiting = onWaiting

        checkPermission(onGranted = { handlePermissionGranted() })
    }

    fun checkPermission(onGranted: () -> Unit = { /*NO-OP*/ },
                        onDenied: () -> Unit = { handleDisabled() }) {
        mLocationEnabler.check(onGranted, onDenied)
    }

    fun onRequestPermissionsResult() {
        mLocationEnabler.onRequestPermissionsResult()
    }

    fun tearDown() {
        mLocationEnabler.tearDown()
        mLocationServiceFunctions?.unSubscribe(mSubscriber!!)
//        mLocationServiceFunctions = null //TODO
    }

    private fun handlePermissionGranted() {
        when {
            mLocationServiceFunctions == null -> LocationService.connect(mContext, this)
            mLocationServiceFunctions!!.hasLocation() -> mOnSuccess.invoke(mLocationServiceFunctions!!.location)
            else -> subscribeToLocationService()
        }
    }

    private fun subscribeToLocationService() {
        if (!mLocationServiceFunctions!!.isSubscribed(mSubscriber!!)) {
            mOnWaiting.invoke()
            mLocationServiceFunctions!!.subscribe(object : LocationConsumer {

                override fun onLocation(location: Location) {
                    mOnSuccess.invoke(mLocationServiceFunctions!!.location)
                    mLocationServiceFunctions!!.unSubscribe(mSubscriber!!)
                }

                override fun onLocationError(e: Throwable) {
                    mOnError.invoke()
                    mLocationServiceFunctions!!.unSubscribe(mSubscriber!!)
                }
            }, mSubscriber!!)
        } else {
            log("$mSubscriber is already subscribed to Location")
        }
    }

    private fun handleError() {
        mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.LocationError, R.string.your_location_not_found_text)
    }

    private fun handleDisabled() {
        mErrorHandler.handleError(AnalyticsCategory.Plan, AnalyticsAction.LocationDisabled, R.string.location_disabled_text)
    }
}