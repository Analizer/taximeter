package com.taximeter.utils

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

class NetworkModule @Inject constructor(private val mContext: Context) {

    fun isConnected(): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo?.isConnected ?: false
    }
}