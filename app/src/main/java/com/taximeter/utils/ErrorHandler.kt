package com.taximeter.utils

import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.ui.ProgressBarModule
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class ErrorHandler @Inject constructor(private val mReporter: Reporter,
                                       private val mSnackBarHandler: SnackBarHandler,
                                       private val mProgressBarModule: ProgressBarModule) {

    fun handleError(toReportCategory: AnalyticsCategory, toReportAction: AnalyticsAction, toShow: Int) {
        mProgressBarModule.hideProgress()
        mReporter.reportEvent(toReportCategory, toReportAction)
        mSnackBarHandler.showSnackBar(toShow)
    }
}