package com.taximeter.utils.calculator

import android.location.Location

import javax.inject.Inject

class PriceCalculator @Inject constructor() {

    private var mDistance = 0.0
    private var mTime = 0f

    fun update(lastLocation: Location, newLocation: Location): Int {
        if (isFastEnough(newLocation.speed)) {
            mDistance += lastLocation.distanceTo(newLocation).toDouble()
        } else {
            mTime += ((newLocation.time - lastLocation.time) / 1000).toFloat()
        }
        return (BASE_PRICE.toDouble() + mDistance * UNIT_PRICE + (mTime * WAITING_PRICE).toDouble()).toInt()
    }

    companion object {

        const val BASE_PRICE = 1000// alapdij
        private const val UNIT_PRICE = 0.4f// utdij, Ft/m
        private const val WAITING_PRICE = 1.66f// 100Ft/perc, varakozasi dij, Ft/sec
        private const val BIAS = 15// kuszobertek, 15km/h alatt WAITING_PRICE, felette UNIT_PRICE

        fun getEstimatedPrice(estimatedDistance: Int, estimatedTime: Int): Int {
            var estimatedPrice = BASE_PRICE
            when {
                estimatedTime == 0 -> estimatedPrice = 0
                estimatedDistance / estimatedTime * 3.6 < BIAS -> estimatedPrice += (estimatedTime * WAITING_PRICE).toInt()
                else -> estimatedPrice += (estimatedDistance * UNIT_PRICE).toInt()
            }
            return estimatedPrice
        }

        private fun isFastEnough(speed: Float): Boolean {
            return speed > BIAS / 3.6
        }
    }
}
