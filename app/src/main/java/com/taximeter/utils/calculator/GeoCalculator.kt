package com.taximeter.utils.calculator

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import javax.inject.Inject

class GeoCalculator @Inject constructor() {

    fun distance(start: Location, end: LatLng): Int {
        return start.distanceTo(toLocation(end)).toInt()
    }

    fun distance(start: Location, latitudeEnd: Double, longitudeEnd: Double): Int {
        return distance(start, LatLng(latitudeEnd, longitudeEnd))
    }

    private fun toLocation(latLng: LatLng): Location {
        val location = Location("")
        location.latitude = latLng.latitude
        location.longitude = latLng.longitude
        return location
    }

    fun toLatLng(location: Location): LatLng {
        return LatLng(location.latitude, location.longitude)
    }
}