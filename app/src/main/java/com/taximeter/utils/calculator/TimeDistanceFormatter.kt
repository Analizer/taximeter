package com.taximeter.utils.calculator

import android.content.res.Resources
import com.taximeter.R
import dagger.hilt.android.scopes.FragmentScoped
import java.text.DecimalFormat
import javax.inject.Inject

@FragmentScoped
class TimeDistanceFormatter @Inject constructor(private val mResources: Resources){

    fun getKilometersTextByMeters(meters: Int): String {
        val kilometers = meters / 1000 * 100000 / 100000
        val decimalFormat = DecimalFormat("#.#")
        return String.format(mResources.getString(R.string.route_distance_formatter), decimalFormat.format(kilometers.toDouble()))
    }

    fun getFormattedTimeBySeconds(seconds: Long, roundToMinutes: Boolean): String {
        return getFormattedTimeByMillis(seconds * 1000, roundToMinutes)
    }

    private fun getFormattedTimeByMillis(millis: Long, roundToMinutes: Boolean): String {
        val second = millis / 1000 % 60
        val minute = millis / (1000 * 60) % 60
        val hour = millis / (1000 * 60 * 60) % 24
        return if (hour == 0L && minute == 0L) {
            String.format(mResources.getString(R.string.time_formatter_seconds), second)
        } else if (roundToMinutes) {
            if (hour > 0) {
                String.format(mResources.getString(R.string.time_formatter_to_hours_to_minutes), hour, minute)
            } else {
                String.format(mResources.getString(R.string.time_formatter_to_minutes_to_minutes), minute)
            }
        } else {
            if (hour > 0) {
                String.format(mResources.getString(R.string.time_formatter_to_hours), hour, minute, second)
            } else {
                String.format(mResources.getString(R.string.time_formatter_to_minutes), minute, second)
            }
        }
    }
}