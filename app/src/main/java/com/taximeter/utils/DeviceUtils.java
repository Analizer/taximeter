package com.taximeter.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;


public class DeviceUtils {

    public static boolean checkPermission(Fragment fragment, String permissionCode, int callbackCode) {
        if (ContextCompat.checkSelfPermission(fragment.getActivity(), permissionCode) != PackageManager.PERMISSION_GRANTED) {
            fragment.requestPermissions(new String[]{permissionCode}, callbackCode);
            return false;
        }
        return true;
    }

    public static void callNumber(Context context, String phoneNumber) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
