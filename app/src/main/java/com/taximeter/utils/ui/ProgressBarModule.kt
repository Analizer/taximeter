package com.taximeter.utils.ui

import android.view.View
import android.widget.ProgressBar
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.android.synthetic.main.horizontal_progress.view.*
import javax.inject.Inject

@FragmentScoped
class ProgressBarModule @Inject constructor() {

    private lateinit var progressBar: ProgressBar

    fun bind(pageLayout: View) {
        progressBar = pageLayout.progress_bar
    }

    fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgress() {
        progressBar.visibility = View.GONE
    }
}