package com.taximeter.utils.ui;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;

public final class RecyclerViewUtils {

    private RecyclerViewUtils() {

    }

    public static void setupRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter, ItemTouchHelper.Callback touchHelperCallback) {
        setupRecyclerView(recyclerView, adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    public static void setupRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
