package com.taximeter.utils.ui

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class RecyclerViewSetup<T : RecyclerView.ViewHolder> @Inject constructor(private val mAdapter: RecyclerView.Adapter<T>) {

    fun setup(recyclerView: RecyclerView, touchHelperCallback: ItemTouchHelper.Callback) {
        recyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = mAdapter

        val touchHelper = ItemTouchHelper(touchHelperCallback)
        touchHelper.attachToRecyclerView(recyclerView)
    }
}