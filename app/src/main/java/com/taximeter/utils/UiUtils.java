package com.taximeter.utils;

import android.app.Activity;
import android.os.IBinder;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.taximeter.R;

public class UiUtils {

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        View viewInFocus = activity.getCurrentFocus();
        if (viewInFocus != null) {
            IBinder token = viewInFocus.getWindowToken();
            inputMethodManager.hideSoftInputFromWindow(token, 0);
        }
    }

    public static String formatDate(long time) {
        return LocaleManager.Companion.getCorrectDateFormat().format(time);
    }

    public static String getFormattedTripTimeRange(long start, long end) {
        StringBuilder sb = new StringBuilder(LocaleManager.Companion.getCorrectDateFormat().format(start));
        sb.append(" - ");
        sb.append(LocaleManager.Companion.getTimeFormat().format(end));
        return sb.toString();
    }

    public static void fadeInView(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            Animation fade = AnimationUtils.loadAnimation(view.getContext(), R.anim.fade_in);
            view.setAnimation(fade);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void fadeOutView(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            Animation fade = AnimationUtils.loadAnimation(view.getContext(), R.anim.fade_out);
            view.setAnimation(fade);
            view.setVisibility(View.GONE);
        }
    }

    public static void animateInView(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            Animation scaleAndFade = AnimationUtils.loadAnimation(view.getContext(), R.anim.scale_and_fade_in);
            view.setAnimation(scaleAndFade);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void animateOutView(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            Animation scaleAndFade = AnimationUtils.loadAnimation(view.getContext(), R.anim.scale_and_fade_out);
            view.setAnimation(scaleAndFade);
            view.setVisibility(View.INVISIBLE);
        }
    }
}
