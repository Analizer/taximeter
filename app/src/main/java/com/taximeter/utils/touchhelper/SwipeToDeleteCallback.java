package com.taximeter.utils.touchhelper;

import androidx.recyclerview.widget.RecyclerView;

public interface SwipeToDeleteCallback {

    void onItemSwiped(RecyclerView.ViewHolder viewHolder);
}
