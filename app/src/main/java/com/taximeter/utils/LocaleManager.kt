package com.taximeter.utils

import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import com.taximeter.activity.main.MainActivity
import com.taximeter.utils.extras.Language
import com.taximeter.persistence.sharedpref.SharedPref
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class LocaleManager @Inject constructor(private val mContext: Context) {

    private val mSharedPref: SharedPref = SharedPref(mContext)

    val currentLanguage: Language
        get() {
            return if (mSharedPref.isLanguageSet) {
                mSharedPref.language
            } else {
                val current = mContext.resources.configuration.locale
                if (current.language.equals(Language.HUNGARIAN.languageCode, ignoreCase = true)) Language.HUNGARIAN else Language.ENGLISH
            }
        }

    fun updateContext(): Context {
        val language = mSharedPref.language
        val locale = Locale(language.languageCode)
        Locale.setDefault(locale)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResourcesLocale(locale)
        } else updateResourcesLocaleLegacy(locale)
    }

    fun setLocale(language: Language) {
        mSharedPref.saveLanguage(language)
        restartApp(mContext)
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResourcesLocale(locale: Locale): Context {
        val configuration = mContext.resources.configuration
        configuration.setLocale(locale)
        return mContext.createConfigurationContext(configuration)
    }

    private fun updateResourcesLocaleLegacy(locale: Locale): Context {
        val resources = mContext.resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return mContext
    }

    private fun restartApp(context: Context) {
        val mStartActivity = Intent(context, MainActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
        System.exit(0)
    }

    companion object {

        private val HUNGARIAN_DATE_FORMAT = SimpleDateFormat("yyyy.MM.dd HH:mm")
        private val ENGLISH_DATE_FORMAT = SimpleDateFormat("dd.MM.yyyy HH:mm")
        val timeFormat = SimpleDateFormat("HH:mm")

        val correctDateFormat: SimpleDateFormat
            get() = if (Locale.getDefault() == Locale.ENGLISH) ENGLISH_DATE_FORMAT else HUNGARIAN_DATE_FORMAT
    }
}
