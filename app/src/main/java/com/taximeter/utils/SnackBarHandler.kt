package com.taximeter.utils

import android.view.View
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.taximeter.R
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class SnackBarHandler @Inject constructor(private val mRootView: View) {

    private var mSnackBar: Snackbar? = null

    fun showSnackBar(messageId: Int, onUndoClick: (() -> Unit)? = null) {
        showSnackBar(messageId, onUndoClick, null)
    }

    fun showSnackBar(messageId: Int,
                     onUndoClick: (() -> Unit)? = null,
                     onSnackBarDismissed: (() -> Unit)? = null) {
        val snackBar = Snackbar.make(mRootView, messageId, Snackbar.LENGTH_LONG)
        val onDismissedCallback = object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                onSnackBarDismissed?.invoke()
            }
        }
        if (onUndoClick != null) {
            snackBar.setAction(R.string.general_undo) {
                snackBar.removeCallback(onDismissedCallback)
                onUndoClick.invoke()
            }
        }
        if (onSnackBarDismissed != null) {
            snackBar.addCallback(onDismissedCallback)
        }
        snackBar.show()
        mSnackBar = snackBar
    }

    fun hideSnackBar() {
        mSnackBar?.dismiss()
    }
}