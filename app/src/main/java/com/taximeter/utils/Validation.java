package com.taximeter.utils;

import java.util.Collection;
import java.util.Map;

public final class Validation {

    private static final String STATUS_STRING = "status";
    private static final String STATUS_OK = "OK";

    private Validation() {
        // this class is not intended to be instantiated
    }

    /**
     * Empty when the parameter collection doesn't contain any elements or is null.
     * @param c collection
     * @return boolean
     */
    public static boolean empty(Collection<?> c) {
        return c == null || c.isEmpty();
    }

    /**
     * Empty when the parameter collection doesn't contain any elements or is null.
     * @param m a {@link Map}
     * @return boolean
     */
    public static boolean empty(Map<?, ?> m) {
        return m == null || m.isEmpty();
    }

    /**
     * Empty when the parameter string doesn't contains any character or null object.
     * @param s {@link CharSequence} object
     * @return boolean
     */
    public static boolean empty(CharSequence s) {
        return s == null || "".equals(s.toString().trim());
    }

    /**
     * Empty when the parameter object is null object.
     * @param o - {@link Object}
     * @return boolean
     */
    public static boolean empty(Object o) {
        return o == null;
    }

    /**
     * Empty when the parameter array is null object.
     * @param o - Object
     * @return boolean
     */
    public static boolean empty(Object[] o) {
        return o == null || o.length == 0;
    }

    /**
     * Negated empty.
     * @param c - {@link Collection}
     * @return boolean
     */
    public static boolean notEmpty(Collection<?> c) {
        return !empty(c);
    }

    /**
     * Negated empty.
     * @param m - {@link Map}
     * @return boolean
     */
    public static boolean notEmpty(Map<?, ?> m) {
        return !empty(m);
    }

    /**
     * Negated empty.
     * @param s - {@link CharSequence}
     * @return boolean
     */
    public static boolean notEmpty(CharSequence s) {
        return !empty(s);
    }

    /**
     * Negated empty.
     * @param o - {@link Object} array
     * @return boolean
     */
    public static boolean notEmpty(Object[] o) {
        return !empty(o);
    }

    /**
     * Negated empty.
     * @param o - {@link Object}
     * @return boolean
     */
    public static boolean notEmpty(Object o) {
        if (o instanceof Collection) {
            return !empty((Collection<?>) o);
        } else {
            return !empty(o);
        }
    }
}
