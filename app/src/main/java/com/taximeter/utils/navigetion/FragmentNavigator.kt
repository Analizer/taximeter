package com.taximeter.utils.navigetion

import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.taximeter.activity.main.MainActivity
import com.taximeter.activity.main.MainActivityFragmentHandler
import com.taximeter.fragments.FragmentType
import com.taximeter.fragments.company.list.mvp.Company
import com.taximeter.fragments.plan.dao.directions.DirectionModel
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.trip.Trip
import com.taximeter.utils.extras.Extra
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class FragmentNavigator @Inject constructor(val mFragment: Fragment) {

    private val mFragmentHandler: MainActivityFragmentHandler = (mFragment.activity as MainActivity).getFragmentHandler()

    fun goToTripDetailsFromMeasure(trip: Trip) {
        mFragmentHandler.removeFragmentFromBackStack(mFragment)
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_TRIP.tag, trip)
        bundle.putBoolean(Extra.TAG_TRIP_FROM_LIST.tag, false)
        mFragmentHandler.switchFragment(FragmentType.TRIP_DETAILS, bundle)
    }

    fun goToTripDetailsFromTripList(trip: Trip) {
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_TRIP.tag, trip)
        bundle.putBoolean(Extra.TAG_TRIP_FROM_LIST.tag, true)
        mFragmentHandler.switchFragment(FragmentType.TRIP_DETAILS, bundle)
    }

    fun goToCompanyDetails(company: Company) {
        val bundle = Bundle()
        bundle.putSerializable(Extra.TAG_COMPANY.tag, company)
        mFragmentHandler.switchFragment(FragmentType.COMPANY_DETAIL, bundle)
    }

    fun goToCompanyList() {
        mFragmentHandler.switchFragment(FragmentType.CALL)
    }

    fun goToPlan() {
        mFragmentHandler.switchFragment(FragmentType.PLAN)
    }

    fun goToMeasure() {
        mFragmentHandler.switchFragment(FragmentType.MEASURE)
    }

    fun goToMeasureFromPlan(direction: DirectionModel) {
        mFragmentHandler.removeFragmentFromBackStack(mFragment)
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_DIRECTIONS.tag, direction)
        mFragmentHandler.switchFragment(FragmentType.MEASURE, bundle)
    }

    fun goToAutoSuggest(model: PlanContract.Model) {
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_MODEL.tag, model)
        mFragmentHandler.switchFragment(FragmentType.AUTO_SUGGEST, bundle)
    }

    fun goToFavorites(model: PlanContract.Model, currentLocation: Location) {
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_CURRENT_LOCATION.tag, currentLocation)
        bundle.putParcelable(Extra.TAG_MODEL.tag, model)
        mFragmentHandler.switchFragment(FragmentType.FAVORITES, bundle)
    }

    fun goToAddressPickerMap(model: PlanContract.Model) {
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_MODEL.tag, model)
        mFragmentHandler.switchFragment(FragmentType.ADDRESS_PICKER_MAP, bundle)
    }

    fun goToRecents(model: PlanContract.Model) {
        val bundle = Bundle()
        bundle.putParcelable(Extra.TAG_MODEL.tag, model)
        mFragmentHandler.switchFragment(FragmentType.RECENT_PLANS, bundle)
    }

    fun goBackToPlan() {
        mFragmentHandler.goBackToPlan()
    }

    fun goBack() {
        mFragmentHandler.goBack()
    }
}
