package com.taximeter.utils.extras

enum class Extra constructor(val tag: String) {

    TAG_TRIP("tag_trip"),
    TAG_TRIP_FROM_LIST("tag_trip_from_list"),
    TAG_START_MEASURE_ROUTE_FRAGMENT("start_measure_fragment"),
    TAG_LOG("taxibudapest"),
    TAG_DIRECTIONS("directions_model"),
    TAG_COMPANY("taxiCompany"),
    TAG_MODEL("model"),
    TAG_AUTO_SUGGEST_RESULT("auto_suggest_result"),
    BASE_URL_DIRECTIONS("https://maps.googleapis.com/maps/api/directions/"),
    BASE_URL_STOPS("https://maps.googleapis.com/maps/api/place/"),
    BASE_URL_POSTAL_CODES("http://api.geonames.org/"),
    ACTION_STOP_FOREGROUND("com.taximeter.stopforeground"),
    AUTO_SUGGEST_REQUEST_CODE("1"),
    ACTION_COMING_FROM_AUTO_SUGGEST("coming_from_auto_suggest"),
    ACTION_TERMINATE("action_terminate"),
    TAG_TERMINATE_CALLER("terminate_caller"),
    TAG_CURRENT_LOCATION("current_location"),
    TAG_DIALOG_TYPE("dialog_type"),
    TAG_DIALOG_POSITIVE_LISTENER("dialog_positive_listener"),
    TAG_DIALOG_NEGATIVE_LISTENER("dialog_negative_listener"),
    TAG_BUDAPEST("Budapest"),
    PERMISSION_REQUEST_CALL_PHONE("986"),
    PERMISSION_REQUEST_LOCATION("987")
}
