package com.taximeter.utils.extras

import java.util.Locale

enum class Language constructor(val languageCode: String) {

    HUNGARIAN("Hu"),
    ENGLISH("En");

    companion object {

        fun getLanguageByLanguageCode(code: String?): Language {
            return if (code == HUNGARIAN.languageCode) HUNGARIAN else ENGLISH
        }

        val currentAppLanguage: Language
            get() = if (Locale.getDefault() == Locale.ENGLISH) {
                ENGLISH
            } else {
                HUNGARIAN
            }
    }

}
