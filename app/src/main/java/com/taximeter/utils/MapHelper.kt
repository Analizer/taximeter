package com.taximeter.utils

import android.content.res.Resources
import android.graphics.Color
import android.location.Location
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.taximeter.R
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class MapHelper @Inject constructor(private val mResources: Resources) {

    fun zoomToPolyline(markerPositionsOnMap: List<LatLng>, map: GoogleMap) {
        val builder = LatLngBounds.Builder()
        for (position in markerPositionsOnMap) {
            builder.include(position)
        }
        val bounds = builder.build()
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, mResources.getDimension(R.dimen.map_padding).toInt())
        map.moveCamera(cu)
    }


    fun zoomToLocation(location: Location, map: GoogleMap) {
        val positionLatLng = LatLng(location.latitude, location.longitude)
        zoomToLocation(positionLatLng, map)
    }

    fun zoomToLocation(coordinates: LatLng, map: GoogleMap) {
        zoomToLocation(coordinates, map, 15)
    }

    private fun zoomToLocation(coordinates: LatLng, map: GoogleMap, zoomLevel: Int) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, zoomLevel.toFloat()))
    }

    fun animateToLocation(coordinates: LatLng, map: GoogleMap) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15f))
    }

    fun zoomToBudapest(map: GoogleMap) {
        zoomToLocation(LatLng(47.497315, 19.070417), map, 10)
    }

    /**
     * Adds a marker to the map and sets its snippet so the infoWindowAdapter can decide if it's a departure or an arrival one.
     */
    fun addMarkerToMap(map: GoogleMap, position: LatLng): Marker {
//        return if (isFavorite) {
        return map.addMarker(MarkerOptions()
                .position(position)
                .icon(getFavoriteMarkerIcon()))!!
//        } else {
//            map.addMarker(MarkerOptions()
//                    .position(position))
//        }
    }

    private fun getFavoriteMarkerIcon(): BitmapDescriptor {
        val hsv = FloatArray(3)
        Color.colorToHSV(Color.parseColor("#349134"), hsv)
        return BitmapDescriptorFactory.defaultMarker(hsv[0])
    }

}