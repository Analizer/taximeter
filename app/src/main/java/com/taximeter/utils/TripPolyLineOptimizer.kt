package com.taximeter.utils

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.maps.android.PolyUtil
import com.taximeter.fragments.trip.Trip
import com.taximeter.utils.calculator.GeoCalculator
import javax.inject.Inject

/**
 * Helper class responsible for taking care of the polyline removing the unnecessary items from it.
 */
class TripPolyLineOptimizer @Inject constructor(private val mGson: Gson,
                                                private val mGeoCalculator: GeoCalculator) {

    private var mListSize: Int = LIST_SIZE_THRESHOLD

    fun update(trip: Trip, location: Location): MutableList<LatLng> {
        var positions: MutableList<LatLng> = trip.getPoints(mGson)
        positions.add(mGeoCalculator.toLatLng(location))
        if (positions.size == mListSize) {
            positions = PolyUtil.simplify(positions, 1.0)
            if (shouldExtendListSize(positions.size)) {
                mListSize += LIST_SIZE_THRESHOLD
            }
        }
        return positions
    }

    private fun shouldExtendListSize(positionsSize: Int): Boolean {
        return mListSize - positionsSize < LIST_SIZE_THRESHOLD / 2
    }

    companion object {
        private const val LIST_SIZE_THRESHOLD = 50
    }
}
