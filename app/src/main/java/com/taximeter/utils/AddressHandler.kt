package com.taximeter.utils

import android.location.Address
import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.address.AddressModel
import com.taximeter.utils.extras.Extra.TAG_BUDAPEST
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AddressHandler @Inject constructor() {

    fun getStopByAddress(address: Address, distance: Int = 0): Stop {
        return AddressModel(getAddressNameByAddress(address),
                AutoSuggestType.ADDRESS,
                LatLng(address.latitude, address.longitude),
                distance,
                address.postalCode,
                false,
                address.subThoroughfare)
    }

    fun isInBudapest(address: Address): Boolean {
        return Validation.notEmpty(address.locality) && address.locality == TAG_BUDAPEST.tag
    }

    private fun getAddressNameByAddress(address: Address): String {
        return if (address.thoroughfare != null) {
            address.thoroughfare
        } else if (address.featureName != null && TAG_BUDAPEST.tag != address.featureName) {
            address.featureName
        } else if (address.getAddressLine(0) != null) {
            address.getAddressLine(0)
        } else {
            TAG_BUDAPEST.tag
        }
    }
}
