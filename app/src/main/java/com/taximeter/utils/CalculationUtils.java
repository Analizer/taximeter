package com.taximeter.utils;

import android.content.res.Resources;

import com.taximeter.R;
import com.taximeter.utils.calculator.PriceCalculator;

import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.StringCharacterIterator;

public class CalculationUtils {

    /**
     * Returns a formatted String from seconds like "14 perc 22 mp"
     *
     * @param seconds
     * @return formatted time String
     */
    public static String getFormattedTimeBySeconds(long seconds, Resources resources, boolean roundToMinutes) {
        return getFormattedTimeByMillis(seconds * 1000, resources, roundToMinutes);
    }

    public static String getFormattedTimeByMillis(long millis, Resources resources, boolean roundToMinutes) {
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;
        if (hour == 0 && minute == 0) {
            return String.format(resources.getString(R.string.time_formatter_seconds), second);
        } else if (roundToMinutes) {
            if (hour > 0) {
                return String.format(resources.getString(R.string.time_formatter_to_hours_to_minutes), hour, minute);
            } else {
                return String.format(resources.getString(R.string.time_formatter_to_minutes_to_minutes), minute);
            }
        } else {
            if (hour > 0) {
                return String.format(resources.getString(R.string.time_formatter_to_hours), hour, minute, second);
            } else {
                return String.format(resources.getString(R.string.time_formatter_to_minutes), minute, second);
            }
        }
    }

    public static String getFullDateString(long date) {
        return LocaleManager.Companion.getCorrectDateFormat().format(date);
    }

    /**
     * @param meters
     * @return kilometers from meters, rounded to one decimal
     */
    public static String getKilometersTextByMeters(int meters, Resources resources) {
        float kilometers = ((meters / 1000) * 100000) / 100000;//TODO
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return String.format(resources.getString(R.string.route_distance_formatter), decimalFormat.format(kilometers));
    }

    public static String getFormattedPrice(int priceToFormat, Resources resources) {
        if (priceToFormat == 0) {
            return String.format(resources.getString(R.string.currency_formatter), String.valueOf(PriceCalculator.BASE_PRICE));
        }
        NumberFormat formatter = new DecimalFormat("#0");
        String formattedPrice;
        try {
            String tmp = formatter.format(priceToFormat);
            CharacterIterator it = new StringCharacterIterator(tmp);
            int counter = 1;
            int position = tmp.length();
            for (char ch = it.last(); ch != CharacterIterator.DONE; ch = it.previous()) {
                position--;
                if (counter % 3 == 0 && position != 0) {
                    tmp = new StringBuffer(tmp).insert(position, ".").toString();
                }
                counter++;
            }
            formattedPrice = tmp;
        } catch (Exception e) {
            formattedPrice = formatter.format(priceToFormat);
        }
        return String.format(resources.getString(R.string.currency_formatter), formattedPrice);
    }

    public static int getRoundedPrice(int price) {
        return price != 0 ? 10 * Math.round(price / 10) : 0;
    }

    public static String getFormattedSpeed(int speed, Resources resources) {
        return String.format(resources.getString(R.string.speed_formatter), speed);
    }
}
