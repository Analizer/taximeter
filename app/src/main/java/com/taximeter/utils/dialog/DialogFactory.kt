package com.taximeter.utils.dialog

import android.app.AlertDialog
import android.content.Context
import androidx.fragment.app.FragmentManager
import com.taximeter.fragments.di.ChildFragmentManager
import com.taximeter.fragments.dialog.TaxiDialogFragment
import com.taximeter.persistence.sharedpref.SharedPref
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class DialogFactory @Inject constructor(private val mContext: Context,
                                        @ChildFragmentManager private val mFragmentManager: FragmentManager,
                                        private val mSharedPref: SharedPref) {

    /**
     * Shows a dialog and calls the lambdas when needed:
     * $type The type of the dialog. It contains the text to show, the Analytics to report and the preference to save
     * $onPositive is called when the user presses the Ok / Yes button or if the dialog is not even needed. Cab be null
     * $onNegative is called when the user presses the No button if present. Can be null
     */
    fun showDialogIfNeeded(type: DialogType,
                           onPositive: (() -> Unit)? = null,
                           onNegative: (() -> Unit)? = null) {
        if (isDialogNeeded(type)) {
            TaxiDialogFragment.create(type, onPositive, onNegative)
                    .show(mFragmentManager, "alert_dialog")
        } else {
            onPositive?.invoke()
        }
    }

    private fun isDialogNeeded(type: DialogType): Boolean {
        return type.preference == null || mSharedPref.getBoolean(type.preference, true)
    }

    fun showPhoneNumbersDialog(phoneNumbers: Array<String>, onNumberSelected: (number: String) -> Unit) {
        AlertDialog.Builder(mContext)
                .setCancelable(true)
                .setItems(phoneNumbers) { dialog, item ->
                    onNumberSelected.invoke(phoneNumbers[item])
                    dialog.dismiss()
                }
                .create()
                .show()
    }
}
