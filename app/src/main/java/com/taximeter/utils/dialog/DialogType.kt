package com.taximeter.utils.dialog

import com.taximeter.R
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.persistence.sharedpref.Preference

enum class DialogType constructor(val textResId: Int,
                                  val analyticsAction: AnalyticsAction,
                                  val preference: Preference? = null) {

    START_MEASUREMENT_FROM_PLAN(R.string.dialog_start_measure_from_plan, AnalyticsAction.DialogStartMeasurementFromPlan, Preference.START_MEASUREMENT_FROM_PLAN_DIALOG_NEEDED),
    STOP_MEASUREMENT(R.string.dialog_stop_measurement, AnalyticsAction.DialogStopMeasurement, Preference.STOP_MEASUREMENT_DIALOG_NEEDED),
    PLAN(R.string.dialog_plan, AnalyticsAction.DialogPlan, Preference.PLAN_DIALOG_NEEDED),
    TRIP_DETAILS(R.string.dialog_trip_details, AnalyticsAction.DialogTripDetails, Preference.TRIP_DETAILS_DIALOG_NEEDED),
    CHANGE_LANGUAGE_WHILE_MEASURING(R.string.dialog_switch_language_while_measuring, AnalyticsAction.DialogChangeLanguageWhileMeasuring),
    ADDRESS_PICKER_ERROR(R.string.plan_map_error_message, AnalyticsAction.PlanPickerMapAddressNotSelectedError)
}
