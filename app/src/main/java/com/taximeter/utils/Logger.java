package com.taximeter.utils;

import android.util.Log;

import com.taximeter.BuildConfig;
import com.taximeter.utils.extras.Extra;

public class Logger {

    public static void log(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(Extra.TAG_LOG.getTag(), message);
        }
    }

    public static void logE(String message) {
        if (BuildConfig.DEBUG) {
            Log.e(Extra.TAG_LOG.getTag(), message);
        }
    }

}
