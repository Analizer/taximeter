package com.taximeter.reporting

enum class AnalyticsCategory constructor(val categoryName: String) {

    Measurement("StartStopMeasurement"),
    CallTaxi("CallTaxi"),
    CompanyList("CompanyList"),
    CompanyDetails("CompanyDetails"),
    Plan("Plan"),
    General("General"),
    PlanPickerMap("PlanPickerMap"),
    Recents("Recents"),
    Favorites("Favorites"),
    Settings("Settings"),
    TripDetails("TripDetails"),
    SavedTrips("SavedTrips"),
    AutoSuggest("AutoSuggest"),
    Dialog("Dialog"),
    Location("Location")

}
