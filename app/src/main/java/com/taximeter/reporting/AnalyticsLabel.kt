package com.taximeter.reporting

enum class AnalyticsLabel constructor(val labelName: String) {

    ErrorInMeasureTerminateBroadcastModule("ErrorInMeasureTerminateBroadcastModule"),
    Swipe("Swipe"),
    DeleteIcon("DeleteIcon"),
    FavoriteAdded("FavoriteAdded"),
    FavoriteRemoved("FavoriteRemoved"),
    City("City"),
    Fo("Fo"),
    Plus("Plus"),
    Hatszorhat("Hatszorhat"),
    Budapest("Budapest"),
    Taxi4("Taxi4"),
    Tele5("Tele5"),
    Elit("Elit")

}
