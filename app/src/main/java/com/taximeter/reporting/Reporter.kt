package com.taximeter.reporting

import android.content.res.Resources
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.taximeter.BuildConfig
import com.taximeter.utils.Logger.log
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Reporter @Inject constructor(private val mTracker: Tracker,
                                   private val mResources: Resources) {

    fun reportEvent(category: AnalyticsCategory, action: AnalyticsAction) {
        if (!BuildConfig.DEBUG) {
            mTracker.send(HitBuilders.EventBuilder().setCategory(category.categoryName).setAction(action.actionName).build())
        }
        log("Analytics: " + category.categoryName + ", " + action.actionName)
    }

    fun reportEvent(category: AnalyticsCategory, action: AnalyticsAction, label: AnalyticsLabel) {
        if (!BuildConfig.DEBUG) {
            mTracker.send(HitBuilders.EventBuilder().setCategory(category.categoryName).setAction(action.actionName).setLabel(label.labelName).build())
        }
        log("Analytics: " + category.categoryName + ", " + action.actionName)
    }

    fun sendScreenReport(pageId: Int) {
        if (!BuildConfig.DEBUG) {
            mTracker.setScreenName(mResources.getString(pageId))
            mTracker.send(HitBuilders.ScreenViewBuilder().build())
        }
        log("Analytics: " + mResources.getString(pageId))
    }
}
