package com.taximeter.persistence.database.favorite

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.address.AddressModel
import com.taximeter.fragments.plan.dao.place.PlaceModel

@Entity
data class FavoriteEntity constructor(@PrimaryKey(autoGenerate = true) val id: Long,
                                 @ColumnInfo var type: String,
                                 @ColumnInfo var latitude: Double,
                                 @ColumnInfo var longitude: Double,
                                 @ColumnInfo var postalCode: String,
                                 @ColumnInfo var address: String?,//address = description from AddressModel
                                 @ColumnInfo var streetNumber: String?,//from AddressModel
                                 @ColumnInfo var icon: String?,//from PlaceModel
                                 @ColumnInfo var name: String?,//from PlaceModel
                                 @ColumnInfo var vicinity: String?/*from PlaceModel*/) {

    constructor(address: AddressModel) : this(
            0,
            AutoSuggestType.ADDRESS.tag,
            address.position.latitude,
            address.position.longitude,
            address.postalCode,
            address.address,
            address.streetNumber,
            null,
            null,
            null
    )

    constructor(place: PlaceModel) : this(
            0,
            AutoSuggestType.PLACE.tag,
            place.geometry.location.lat,
            place.geometry.location.lng,
            place.postalCode,
            null,
            null,//TODO
            place.icon,
            place.name,
            place.vicinity
    )
}