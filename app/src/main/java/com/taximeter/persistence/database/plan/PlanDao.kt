package com.taximeter.persistence.database.plan

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface PlanDao {

    @Query("SELECT * from PlanEntity")
    fun getAll(): Single<List<PlanEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(planEntity: PlanEntity): Completable

    @Query("DELETE FROM PlanEntity WHERE date = :date")
    fun delete(date: Long): Completable
}