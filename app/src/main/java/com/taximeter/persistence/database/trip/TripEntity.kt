package com.taximeter.persistence.database.trip

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.taximeter.fragments.trip.Trip

@Entity
data class TripEntity constructor(@PrimaryKey var departureTime: Long,
                                  @ColumnInfo var distance: Int,
                                  @ColumnInfo var arrivalTime: Long,
                                  @ColumnInfo var price: Int,
                                  @ColumnInfo var sourceAddress: String,
                                  @ColumnInfo var destinationAddress: String,
                                  @ColumnInfo var mapThumbnailString: String,
                                  @ColumnInfo var averageSpeed: Float,
                                  @ColumnInfo var pointsString: String) {
    constructor(trip: Trip) : this(
            trip.departureTime,
            trip.distance,
            trip.arrivalTime,
            trip.price,
            trip.sourceAddress!!,
            trip.destinationAddress!!,
            trip.mapThumbnailString!!,
            trip.averageSpeed,
            trip.pointsString)
}