package com.taximeter.persistence.database.plan

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.address.AddressModel
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.fragments.plan.main.mvp.PlanContract

@Entity
data class PlanEntity constructor(@PrimaryKey val date: Long,
                                  @ColumnInfo var sourceType: String,
                                  @ColumnInfo var sourceLatitude: Double,
                                  @ColumnInfo var sourceLongitude: Double,
                                  @ColumnInfo var sourcePostalCode: String,
                                  @ColumnInfo var sourceAddress: String?,//address = description from AddressModel
                                  @ColumnInfo var sourceStreetNumber: String?,//from AddressModel
                                  @ColumnInfo var sourceIcon: String?,//from PlaceModel
                                  @ColumnInfo var sourceName: String?,//from PlaceModel
                                  @ColumnInfo var sourceVicinity: String?/*from PlaceModel*/,
                                  @ColumnInfo var destinationType: String,
                                  @ColumnInfo var destinationLatitude: Double,
                                  @ColumnInfo var destinationLongitude: Double,
                                  @ColumnInfo var destinationPostalCode: String,
                                  @ColumnInfo var destinationAddress: String?,//address = description from AddressModel
                                  @ColumnInfo var destinationStreetNumber: String?,//from AddressModel
                                  @ColumnInfo var destinationIcon: String?,//from PlaceModel
                                  @ColumnInfo var destinationName: String?,//from PlaceModel
                                  @ColumnInfo var destinationVicinity: String?/*from PlaceModel*/) {

    constructor(model: PlanContract.Model) : this(
            model.date,
            model.source.type.tag,
            model.source.position.latitude,
            model.source.position.longitude,
            model.source.postalCode,
            getAddress(model.source),
            getStreetNumber(model.source),
            getIcon(model.source),
            getName(model.source),
            getVicinity(model.source),
            model.destination.type.tag,
            model.destination.position.latitude,
            model.destination.position.longitude,
            model.destination.postalCode,
            getAddress(model.destination),
            getStreetNumber(model.destination),
            getIcon(model.destination),
            getName(model.destination),
            getVicinity(model.destination)
    )

    companion object {
        private fun getAddress(stop: Stop): String? {
            return when (stop.type) {
                AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> (stop as AddressModel).address
                AutoSuggestType.PLACE -> null
                AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong type in PlanEntity / getAddress: ${stop.type}")
            }
        }

        private fun getStreetNumber(stop: Stop): String? {
            return when (stop.type) {
                AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> (stop as AddressModel).streetNumber
                AutoSuggestType.PLACE -> null
                AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong type in PlanEntity / getStreetNumber: ${stop.type}")
            }
        }

        private fun getIcon(stop: Stop): String? {
            return when (stop.type) {
                AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> null
                AutoSuggestType.PLACE -> (stop as PlaceModel).icon
                AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong type in PlanEntity / getIcon: ${stop.type}")
            }
        }

        private fun getName(stop: Stop): String? {
            return when (stop.type) {
                AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> null
                AutoSuggestType.PLACE -> (stop as PlaceModel).name
                AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong type in PlanEntity / getName: ${stop.type}")
            }
        }

        private fun getVicinity(stop: Stop): String? {
            return when (stop.type) {
                AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> null
                AutoSuggestType.PLACE -> (stop as PlaceModel).vicinity
                AutoSuggestType.NONE -> throw IllegalArgumentException("Wrong type in PlanEntity / getVicinity: ${stop.type}")
            }
        }
    }
}