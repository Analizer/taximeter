package com.taximeter.persistence.database.favorite

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface FavoriteDao {

    @Query("SELECT * from FavoriteEntity")
    fun getAll(): Flowable<List<FavoriteEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favoriteEntity: FavoriteEntity): Completable

    @Query("DELETE FROM FavoriteEntity WHERE latitude = :latitude AND longitude = :longitude")
    fun delete(latitude: Double, longitude: Double): Completable
}