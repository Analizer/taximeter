package com.taximeter.persistence.database

import com.google.android.gms.maps.model.LatLng
import com.taximeter.fragments.plan.autosuggest.AutoSuggestType
import com.taximeter.fragments.plan.dao.Stop
import com.taximeter.fragments.plan.dao.address.AddressModel
import com.taximeter.fragments.plan.dao.place.CoordinatesModel
import com.taximeter.fragments.plan.dao.place.PlaceGeometryModel
import com.taximeter.fragments.plan.dao.place.PlaceModel
import com.taximeter.fragments.plan.main.mvp.PlanContract
import com.taximeter.fragments.plan.main.mvp.PlanModel
import com.taximeter.fragments.trip.Trip
import com.taximeter.persistence.database.favorite.FavoriteEntity
import com.taximeter.persistence.database.plan.PlanEntity
import com.taximeter.persistence.database.trip.TripEntity
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@FragmentScoped
class DatabaseModule @Inject constructor(private val database: TaxiDatabase) {

    private val mDisposables: CompositeDisposable = CompositeDisposable()

    fun tearDown() {
        mDisposables.dispose()
    }

    fun saveTrip(trip: Trip, onTripSaved: () -> Unit) {
        mDisposables.add(database
                .tripDao()
                .insert(TripEntity(trip))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onTripSaved.invoke() })
    }

    fun deleteTrip(departureTime: Long) {
        mDisposables.add(database
                .tripDao()
                .delete(departureTime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { /*NO-OP*/ })
    }

    fun getTrips(onModelReady: (MutableList<Trip>?) -> Unit) {
        mDisposables.add(database
                .tripDao()
                .getAll()
                .flatMap { tripEntities ->
                    val trips: MutableList<Trip> = mutableListOf()
                    for (tripEntity in tripEntities) {
                        trips.add(Trip(tripEntity.departureTime,
                                tripEntity.distance,
                                tripEntity.arrivalTime,
                                tripEntity.price,
                                tripEntity.sourceAddress,
                                tripEntity.destinationAddress,
                                tripEntity.mapThumbnailString,
                                tripEntity.averageSpeed,
                                tripEntity.pointsString))
                    }
                    Single.just(trips)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { trips: MutableList<Trip>? ->
                    onModelReady.invoke(trips)
                })
    }

    fun savePlan(plan: PlanContract.Model, onPlanSaved: () -> Unit = {}) {
        mDisposables.add(database
                .planDao()
                .insert(PlanEntity(plan))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onPlanSaved.invoke() })
    }

    fun deletePlan(date: Long) {
        mDisposables.add(database
                .planDao()
                .delete(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { /*NO-OP*/ })
    }

    fun getPlans(onModelReady: (MutableList<PlanContract.Model>?) -> Unit) {
        mDisposables.add(database
                .planDao()
                .getAll()
                .flatMap { planEntities ->
                    val plans: MutableList<PlanContract.Model> = mutableListOf()
                    for (planEntity in planEntities) {
                        plans.add(getPlanModelFromPlanEntity(planEntity))
                    }
                    Single.just(plans)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { plans: MutableList<PlanContract.Model>? ->
                    onModelReady.invoke(plans)
                })
    }

    fun saveFavorite(favorite: Stop, onFavoriteSaved: () -> Unit) {
        val favoriteEntity = when (favorite.type) {
            AutoSuggestType.ADDRESS, AutoSuggestType.CURRENT_LOCATION -> FavoriteEntity(favorite as AddressModel)
            AutoSuggestType.PLACE -> FavoriteEntity(favorite as PlaceModel)
            else -> throw IllegalArgumentException("Wrong argument passed to FavoriteEntity: " + favorite.type)
        }
        mDisposables.add(database
                .favoriteDao()
                .insert(favoriteEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onFavoriteSaved.invoke() })
    }

    fun deleteFavorite(position: LatLng, onFavoriteDeleted: () -> Unit = {}) {
        mDisposables.add(database
                .favoriteDao()
                .delete(position.latitude, position.longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onFavoriteDeleted.invoke() })
    }

    fun getFavorites(onModelReady: (MutableList<Stop>?) -> Unit) {
        mDisposables.add(database
                .favoriteDao()
                .getAll()
                .distinctUntilChanged()
                .flatMap { favoriteEntities ->
                    val favorites: MutableList<Stop> = mutableListOf()
                    for (favoriteEntity in favoriteEntities) {
                        when (favoriteEntity.type) {
                            AutoSuggestType.ADDRESS.tag -> favorites.add(getAddressModelFromFavoriteEntity(favoriteEntity))
                            AutoSuggestType.PLACE.tag -> favorites.add(getPlaceModelFromFavoriteEntity(favoriteEntity))
                            else -> throw IllegalArgumentException("Wrong type read in DatabaseModule / getFavorites() : " + favoriteEntity.type)
                        }
                    }
                    Flowable.just(favorites)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { favorites: MutableList<Stop>? ->
                    onModelReady.invoke(favorites)
                })
    }

    private fun getPlanModelFromPlanEntity(planeEntity: PlanEntity): PlanContract.Model {
        return PlanModel(planeEntity.date,
                getSourceStopFromPlanEntity(planeEntity),
                getDestinationStopFromPlanEntity(planeEntity))
    }

    private fun getSourceStopFromPlanEntity(planeEntity: PlanEntity): Stop {
        when (planeEntity.sourceType) {
            AutoSuggestType.ADDRESS.tag -> return AddressModel(
                    planeEntity.sourceAddress!!,
                    AutoSuggestType.ADDRESS,
                    LatLng(planeEntity.sourceLatitude, planeEntity.sourceLongitude),
                    0,
                    planeEntity.sourcePostalCode,
                    false,
                    planeEntity.sourceStreetNumber)
            AutoSuggestType.PLACE.tag -> return PlaceModel(
                    PlaceGeometryModel(CoordinatesModel(planeEntity.sourceLatitude, planeEntity.sourceLongitude)),
                    planeEntity.sourceIcon!!,
                    planeEntity.sourceName!!,
                    planeEntity.sourceVicinity!!,
                    AutoSuggestType.PLACE,
                    LatLng(planeEntity.sourceLatitude, planeEntity.sourceLongitude),
                    0,
                    planeEntity.sourcePostalCode,
                    false)
            else -> throw IllegalArgumentException("Wrong type read in DatabaseModule / getSourceStopFromPlanEntity() : " + planeEntity.sourceType)

        }
    }

    private fun getDestinationStopFromPlanEntity(planeEntity: PlanEntity): Stop {
        when (planeEntity.destinationType) {
            AutoSuggestType.ADDRESS.tag -> return AddressModel(
                    planeEntity.destinationAddress!!,
                    AutoSuggestType.ADDRESS,
                    LatLng(planeEntity.destinationLatitude, planeEntity.destinationLongitude),
                    0,
                    planeEntity.destinationPostalCode,
                    false,
                    planeEntity.destinationStreetNumber)
            AutoSuggestType.PLACE.tag -> return PlaceModel(
                    PlaceGeometryModel(CoordinatesModel(planeEntity.destinationLatitude, planeEntity.destinationLongitude)),
                    planeEntity.destinationIcon!!,
                    planeEntity.destinationName!!,
                    planeEntity.destinationVicinity!!,
                    AutoSuggestType.PLACE,
                    LatLng(planeEntity.destinationLatitude, planeEntity.destinationLongitude),
                    0,
                    planeEntity.destinationPostalCode,
                    false)
            else -> throw IllegalArgumentException("Wrong type read in DatabaseModule / getDestinationStopFromPlanEntity() : " + planeEntity.destinationType)

        }
    }

    private fun getAddressModelFromFavoriteEntity(favoriteEntity: FavoriteEntity): AddressModel {
        return AddressModel(
                favoriteEntity.address!!,
                AutoSuggestType.ADDRESS,
                LatLng(favoriteEntity.latitude, favoriteEntity.longitude),
                0,
                favoriteEntity.postalCode,
                false,
                favoriteEntity.streetNumber)
    }

    private fun getPlaceModelFromFavoriteEntity(favoriteEntity: FavoriteEntity): PlaceModel {
        return PlaceModel(
                PlaceGeometryModel(CoordinatesModel(favoriteEntity.latitude, favoriteEntity.longitude)),
                favoriteEntity.icon!!,
                favoriteEntity.name!!,
                favoriteEntity.vicinity!!,
                AutoSuggestType.PLACE,
                LatLng(favoriteEntity.latitude, favoriteEntity.longitude),
                0,
                favoriteEntity.postalCode,
                false
        )
    }
}