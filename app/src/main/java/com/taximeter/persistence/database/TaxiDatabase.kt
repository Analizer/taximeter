package com.taximeter.persistence.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.taximeter.persistence.database.favorite.FavoriteDao
import com.taximeter.persistence.database.favorite.FavoriteEntity
import com.taximeter.persistence.database.plan.PlanDao
import com.taximeter.persistence.database.plan.PlanEntity
import com.taximeter.persistence.database.trip.TripDao
import com.taximeter.persistence.database.trip.TripEntity

@Database(entities = [TripEntity::class, FavoriteEntity::class, PlanEntity::class], version = 1)
abstract class TaxiDatabase : RoomDatabase() {

    abstract fun tripDao(): TripDao

    abstract fun favoriteDao(): FavoriteDao

    abstract fun planDao(): PlanDao

    companion object {
        fun get(context: Context): TaxiDatabase {
            return Room
                    .databaseBuilder(context.applicationContext, TaxiDatabase::class.java, "taxibudapest.db")
                    .build()
        }
    }
}