package com.taximeter.persistence.database.trip

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface TripDao {

    @Query("SELECT * from TripEntity")
    fun getAll(): Single<List<TripEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tripEntity: TripEntity): Completable

    @Query("DELETE FROM TripEntity WHERE departureTime = :departureTime")
    fun delete(departureTime: Long): Completable
}