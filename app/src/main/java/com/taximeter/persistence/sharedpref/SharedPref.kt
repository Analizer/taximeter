package com.taximeter.persistence.sharedpref

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.taximeter.reporting.AnalyticsAction
import com.taximeter.reporting.AnalyticsCategory
import com.taximeter.reporting.Reporter
import com.taximeter.utils.extras.Language
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPref @Inject constructor(context: Context,
                                     val reporter: Reporter? = null) {

    private val appSharedPrefs: SharedPreferences = context.getSharedPreferences(Preference.PREFERENCE_NAME.tag, Activity.MODE_PRIVATE)
    private val prefsEditor: SharedPreferences.Editor = appSharedPrefs.edit()

    val language: Language
        get() = Language.getLanguageByLanguageCode(appSharedPrefs.getString(Preference.LANGUAGE.tag, Language.ENGLISH.languageCode))

    val isLanguageSet: Boolean
        get() = appSharedPrefs.contains(Preference.LANGUAGE.tag)

    fun saveLanguage(language: Language) {
        prefsEditor.putString(Preference.LANGUAGE.tag, language.languageCode)
        prefsEditor.commit()
    }

    fun saveBoolean(preference: Preference, value: Boolean) {
        prefsEditor.putBoolean(preference.tag, value)
        prefsEditor.commit()
    }

    fun getBoolean(preference: Preference, defaultValue: Boolean): Boolean {
        return appSharedPrefs.getBoolean(preference.tag, defaultValue)
    }

    fun clearSharedPrefsIfNeeded() {//TODO needed?
        if (!getBoolean(Preference.SHARED_PREFS_CLEARED, false)) {
            prefsEditor.clear()
            saveBoolean(Preference.SHARED_PREFS_CLEARED, true)
            reporter?.reportEvent(AnalyticsCategory.General, AnalyticsAction.SharedPreferencesCleared)
        }
    }
}
