package com.taximeter.persistence.sharedpref

enum class Preference constructor(val tag: String) {

    PREFERENCE_NAME("taxi_meter_preferences"),
    LANGUAGE("settings"),
    TRAFFIC("traffic"),
    SCREEN_AWAKE("screen_awake"),
    INITIAL_DRAWER("initial_drawer"),
    STOP_MEASUREMENT_DIALOG_NEEDED("stop_measurement_dialog_needed"),
    START_MEASUREMENT_FROM_PLAN_DIALOG_NEEDED("start_measurement_from_plan_dialog_needed"),
    PLAN_DIALOG_NEEDED("plan_needed"),
    TRIP_DETAILS_DIALOG_NEEDED("trip_details_needed"),
    SHARED_PREFS_CLEARED("shared_prefs_cleared")
}
